import type { Preview } from "@storybook/react";

import React from "react";
import { Provider } from "jotai";

import { NextUIProvider } from "../src/providers/NextUi";

import "../src/app/globals.css";

const preview: Preview = {
  parameters: {
    controls: {
      matchers: {
        string: /(background|color)$/i,
        date: /Date$/i,
      },
    },
  },
  decorators: [
    (story, context) => <NextUIProvider>{story(context)}</NextUIProvider>,
    (story, context) => <Provider>{story(context)}</Provider>,
  ],
};

export default preview;
