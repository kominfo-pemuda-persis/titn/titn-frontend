"use client";

import { useCallback, useEffect, useState } from "react";
import { useAtom } from "jotai/react";

import { clearUserData, fetchCurrentUser } from "@/services/actions";
import { loggedUser } from "@/states/user";
import { AnggotaProps } from "@/types";

const useFetchUser = () => {
  const [currentUser, setCurrentUser] = useAtom(loggedUser);
  const [isLoading, setIsLoading] = useState<boolean>(true);

  const getUser = useCallback(async () => {
    try {
      const newUser: AnggotaProps = await fetchCurrentUser();

      if (newUser) {
        setCurrentUser(newUser);
      } else {
        clearUserData();
      }
    } catch (error) {
      console.log(error);
      clearUserData();
    } finally {
      setIsLoading(false);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    getUser();
  }, [getUser]);

  return {
    currentUser,
    isLoading,
    setIsLoading,
  };
};

export default useFetchUser;
