import { NextRequest, NextResponse } from "next/server";

export async function middleware(request: NextRequest) {
	const session = request.cookies.get("session");

	if (request.nextUrl.pathname.startsWith("/login")) {
		if (session) {
			return NextResponse.redirect(new URL("/dashboard", request.url));
		}
	} else {
		if (!session) {
			const redirectUrl = `/login?redirect=${encodeURIComponent(
				request.nextUrl.pathname
			)}`;
			return NextResponse.redirect(new URL(redirectUrl, request.url));
		}
	}
}

export const config = {
	matcher: [
		"/dashboard",
		"/calon-peserta",
		"/calon-peserta/:path*",
		"/peserta",
		"/peserta/:path*",
		"/rekap",
		"/rekap/:path*",
		"/approval-history",
		"/settings/:path*",
		"/check-in",
		"/spinner",
		"/login",
	],
};
