import { Toaster } from "react-hot-toast";

const ReactHotToast = () => <Toaster />;

export default ReactHotToast;
