"use client";

import { ReactNode } from "react";
import { ThemeProvider as NextThemesProvider } from "next-themes";
import { useRouter } from "next/navigation";
import { NextUIProvider as Provider } from "@nextui-org/react";

import { Theme } from "@/types/utils";

export const NextUIProvider = ({ children }: { children: ReactNode }) => {
  const router = useRouter();

  return (
    <Provider navigate={router.push}>
      <NextThemesProvider attribute="class" defaultTheme={Theme.Light}>
        {children}
      </NextThemesProvider>
    </Provider>
  );
};
