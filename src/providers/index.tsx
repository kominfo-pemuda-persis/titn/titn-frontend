import { ReactNode } from "react";

import { JotaiProviders } from "@/providers/Jotai";
import { NextUIProvider } from "@/providers/NextUi";
import ReactHotToast from "@/providers/ReactHotToast";
import TanstackProvider from "@/providers/Tanstack";

const Providers = ({ children }: { children: ReactNode }) => (
  <NextUIProvider>
    <JotaiProviders>
      <TanstackProvider>
        {children}
        <ReactHotToast />
      </TanstackProvider>
    </JotaiProviders>
  </NextUIProvider>
);

export default Providers;
