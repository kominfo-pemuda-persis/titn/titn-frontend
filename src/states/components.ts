import { atomWithReset } from "jotai/utils";

export const isElementLoading = atomWithReset<boolean>(false);
