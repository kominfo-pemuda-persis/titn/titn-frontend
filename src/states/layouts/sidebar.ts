import { atom } from "jotai";

export const collapsed = atom<boolean>(true);
