import { atom } from "jotai";
import { atomWithReset } from "jotai/utils";

export const selectedEvents = atomWithReset<{
  sports: number | null;
  science: number[];
  nothing: boolean;
}>({
  sports: null,
  science: [],
  nothing: false,
});

export const selectedEventsState = atom((get) => {
  const userSelectedEvents = get(selectedEvents);
  const nonEventsId = userSelectedEvents.nothing ? 20 : undefined;
  const events = [
    ...userSelectedEvents.science,
    userSelectedEvents.sports,
    nonEventsId,
  ].filter(Boolean);

  return { events };
});
