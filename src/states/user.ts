import { atom } from "jotai";

import { RouteConfig } from "@/constant/routes";
import { AnggotaProps } from "@/types";

export const loggedUser = atom<AnggotaProps | null>(null);

export const currentUser = atom((get) => {
  const user: AnggotaProps | null = get(loggedUser);

  return { user };
});

export const permission = atom((get) => {
  const user = get(currentUser)?.user;
  const currentRole = user?.role;

  const isAnggota = currentRole?.find((role) => role.name === "anggota");
  const isAdminPC = currentRole?.find((role) => role.name === "admin_pc");
  const isAdminPD = currentRole?.find((role) => role.name === "admin_pd");
  const isAdminPW = currentRole?.find((role) => role.name === "admin_pw");
  const isAdminPP = currentRole?.find((role) => role.name === "admin_pp");
  const isTasykilPP = currentRole?.find((role) => role.name === "tasykil_pp");
  const isSuperAdmin = currentRole?.find((role) => role.name === "superadmin");
  const isAdmin =
    isAdminPC || isAdminPD || isAdminPW || isAdminPP || isSuperAdmin;

  const routeChecker = (route: string) => {
    if (isAnggota && RouteConfig.allowedAnggotaUrl.includes(route)) {
      return true;
    }

    if (isAdminPC && RouteConfig.allowedAdminPCUrl.includes(route)) {
      return true;
    }

    if (isAdminPD && RouteConfig.allowedAdminPDUrl.includes(route)) {
      return true;
    }

    if (isAdminPW && RouteConfig.allowedAdminPWUrl.includes(route)) {
      return true;
    }

    if (isAdminPP && RouteConfig.allowedAdminPPUrl.includes(route)) {
      return true;
    }

    if (isTasykilPP && RouteConfig.allowedTasykilPPUrl.includes(route)) {
      return true;
    }

    if (isSuperAdmin && RouteConfig.allowedSuperAdminUrl.includes(route)) {
      return true;
    }

    return false;
  };

  return {
    currentRole: {
      isAnggota,
      isAdminPC,
      isAdminPD,
      isAdminPW,
      isAdminPP,
      isTasykilPP,
      isSuperAdmin,
      isAdmin,
    },
    isAllowedCheckIn: isSuperAdmin || isAdminPP,
    isAllowedSpinner: isSuperAdmin || isAdminPP,
    routeChecker,
  };
});
