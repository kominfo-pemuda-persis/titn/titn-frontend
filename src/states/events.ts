import { atom } from "jotai";

import { eventItem } from "@/types/events";

export const eventsAtom = atom<eventItem[]>([]);
