import { Selection, SortDescriptor } from "@nextui-org/react";
import { atomWithReset } from "jotai/utils";

export const tableStates = atomWithReset<{
  searchValue: string;
  page: number;
  pages: number;
  rowsPerPage: number;
  totalItems: number;
  sortDescriptor: SortDescriptor;
  selectedKeys: Selection;
  statusFilter: Selection;
}>({
  searchValue: "",
  page: 1,
  pages: 1,
  rowsPerPage: 15,
  totalItems: 0,
  selectedKeys: new Set([]),
  statusFilter: new Set([]),
  sortDescriptor: {
    column: "createdAt",
    direction: "descending",
  },
});
