import axios from "axios";

export const urlToImg = async (url: string) => {
  const response = await axios.get(url, {
    responseType: "arraybuffer",
  });
  const base64 = Buffer.from(response.data, "binary").toString("base64");
  return base64;
};
