"use client";

import { ChangeEvent, forwardRef, Ref } from "react";

import { Radio, RadioGroup } from "@/components/molecules/RadioGroup";

const UkuranBaju = (
  {
    isDisabled,
    value,
    handleOnChange,
  }: {
    isDisabled: boolean;
    value: string;
    handleOnChange: (e: ChangeEvent<HTMLInputElement>) => void;
  },
  ref: Ref<HTMLInputElement>
) => {
  return (
    <>
      <div className="font-semibold">Ukuran Baju</div>
      <div className="flex items-center gap-2">
        <RadioGroup
          id="clothes_size"
          name="clothes_size"
          value={value}
          ref={ref}
          onChange={handleOnChange}
          isRequired
          isDisabled={isDisabled}
        >
          <Radio required value="S">
            S
          </Radio>
          <Radio value="M">M</Radio>
          <Radio value="L">L</Radio>
          <Radio value="XL">XL</Radio>
          <Radio value="XXL">XXL</Radio>
          <Radio value="XXXL">XXXL</Radio>
          <Radio className="hidden" value="invalid" />
        </RadioGroup>
      </div>
    </>
  );
};

export default forwardRef(UkuranBaju);
