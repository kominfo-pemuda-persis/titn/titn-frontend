"use client";

import { ChangeEvent, forwardRef, Ref, useEffect, useState } from "react";
import { useQuery } from "@tanstack/react-query";
import { useAtom } from "jotai/react";
import { RESET } from "jotai/utils";

import { Checkbox } from "@/components/atoms/Checkbox";
import { Chip } from "@/components/atoms/Chip";
import { Spinner } from "@/components/atoms/Spinner";
import { Radio, RadioGroup } from "@/components/molecules/RadioGroup";
import { getServerDate } from "@/services/actions";
import { getEvents } from "@/services/query/events";
import { selectedEvents as selectedEventsAtoms } from "@/states/form";
import { eventItem } from "@/types/events";

const ListKompetisi = (
  {
    isDisabled,
  }: {
    isDisabled: boolean;
  },
  ref: Ref<HTMLInputElement>
) => {
  const [userSelectedEvents, setUserSelectedEvents] =
    useAtom(selectedEventsAtoms);

  const [events, setEvents] = useState<eventItem[]>([]);
  const [isNotAvailable, setAvailability] = useState<boolean>(true);
  const [isClosed, setClosed] = useState<boolean>(false);

  const { isLoading } = useQuery({
    queryKey: ["events"],
    queryFn: async () => {
      const results = await getEvents();
      setEvents(results.content ?? []);
      return results;
    },
  });

  const [selectedScienceEventIds, setSelectedScienceEventIds] =
    useState<number[]>();
  const [isNotFollowingEvent, setIsNotFollowingEvent] =
    useState<boolean>(false);

  const handleRadioChangeSport = (e: ChangeEvent<HTMLInputElement>) => {
    const selectedId = e.target.value;

    setUserSelectedEvents((prev) => ({
      ...prev,
      sports: parseInt(selectedId),
      nothing: false,
    }));

    if (isNotFollowingEvent) {
      setIsNotFollowingEvent(false);
    }
  };

  const handleRadioChangeScience = (e: ChangeEvent<HTMLInputElement>) => {
    const selectedId = e.target.value;

    setUserSelectedEvents((prev) => ({
      ...prev,
      science: [parseInt(selectedId)],
      nothing: false,
    }));

    if (isNotFollowingEvent) {
      setIsNotFollowingEvent(false);
    }
  };

  // const handleOnChangeCheckbox = (id: number) => {
  //   let ids = [];

  //   if (selectedScienceEventIds?.includes(id)) {
  //     ids = selectedScienceEventIds.filter((event) => event !== id);
  //   } else {
  //     ids = [...(selectedScienceEventIds || []), id];
  //   }

  //   if (isNotFollowingEvent) {
  //     setIsNotFollowingEvent(false);
  //   }

  //   setSelectedScienceEventIds(ids);
  //   setUserSelectedEvents((prev) => ({
  //     ...prev,
  //     science: ids,
  //     nothing: false,
  //   }));
  // };

  const handleNotFollowingChange = () => {
    setIsNotFollowingEvent(!isNotFollowingEvent);
    setUserSelectedEvents({
      nothing: !isNotFollowingEvent,
      science: [],
      sports: null,
    });

    if (!isNotFollowingEvent) {
      setSelectedScienceEventIds([]);
    }
  };

  useEffect(() => {
    getServerDate().then((res) => {
      const isExpired = new Date(res) > new Date("9/6/2024, 12:01:00 AM");

      console.log({
        currentServer: new Date(res).toLocaleString(),
        target: "9/5/2024, 12:18:47 AM",
        isExpired,
      });

      setAvailability(isExpired);
      setClosed(isExpired);
    });

    return () => {
      setUserSelectedEvents(RESET);
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <div>
      <div className="font-semibold">Kompetisi</div>
      {isLoading ? (
        <div className="flex w-full justify-center">
          <Spinner size="lg" color="primary" />
        </div>
      ) : (
        events.length > 0 && (
          <>
            <RadioGroup
              id="event_id"
              name="event_id"
              className="mt-2 flex flex-col gap-2"
              isRequired
              ref={ref}
              value={String(userSelectedEvents.sports)}
              onChange={handleRadioChangeSport}
              isDisabled={isDisabled || isNotAvailable}
            >
              <div className="flex items-center gap-2 font-semibold">
                Cabang Olahraga{" "}
                {isClosed && (
                  <Chip size="sm" color="danger">
                    Sudah ditutup
                  </Chip>
                )}
              </div>
              {events
                .filter((event) => event.type === "OLAHRAGA")
                .map((event) => (
                  <Radio key={event.id} value={event.id.toString()} required>
                    {event.name}
                  </Radio>
                ))}
            </RadioGroup>
            <RadioGroup
              id="event_id"
              name="event_id"
              className="mt-2 flex flex-col gap-2"
              isRequired
              ref={ref}
              value={String(userSelectedEvents.science)}
              onChange={handleRadioChangeScience}
              isDisabled={isDisabled || isNotAvailable}
            >
              <div className="flex items-center gap-2 font-semibold">
                Cabang Ilmiah{" "}
                {isClosed && (
                  <Chip size="sm" color="danger">
                    Sudah ditutup
                  </Chip>
                )}
              </div>
              {events
                .filter((event) => event.type === "ILMIAH")
                .map((event) => (
                  <Radio key={event.id} value={event.id.toString()} required>
                    {event.name}
                  </Radio>
                ))}
            </RadioGroup>

            <div className="my-2 flex flex-col gap-2">
              <div className="font-semibold">Lainnya</div>
              {events
                .filter((event) => event.type === "ETC")
                .filter((event) => event.id.toString() === "20")
                .map((event) => (
                  <Checkbox
                    disabled={isDisabled}
                    radius="full"
                    key={event.id}
                    isSelected={userSelectedEvents.nothing}
                    onChange={handleNotFollowingChange}
                  >
                    {event.name}
                  </Checkbox>
                ))}
            </div>
          </>
        )
      )}
    </div>
  );
};

export default forwardRef(ListKompetisi);
