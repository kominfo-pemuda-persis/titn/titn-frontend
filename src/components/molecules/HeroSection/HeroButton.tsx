"use client";

import { useRouter } from "next/navigation";

import { Button } from "@/components/atoms/Button";
import { ROUTE_DAFTAR } from "@/constant/routes";

const HeroButton = () => {
  const router = useRouter();

  return (
    <Button
      color="primary"
      onClick={() => router.push(ROUTE_DAFTAR)}
      className="w-full md:w-fit"
      variant="shadow"
    >
      Daftar Sekarang
    </Button>
  );
};

export { HeroButton };
