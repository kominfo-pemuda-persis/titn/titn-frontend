import { ReactNode } from "react";
import { Selection } from "@nextui-org/react";
import { SearchIcon } from "@nextui-org/shared-icons";
import { useAtom } from "jotai";
import { BsChevronDown, BsDownload } from "react-icons/bs";

import { Button } from "@/components/atoms/Button";
import { Input } from "@/components/atoms/Input";
import {
  Dropdown,
  DropdownItem,
  DropdownMenu,
  DropdownTrigger,
} from "@/components/molecules/Dropdown";
import { tableStates } from "@/states/table";

const Header = ({
  filterValue,
  onClear,
  onSearchChange,
  placeholder = "Cari berdasarkan nama...",
  startContext = <SearchIcon />,
  handleStatusChange,
  statusOptions,
  isLoading,
  additionalContents,
  enableDownload,
  handleDownloadExcel,
}: {
  filterValue: string;
  onClear: () => void;
  onSearchChange: (value: string) => void;
  placeholder?: string;
  startContext?: ReactNode;
  handleStatusChange?: (keys: Selection) => void;
  statusOptions?: { uid: string; name: string }[];
  isLoading?: boolean;
  additionalContents?: ReactNode;
  enableDownload?: boolean;
  handleDownloadExcel?: () => void;
}) => {
  const [{ statusFilter }] = useAtom(tableStates);
  return (
    <>
      <div className="flex flex-col gap-4">
        <div className="flex items-center justify-between gap-3">
          <Input
            isClearable
            className="w-full sm:max-w-[44%]"
            placeholder={placeholder}
            startContent={startContext}
            value={filterValue}
            onClear={onClear}
            onValueChange={onSearchChange}
          />
          <div className="flex items-center gap-3">
            {statusOptions && statusOptions.length > 0 && (
              <Dropdown isDisabled={isLoading}>
                <DropdownTrigger>
                  <Button
                    endContent={<BsChevronDown className="text-small" />}
                    variant="flat"
                  >
                    <span className="hidden md:flex">Status :</span>

                    {Array.from(statusFilter)[0]}
                  </Button>
                </DropdownTrigger>
                <DropdownMenu
                  disallowEmptySelection
                  aria-label="Status filter"
                  closeOnSelect={true}
                  selectedKeys={statusFilter}
                  selectionMode="single"
                  defaultSelectedKeys="all"
                  onSelectionChange={handleStatusChange}
                >
                  {statusOptions.map((status) => (
                    <DropdownItem key={status.uid}>{status.name}</DropdownItem>
                  ))}
                </DropdownMenu>
              </Dropdown>
            )}
            {additionalContents}
            {enableDownload && (
              <Button
                isLoading={isLoading}
                isIconOnly
                variant="light"
                color="success"
                onClick={handleDownloadExcel}
                title="Download"
              >
                <BsDownload size={20} />
              </Button>
            )}
          </div>
        </div>
      </div>
    </>
  );
};

export default Header;
