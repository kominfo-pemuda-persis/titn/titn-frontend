import { Key, ReactNode, Suspense, useCallback, useEffect } from "react";
import { usePathname, useRouter, useSearchParams } from "next/navigation";
import { Selection, SortDescriptor } from "@nextui-org/react";
import { useAtom } from "jotai";
import { RESET } from "jotai/utils";

import { Spinner } from "@/components/atoms/Spinner";
import Toast from "@/components/atoms/Toast";
import {
  Table,
  TableBody,
  TableCell,
  TableColumn,
  TableHeader,
  TableRow,
} from "@/components/molecules/Table";
import Footer from "@/components/molecules/Table/Footer";
import Header from "@/components/molecules/Table/Header";
import useRouterParameter from "@/hooks/useRouterParameter";
import { isElementLoading } from "@/states/components";
import { tableStates } from "@/states/table";

const TableTemplate = ({
  ariaLabel = "Table",
  selectionMode = "single",
  isFetching,
  emptyContent,
  renderCell,
  columns,
  items = [],
  onRowAction,
  response,
  statusOptions,
  additionalHeaderContent,
  downloadOptions,
  isCompact = false,
  baseClassName = "max-h-[70vh]",
}: TableTemplateProps) => {
  const router = useRouter();
  const pathname = usePathname();
  const searchParams = useSearchParams();
  const [
    { searchValue, sortDescriptor, totalItems, selectedKeys },
    setTableConfig,
  ] = useAtom(tableStates);
  const { updateQueryString } = useRouterParameter();
  const [isButtonLoading, setButtonLoading] = useAtom(isElementLoading);

  const handleSelectionChange = useCallback(
    (keys: Selection) => {
      setTableConfig((prev) => ({
        ...prev,
        selectedKeys: keys,
      }));
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    []
  );

  const deletePageParameter = useCallback(
    () => {
      const params = new URLSearchParams(searchParams.toString());
      params.delete("page");

      const newParams = params.toString();
      router.replace(pathname + "?" + newParams);
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    []
  );

  const handleSortChange = (e: SortDescriptor) =>
    setTableConfig((prev) => ({
      ...prev,
      sortDescriptor: {
        column: e.column,
        direction: e.direction,
      },
    }));

  const handleStatusChange = useCallback(
    (keys: Selection) => {
      const convertedKeys = Array.from(keys);
      deletePageParameter();
      updateQueryString("filter", convertedKeys[0] as string);

      setTableConfig((prev) => ({
        ...prev,
        statusFilter: keys,
        page: 1,
      }));
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    []
  );

  const onSearchChange = useCallback((value?: string) => {
    if (value) {
      setTableConfig((prev) => ({
        ...prev,
        page: 1,
        searchValue: value,
      }));
    } else {
      setTableConfig((prev) => ({
        ...prev,
        page: 1,
        searchValue: "",
      }));
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const onClear = useCallback(() => {
    setTableConfig((prev) => ({
      ...prev,
      page: 1,
      searchValue: "",
    }));
    deletePageParameter();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [deletePageParameter]);

  const handleDownloadExcel = async () => {
    try {
      setButtonLoading(true);

      if (downloadOptions?.isEnabled) {
        const data = await downloadOptions.getData();

        const header = Object.keys(data[0]);
        const csv = [
          header.join(","),
          ...data.map((row: any) =>
            header.map((fieldName) => row[fieldName]).join(",")
          ),
        ].join("\n");

        const blob = new Blob([csv], { type: "text/csv" });
        const url = URL.createObjectURL(blob);
        const a = document.createElement("a");
        a.href = url;
        a.download = `${downloadOptions?.fileName}.csv`;
        a.click();

        Toast({
          message: "File berhasil diunduh",
          type: "success",
        });
      }
    } catch (error: any) {
      console.log(error);

      Toast({
        message: error.message,
        type: "error",
      });
    } finally {
      setButtonLoading(false);
    }
  };

  const loadingState = isFetching ? "loading" : "idle";

  useEffect(() => {
    if (searchParams.has("page")) {
      setTableConfig((prev) => ({
        ...prev,
        page: Number(searchParams.get("page")),
      }));
    }

    if (searchParams.has("rows")) {
      setTableConfig((prev) => ({
        ...prev,
        rowsPerPage: Number(searchParams.get("rows")),
      }));
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [searchParams]);

  useEffect(() => {
    if (response?.totalPages) {
      setTableConfig((prev) => ({
        ...prev,
        page: response?.number === 0 ? 1 : response?.number + 1,
        pages: response?.totalPages,
        totalItems: response?.totalElements,
      }));
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [response?.number, response?.totalElements, response?.totalPages]);

  useEffect(() => {
    return () => {
      setTableConfig(RESET);
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <>
      <Table
        selectedKeys={selectedKeys}
        onSelectionChange={handleSelectionChange}
        aria-label={ariaLabel}
        classNames={{
          base: baseClassName,
        }}
        topContentPlacement="outside"
        bottomContentPlacement="outside"
        isHeaderSticky
        sortDescriptor={sortDescriptor}
        onSortChange={handleSortChange}
        isStriped
        selectionMode={selectionMode}
        onRowAction={onRowAction}
        topContent={
          <Header
            isLoading={isFetching || isButtonLoading}
            filterValue={searchValue}
            onClear={onClear}
            onSearchChange={onSearchChange}
            handleStatusChange={handleStatusChange}
            statusOptions={statusOptions}
            additionalContents={additionalHeaderContent}
            enableDownload={downloadOptions?.isEnabled}
            handleDownloadExcel={handleDownloadExcel}
          />
        }
        isCompact={isCompact}
        bottomContent={
          <Suspense fallback={null}>
            <Footer
              footerText={isFetching ? "" : `Total ${totalItems} items`}
              isLoading={isFetching}
            />
          </Suspense>
        }
      >
        <TableHeader columns={columns}>
          {(column) => (
            <TableColumn
              key={column.uid}
              align={column.uid === "actions" ? "center" : "start"}
              allowsSorting={column.sortable}
            >
              {column.name}
            </TableColumn>
          )}
        </TableHeader>
        <TableBody
          items={items}
          emptyContent={emptyContent}
          loadingContent={<Spinner />}
          loadingState={loadingState}
        >
          {(item) => (
            <TableRow
              key={item?.id as string}
              className={onRowAction ? "cursor-pointer" : ""}
            >
              {(columnKey) => (
                <TableCell className="text-nowrap">
                  {renderCell(item, columnKey as string)}
                </TableCell>
              )}
            </TableRow>
          )}
        </TableBody>
      </Table>
    </>
  );
};

export default TableTemplate;

interface TableTemplateProps {
  ariaLabel?: string;
  isFetching: boolean;
  emptyContent: string;
  isCompact?: boolean;
  renderCell: (item: any, columnKey: string) => ReactNode;
  columns: {
    uid: string;
    name: string;
    sortable: boolean;
  }[];
  items: any[];
  onRowAction?: (key: Key) => void;
  response: {
    number: number;
    totalElements: number;
    totalPages: number;
  };
  statusFilter?: Selection;
  setStatusFilter?: (keys: Selection) => void;
  statusOptions?: {
    uid: string;
    name: string;
  }[];
  additionalHeaderContent?: ReactNode;
  selectionMode?: "single" | "multiple";
  baseClassName?: string;
  downloadOptions?: {
    fileName: string;
    isEnabled: boolean;
    getData: () => Promise<any[]>;
  };
}
