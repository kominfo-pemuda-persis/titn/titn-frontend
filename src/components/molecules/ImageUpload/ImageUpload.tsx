import {
  ChangeEvent,
  cloneElement,
  ReactElement,
  SVGProps,
  useEffect,
  useRef,
  useState,
} from "react";
import { User } from "@nextui-org/shared-icons";
import { clsx } from "@nextui-org/shared-utils";
import { SlotsToClasses } from "@nextui-org/theme";
import axios from "axios";
import imageCompression from "browser-image-compression";
import { BsTrash3Fill, BsUpload } from "react-icons/bs";

import { Image, ImageProps } from "@/components/atoms/Image";
import { Spinner } from "@/components/atoms/Spinner";
import { UPLOAD_IMAGE } from "@/constant/endpoint";

export type ImageUploadProps = {
  classNames?: SlotsToClasses<"wrapper" | "image">;
  onChange?: (url: string) => void;
  onRemove?: () => void;
  disabled?: boolean;
  value?: string;
  name?: string;
  rounded?: boolean;
  placeholder?: ReactElement;
  category?: "calon_peserta" | "rekap";
} & Pick<ImageProps, "width" | "src">;

const ImagePlaceholder = (props: SVGProps<SVGSVGElement>) => {
  return (
    <User
      className="h-full w-full bg-black text-white"
      fill="currentColor"
      {...props}
    />
  );
};

const ImageUpload = ({
  classNames,
  name,
  value,
  width,
  rounded,
  disabled,
  onChange,
  placeholder,
  category,
}: ImageUploadProps) => {
  useEffect(() => {
    value ? setUrl(encodeURI(value as string)) : setUrl("");
  }, [value]);

  const [loadingState, setLoadingState] = useState<boolean>(false);
  const [url, setUrl] = useState<string>(value ?? "");
  const [error, setError] = useState<string>("");
  const [isMenuDisplay, setIsMenuDisplay] = useState<boolean>(false);
  const inputRef = useRef<HTMLInputElement>(null);
  const imgClick = () => {
    if (disabled) return;

    inputRef.current?.click();
  };

  const imgChangeHandler = async (e: ChangeEvent<HTMLInputElement>) => {
    if (disabled) return;
    setLoadingState(true);
    const file = e.target.files?.[0];
    if (file) {
      const compressedIimage = await imageCompression(file, {
        maxSizeMB: 2,
        maxWidthOrHeight: 1920,
        useWebWorker: true,
        fileType: "image/jpeg",
      });
      const data = new FormData();
      data.append("file", compressedIimage);
      if (category) {
        data.append("category", category);
      }
      try {
        const response = await axios.post(UPLOAD_IMAGE, data);
        if (response.status === 200) {
          if (response.data.imageUrl) {
            setUrl(response.data.imageUrl);
            setError("");
            onChange?.(response.data.imageUrl);
          }
        } else {
          setError(response.data.message ?? "Failed to upload image");
        }
      } catch (error) {
        if (error instanceof axios.AxiosError) {
          setError(error.response?.data.message ?? "Failed to upload image");
        } else {
          setError("Failed to upload image");
        }
      }
    }
    setLoadingState(false);
  };

  return (
    <div>
      <div
        className={clsx(
          "relative w-auto cursor-pointer overflow-hidden",
          rounded && "rounded-full",
          classNames?.wrapper as string,
          error && "rounded border-3 border-red-500 bg-red-50"
        )}
        onMouseOver={() => setIsMenuDisplay(true)}
        onMouseLeave={() => setIsMenuDisplay(false)}
        onClick={() => {
          if (isMenuDisplay || !url) {
            imgClick();
          } else {
            setIsMenuDisplay(true);
          }
        }}
      >
        {url ? (
          <Image
            className={clsx("h-full w-full", classNames?.image as string)}
            alt="image upload"
            src={url}
            width={width}
            classNames={{
              wrapper: "mx-auto",
            }}
          />
        ) : placeholder ? (
          cloneElement(placeholder as ReactElement, {
            className: clsx(
              placeholder?.props?.className,
              "h-full w-full",
              error ?? "bg-red-50 text-red-500",
              classNames?.wrapper as string
            ),
          })
        ) : (
          <ImagePlaceholder
            width={width}
            className={clsx(
              "h-full w-full",
              classNames?.image,
              error ? "bg-red-50 text-red-500" : "bg-black text-white"
            )}
          />
        )}
        {loadingState ? (
          <div
            className={
              "absolute bottom-0 left-0 top-0 z-[20] flex min-h-[100px] w-full items-center justify-center gap-2 bg-gray-600 bg-opacity-60"
            }
          >
            <div className="flex justify-center rounded-full bg-white p-2 text-primary dark:bg-black">
              <Spinner />
            </div>
          </div>
        ) : (
          <>
            <div
              className={
                "absolute bottom-0 left-0 top-0 z-[20] flex min-h-[100px] w-full items-center justify-center gap-2 opacity-0 hover:bg-opacity-20 hover:opacity-100 " +
                (error
                  ? "bg-red-600 bg-opacity-10"
                  : "bg-primary-50 bg-opacity-0")
              }
            >
              {!disabled && (
                <>
                  <div className="rounded bg-white p-2 text-primary">
                    <BsUpload size={20} />
                  </div>
                  {url && (
                    <div
                      className="rounded bg-white p-2 text-red-500"
                      onClick={(e) => {
                        e.stopPropagation();
                        if (disabled) return;
                        if (isMenuDisplay) {
                          setUrl("");
                          setError("");
                        } else {
                          setIsMenuDisplay(true);
                        }
                      }}
                    >
                      <BsTrash3Fill size={20} />
                    </div>
                  )}
                </>
              )}
            </div>
          </>
        )}

        <input
          type="file"
          accept="image/*"
          hidden
          ref={inputRef}
          name={name}
          onChange={imgChangeHandler}
        />
        <input hidden type="text" name={name + "Url"} readOnly value={url} />
      </div>
      {error && <p className="text-xs text-red-500">{error}</p>}
    </div>
  );
};

export default ImageUpload;
