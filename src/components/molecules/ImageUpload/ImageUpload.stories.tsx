import React from "react";
import { Meta } from "@storybook/react";

import ImageUpload, { ImageUploadProps } from "./ImageUpload";

export default {
  title: "Molecules/ImageUpload",
  component: ImageUpload,
  argTypes: {},
} as Meta<typeof ImageUpload>;

const defaultProps = {
  label: "Birth date",
};

const Template = (args: ImageUploadProps) => (
  <div className="h-80 w-80">
    <ImageUpload {...args} />
  </div>
);

export const Default = {
  render: Template,
  args: {
    ...defaultProps,
  },
};

export const RoundedImage = {
  render: Template,
  args: {
    ...defaultProps,
    rounded: true,
  },
};
