// type
import type {
  DropdownMenuProps as NextUiDropdownMenuProps,
  DropdownProps as NextUiDropdownProps,
  DropdownTriggerProps as NextUiDropdownTriggerProps,
} from "@nextui-org/dropdown";
import type { MenuItemProps, MenuSectionProps } from "@nextui-org/menu";

import {
  // component
  Dropdown as NextAutoDropdown,
  DropdownMenu as NextUiDropdownMenu,
  DropdownTrigger as NextUiDropdownTrigger,
  // hook
  useDropdown as nextUiUseDropdown,
} from "@nextui-org/dropdown";
import { MenuItem, MenuSection } from "@nextui-org/menu";

// export type
export interface DropdownProps extends NextUiDropdownProps {}
export interface DropdownTriggerProps extends NextUiDropdownTriggerProps {}
export type DropdownMenuProps<T extends object = object> =
  NextUiDropdownMenuProps<T>;
export type DropdownItemProps<T extends object> = MenuItemProps<T>;
export type DropdownSectionProps<T extends object> = NextUiDropdownMenuProps<T>;

// export component
export const Dropdown = NextAutoDropdown;
export const DropdownTrigger = NextUiDropdownTrigger;
export const DropdownMenu = NextUiDropdownMenu;
export const DropdownItem = MenuItem;
export const DropdownSection = MenuSection;

// export hook
export const useDropdown = nextUiUseDropdown;
