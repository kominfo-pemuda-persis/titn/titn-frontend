import type { ButtonGroupProps as NextUiButtonGroupProps } from "@nextui-org/button";

import { ButtonGroup as NextUiButtonGroup } from "@nextui-org/button";

export type ButtonGroupProps = NextUiButtonGroupProps;
export const ButtonGroup = NextUiButtonGroup;
