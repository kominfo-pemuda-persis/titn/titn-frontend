import { useEffect, useState } from "react";
import { useTheme } from "next-themes";
import { MoonFilledIcon, SunFilledIcon } from "@nextui-org/shared-icons";

import { Button } from "@/components/atoms/Button";
import { Spinner } from "@/components/atoms/Spinner";
import { Theme } from "@/types/utils";

const ThemeSwitcher = () => {
  const { theme, setTheme } = useTheme();
  const [mounted, setMounted] = useState<boolean>(false);

  useEffect(() => setMounted(true), []);

  if (!mounted) return <Spinner />;

  return (
    <>
      {theme === Theme.Dark ? (
        <Button
          radius="full"
          title={theme}
          aria-label="Toggle Light Mode"
          color="secondary"
          isIconOnly
          size="sm"
          onClick={() => setTheme(Theme.Light)}
        >
          <SunFilledIcon />
        </Button>
      ) : (
        <Button
          radius="full"
          title={theme}
          aria-label="Toggle Dark Mode"
          color="secondary"
          isIconOnly
          size="sm"
          onClick={() => setTheme(Theme.Dark)}
        >
          <MoonFilledIcon />
        </Button>
      )}
    </>
  );
};

export default ThemeSwitcher;
