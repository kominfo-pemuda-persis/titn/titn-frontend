import type { NavbarProps as NextUiNavbarProps } from "@nextui-org/navbar";

import {
  Navbar as NextUiNavbar,
  NavbarBrand as NextUiNavbarBrand,
  NavbarContent as NextUiNavbarContent,
  NavbarItem as NextUiNavbarItem,
  NavbarMenu as NextUiNavbarMenu,
  NavbarMenuItem as NextUiNavbarMenuItem,
  NavbarMenuToggle as NextUiNavbarMenuToggle,
} from "@nextui-org/navbar";

export const Navbar = NextUiNavbar;
export const NavbarBrand = NextUiNavbarBrand;
export const NavbarContent = NextUiNavbarContent;
export const NavbarItem = NextUiNavbarItem;
export const NavbarMenuToggle = NextUiNavbarMenuToggle;
export const NavbarMenu = NextUiNavbarMenu;
export const NavbarMenuItem = NextUiNavbarMenuItem;
export type NavbarProps = NextUiNavbarProps;
