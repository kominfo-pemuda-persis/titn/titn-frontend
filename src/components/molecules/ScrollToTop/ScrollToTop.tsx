"use client";

import { forwardRef, Ref, useEffect, useRef, useState } from "react";
import { BsArrowUp } from "react-icons/bs";

import { Button } from "@/components/atoms/Button";
import { Spinner } from "@/components/atoms/Spinner";
import smoothScroll from "@/utils/smoothScroll";

const ScrollToTop = (
  { target }: { target: string },
  ref: Ref<HTMLDivElement>
) => {
  const scrollButtonRef = useRef<HTMLButtonElement>(null);
  const [mounted, setMounted] = useState<boolean>(false);
  const [isScrolled, setIsScrolled] = useState<boolean>(false);

  useEffect(() => setMounted(true), []);

  useEffect(() => {
    if (!ref) return;

    const containerElement = "current" in ref ? ref.current : null;

    const handleScroll = () => {
      if (containerElement!?.scrollTop > 0) {
        setIsScrolled(true);
      } else {
        setIsScrolled(false);
      }
    };

    containerElement?.addEventListener("scroll", handleScroll);

    return () => {
      containerElement?.removeEventListener("scroll", handleScroll);
    };
  }, [ref]);

  if (!mounted) return <Spinner className="absolute bottom-8 right-8" />;

  return (
    <>
      {isScrolled ? (
        <Button
          ref={scrollButtonRef}
          variant="shadow"
          color="primary"
          className="absolute bottom-8 right-8 animate-appearance-in"
          isIconOnly
          radius="full"
          onClick={(e) => smoothScroll(e, target)}
        >
          <BsArrowUp size={20} />
        </Button>
      ) : null}
    </>
  );
};

export default forwardRef(ScrollToTop);
