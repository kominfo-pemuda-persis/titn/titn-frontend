import React from "react";
import Image from "next/image";
import { Meta } from "@storybook/react";

import { Carousel } from "@/components/molecules/Carousel";

export default {
  title: "Molecules/Carousel",
  component: Carousel,
  argTypes: {},
} as Meta<typeof Carousel>;

const defaultProps = {};

const carouselSlides = [
  {
    src: "https://random.imagecdn.app/1280/500",
    alt: "gallery-1",
  },
  {
    src: "https://random.imagecdn.app/1280/501",
    alt: "gallery-2",
  },
  {
    src: "https://random.imagecdn.app/1280/502",
    alt: "gallery-3",
  },
  {
    src: "https://random.imagecdn.app/1280/503",
    alt: "gallery-4",
  },
];

const Template = () => (
  <Carousel autoSlideInterval={3000} className="max-w-5xl">
    {carouselSlides.map((slide, key) => (
      <Image
        key={key}
        alt={slide.alt}
        src={slide.src}
        width={1280}
        height={500}
        className="w-full bg-slate-500 object-fill object-center"
      />
    ))}
  </Carousel>
);

export const Default = {
  render: Template,
  args: {
    ...defaultProps,
  },
};
