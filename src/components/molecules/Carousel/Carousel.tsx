"use client";

import { ReactNode, useCallback, useEffect, useState } from "react";
import { ArrowLeftIcon } from "@nextui-org/shared-icons";

import { Button } from "@/components/atoms/Button";

export interface CarouselProps {
  children: ReactNode[];
  autoSlide?: boolean;
  autoSlideInterval?: number;
  className?: string;
}

const Carousel = ({
  children: slides,
  autoSlide = false,
  autoSlideInterval = 3000,
  className = "",
}: CarouselProps) => {
  const [curr, setCurr] = useState<number>(0);

  const prev = () =>
    setCurr((curr) => (curr === 0 ? slides?.length - 1 : curr - 1));

  const next = useCallback(
    () => setCurr((curr) => (curr === slides?.length - 1 ? 0 : curr + 1)),
    [slides?.length]
  );

  useEffect(() => {
    if (!autoSlide) return;
    const slideInterval = setInterval(next, autoSlideInterval);
    return () => clearInterval(slideInterval);
  }, [autoSlide, autoSlideInterval, next]);

  return (
    <div className={`relative overflow-hidden ${className}`}>
      <div
        className="flex w-full transition-transform duration-500 ease-out"
        style={{ transform: `translateX(-${curr * 100}%)` }}
      >
        {slides}
      </div>
      <div
        className="absolute inset-0 flex items-center justify-between p-4"
        onTouchStart={(e) => {
          const x = e.touches[0].clientX;
          const touchMove = (e: TouchEvent) => {
            const dx = x - e.touches[0].clientX;
            if (dx > 50) {
              next();
            } else if (dx < -50) {
              prev();
            }
          };
          document.addEventListener("touchmove", touchMove);
          document.addEventListener("touchend", () => {
            document.removeEventListener("touchmove", touchMove);
          });
        }}
      >
        <Button
          title="Previous Slide"
          role="button"
          isIconOnly
          onClick={prev}
          color="secondary"
          className="flex items-center rounded-full bg-opacity-50 shadow-xl hover:bg-slate-500"
        >
          <ArrowLeftIcon fontSize={25} />
        </Button>
        <Button
          title="Next Slide"
          role="button"
          isIconOnly
          onClick={next}
          color="secondary"
          className="flex items-center rounded-full bg-opacity-50 shadow-xl hover:bg-slate-500"
        >
          <ArrowLeftIcon fontSize={25} className="rotate-180" />
        </Button>
      </div>
      <div className="absolute bottom-4 left-0 right-0">
        <div className="flex items-center justify-center gap-2">
          {slides?.map((_, i) => (
            <div
              key={i}
              className={`h-2 w-2 rounded-full bg-secondary shadow-lg transition-all md:h-4 md:w-4 ${curr === i ? "p-0.5" : "bg-opacity-50"}`}
            />
          ))}
        </div>
      </div>
    </div>
  );
};

export { Carousel };
