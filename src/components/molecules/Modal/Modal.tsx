import type {
  ModalBodyProps as NextUiModalBodyProps,
  ModalContentProps as NextUiModalContentProps,
  ModalFooterProps as NextUiModalFooterProps,
  ModalHeaderProps as NextUiModalHeaderProps,
  ModalProps as NextUiModalProps,
} from "@nextui-org/modal";

import {
  Modal as NextUiModal,
  ModalBody as NextUiModalBody,
  ModalContent as NextUiModalContent,
  ModalFooter as NextUiModalFooter,
  ModalHeader as NextUiModalHeader,
  useModal as NextUiUseModal,
} from "@nextui-org/modal";

export const Modal = NextUiModal;
export const ModalContent = NextUiModalContent;
export const ModalBody = NextUiModalBody;
export const ModalFooter = NextUiModalFooter;
export const ModalHeader = NextUiModalHeader;
export const useModal = NextUiUseModal;

export type ModalProps = NextUiModalProps;
export type ModalContentProps = NextUiModalContentProps;
export type ModalBodyProps = NextUiModalBodyProps;
export type ModalFooterProps = NextUiModalFooterProps;
export type ModalHeaderProps = NextUiModalHeaderProps;
