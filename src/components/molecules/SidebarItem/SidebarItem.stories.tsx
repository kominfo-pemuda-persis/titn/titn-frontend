import React from "react";
import { Meta, StoryObj } from "@storybook/react";
import { FaChevronRight } from "react-icons/fa6";

import { SidebarItem } from "./SidebarItem";

export default {
  title: "Molecules/SidebarItem",
  component: SidebarItem,
  parameters: {
    // Optional parameter to center the component in the Canvas. More info: https://storybook.js.org/docs/configure/story-layout
    layout: "centered",
  },
} as Meta<typeof SidebarItem>;

type Story = StoryObj<typeof SidebarItem>;

export const DefaultSidebarMenu: Story = {
  args: {
    title: "Menu",
    icon: FaChevronRight,
    isActive: false,
  },
};

export const ActiveSidebarMenu: Story = {
  args: {
    title: "Menu Active",
    icon: FaChevronRight,
    isActive: true,
  },
};
