import type { IconType } from "react-icons";

import NextLink from "next/link";
import { Tooltip } from "@nextui-org/react";
import clsx from "clsx";

interface SidebarItemProps {
  title: string;
  icon: IconType;
  isActive?: boolean;
  href?: string;
  collapsed?: boolean;
  handleClick?: () => void;
}

export const SidebarItem = ({
  icon,
  title,
  isActive,
  href = "",
  collapsed = false,
  handleClick,
}: SidebarItemProps) => {
  const Icon = icon;

  return (
    <NextLink
      href={href}
      onClick={handleClick}
      className="flex max-w-full  text-default-900 active:bg-none md:block"
    >
      <Tooltip
        content={title}
        placement="right"
        hidden={collapsed}
        className="hidden md:block"
        color="primary"
      >
        <div
          className={clsx(
            isActive
              ? "bg-primary-100 [&_svg_path]:fill-primary-500"
              : "hover:bg-default-100",
            collapsed ? "md:flex-row" : "md:flex-col md:justify-center",
            "flex h-full cursor-pointer flex-col items-center gap-2 rounded-xl px-3.5 py-2 transition-all duration-150 active:scale-[0.98] md:min-h-[44px] md:w-full md:py-0"
          )}
        >
          <Icon />
          <span
            className={clsx(
              collapsed ? "md:block" : "md:hidden",
              "text-xs text-default-900 md:text-sm"
            )}
          >
            {title}
          </span>
        </div>
      </Tooltip>
    </NextLink>
  );
};
