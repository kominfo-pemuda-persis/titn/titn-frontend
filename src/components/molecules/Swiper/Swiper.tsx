"use client";

import {
  Swiper as SwiperContainer,
  SwiperProps,
  SwiperSlide,
} from "swiper/react";

import "swiper/css";

import Image from "next/image";

export interface SwiperContainerProps extends SwiperProps {
  slides: {
    src: string;
    alt: string;
  }[];
}

const Swiper = ({
  slides,
  effect,
  grabCursor,
  modules,
}: SwiperContainerProps) => {
  return (
    <SwiperContainer
      effect={effect}
      grabCursor={grabCursor}
      modules={modules}
      className="h-full max-h-[400px] w-full min-w-[200px] max-w-2xl"
    >
      {slides.map((slide, key) => (
        <SwiperSlide key={key} className="rounded-large">
          <Image
            alt={slide.alt}
            src={slide.src}
            width="0"
            height="0"
            quality={85}
            sizes="(max-width: 640px) 100vw, (max-width: 768px) 100vw, (max-width: 1024px) 100vw, 100vw"
            className="h-full w-full bg-slate-500 object-cover object-center shadow-xl"
          />
        </SwiperSlide>
      ))}
    </SwiperContainer>
  );
};

export default Swiper;
