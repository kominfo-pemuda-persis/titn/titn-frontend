"use client";

import { useRef } from "react";
import dynamic from "next/dynamic";

import ScrollToTop from "@/components/molecules/ScrollToTop/ScrollToTop";
import SnapContainer from "@/components/organisms/SnapContainer/SnapContainer";

const TimelineSection = dynamic(
  () => import("@/components/organisms/HomeSection/TimelineSection"),
  {
    ssr: false,
  }
);
const AboutSection = dynamic(
  () => import("@/components/organisms/HomeSection/AboutSection"),
  {
    ssr: false,
  }
);
const FAQSection = dynamic(
  () => import("@/components/organisms/HomeSection/FAQSection"),
  {
    ssr: false,
  }
);
const GallerySection = dynamic(
  () => import("@/components/organisms/HomeSection/GallerySection"),
  {
    ssr: false,
  }
);
const LastTITNSection = dynamic(
  () => import("@/components/organisms/HomeSection/LastTITNSection"),
  {
    ssr: false,
  }
);
const HeroSection = dynamic(
  () => import("@/components/organisms/HomeSection/HeroSection"),
  {
    ssr: false,
  }
);
const KompetisiSection = dynamic(
  () => import("@/components/organisms/HomeSection/KompetisiSection"),
  {
    ssr: false,
  }
);
const LokasiSection = dynamic(
  () => import("@/components/organisms/HomeSection/LokasiSection"),
  {
    ssr: false,
  }
);
const SocialSection = dynamic(
  () => import("@/components/organisms/HomeSection/SocialSection"),
  {
    ssr: false,
  }
);
const SponsorSection = dynamic(
  () => import("@/components/organisms/HomeSection/SponsorSection"),
  {
    ssr: false,
  }
);

const HomepageContainer = () => {
  const containerRef = useRef<HTMLDivElement>(null);

  return (
    <>
      <SnapContainer
        ref={containerRef}
        items={[
          <HeroSection key="hero-section" />,
          <AboutSection key="about-section" />,
          <TimelineSection key="timeline-section" />,
          <KompetisiSection key="kompetisi-section" />,
          <LastTITNSection key="last-titn-section" />,
          <GallerySection key="gallery-section" />,
          <LokasiSection key="location-section" />,
          <FAQSection key="faq-section" />,
          <SponsorSection key="sponsor-section" />,
          <SocialSection key="contact-section" />,
        ]}
      />

      <ScrollToTop ref={containerRef} target="#hero" />
    </>
  );
};

export { HomepageContainer };
