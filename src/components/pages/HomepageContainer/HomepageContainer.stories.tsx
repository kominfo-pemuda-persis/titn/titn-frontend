import { Meta } from "@storybook/react";

import { HomepageContainer } from "@/components/pages/HomepageContainer";
import Providers from "@/providers";

export default {
  title: "pages/Home",
  component: HomepageContainer,
  argTypes: {},
} as Meta<typeof HomepageContainer>;

export const Default = () => (
  <Providers>
    <HomepageContainer />
  </Providers>
);
