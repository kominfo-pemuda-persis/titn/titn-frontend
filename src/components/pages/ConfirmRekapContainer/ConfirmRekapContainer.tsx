"use client";

import { Card, CardBody, CardHeader } from "@/components/atoms/Card";
import { Divider } from "@/components/atoms/Divider";
import { Image } from "@/components/atoms/Image";
import { Paragraph, Subtitle, Title } from "@/components/atoms/Text";
import FormRekapWrapper from "@/components/organisms/FormRekapWrapper/FormRekapWrapper";

export const ConfirmRekapContainer = () => {
  return (
    <div className="mx-auto my-2 max-w-5xl">
      <Card shadow="sm">
        <CardHeader className="flex flex-col items-center space-y-2 text-center">
          <Title
            text="Formulir Konfirmasi Calon Peserta TITN"
            className="font-bold text-primary"
          />
          <Subtitle
            text="Silahkan isi formulir dibawah ini untuk mengkonfirmasi calon peserta TITN"
            className="!text-medium text-secondary"
          />
          <Divider />

          <div className="flex flex-col items-center gap-1">
            <Paragraph
              text="(Pastikan sudah melakukan pembayaran sebesar 200rb/peserta)"
              className="!text-sm text-slate-500"
            />
            <div className="flex gap-2">
              <Image
                src="/assets/BRI_2020.png"
                alt="Logo BRI"
                radius="none"
                width={100}
                className="mx-auto"
              />
              <div className="!text-left">
                <Paragraph
                  text="Atas nama Hendi Santika"
                  className="!text-sm"
                />
                <Paragraph
                  text={
                    <span className="flex w-full items-center space-x-1">
                      <span>3772</span>
                      <span>0102</span>
                      <span>1521</span>
                      <span>502</span>
                    </span>
                  }
                  className="!text-sm font-bold italic"
                />
              </div>
            </div>
          </div>
        </CardHeader>

        <Divider />

        <CardBody>
          <FormRekapWrapper />
        </CardBody>
      </Card>
    </div>
  );
};

export default ConfirmRekapContainer;
