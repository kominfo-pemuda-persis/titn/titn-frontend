"use client";

import { Key, ReactNode, useEffect, useMemo, useState } from "react";
import dynamic from "next/dynamic";
import { usePathname, useRouter, useSearchParams } from "next/navigation";
import { useAtomValue } from "jotai";
import { BsPencil, BsPlusLg } from "react-icons/bs";

import { Button } from "@/components/atoms/Button";
import { Divider } from "@/components/atoms/Divider";
import { Link } from "@/components/atoms/Link";
import { Subtitle, Title } from "@/components/atoms/Text";
import { Tab, Tabs } from "@/components/molecules/Tabs/Tabs";
import { isElementLoading } from "@/states/components";

const TableCalonPeserta = dynamic(
  () => import("@/components/organisms/TableCalonPeserta/TableCalonPeserta"),
  {
    ssr: false,
    suspense: true,
  }
);

const TableCalonPesertaNonAnggota = dynamic(
  () =>
    import(
      "@/components/organisms/TableCalonPesertaNonAnggota/TableCalonPesertaNonAnggota"
    ),
  {
    ssr: false,
    suspense: true,
  }
);

enum ActiveTabs {
  ANGGOTA = "anggota",
  NON_ANGGOTA = "non-anggota",
}

export const CalonPesertaContainer = () => {
  const router = useRouter();
  const pathname = usePathname();
  const searchParams = useSearchParams();
  const isLoading = useAtomValue(isElementLoading);

  const [activeTab, setActiveTab] = useState<ActiveTabs>(ActiveTabs.ANGGOTA);

  let tabs: {
    id: ActiveTabs.ANGGOTA | ActiveTabs.NON_ANGGOTA;
    label: string;
    content?: ReactNode;
  }[] = useMemo(
    () => [
      {
        id: ActiveTabs.ANGGOTA,
        label: "Anggota",
        content: <TableCalonPeserta key={ActiveTabs.ANGGOTA} />,
      },
      {
        id: ActiveTabs.NON_ANGGOTA,
        label: "Partisipan Non-Anggota",
        content: <TableCalonPesertaNonAnggota key={ActiveTabs.NON_ANGGOTA} />,
      },
    ],
    []
  );

  const handleClick = (id: Key) => {
    const params = new URLSearchParams(searchParams.toString());
    params.set("tab", id as string);
    params.delete("page");

    const newParams = params.toString();

    router.replace(pathname + "?" + newParams);
  };

  useEffect(() => {
    if (searchParams.has("tab")) {
      const page = searchParams.get("tab") as ActiveTabs;
      setActiveTab(page);
    }
  }, [searchParams]);

  return (
    <div className="space-y-4">
      <div className="flex flex-col items-start justify-between gap-4 xl:flex-row xl:items-center">
        <div>
          <Title text="List Calon Peserta" className="text-primary" />
          <Subtitle
            text="List calon peserta yang mendaftar pada event TITN"
            className="!text-sm"
          />
        </div>

        <div className="flex flex-wrap gap-4">
          <Button
            startContent={!isLoading && <BsPlusLg size={15} />}
            size="sm"
            color="primary"
            href="/calon-peserta/add"
            as={Link}
            variant="shadow"
            isLoading={isLoading}
          >
            Tambah
          </Button>
          <Button
            startContent={!isLoading && <BsPencil size={15} />}
            size="sm"
            color="primary"
            href="/calon-peserta/add-rekap"
            as={Link}
            variant="shadow"
            isLoading={isLoading}
          >
            Buat Rekap
          </Button>
        </div>
      </div>

      <Divider />

      <Tabs
        defaultSelectedKey={activeTab}
        selectedKey={activeTab}
        aria-label="Dynamic tabs"
        variant="underlined"
        color="primary"
        items={tabs}
        onSelectionChange={handleClick}
        isDisabled={isLoading}
        className="!mt-0"
      >
        {(item) => (
          <Tab key={item.id} title={item.label}>
            {item.content}
          </Tab>
        )}
      </Tabs>
    </div>
  );
};
