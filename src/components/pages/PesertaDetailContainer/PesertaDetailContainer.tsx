"use client";

import { useCookies } from "next-client-cookies";
import { useQuery } from "@tanstack/react-query";

import Details from "@/components/organisms/Participant/Details";
import { getParticipantById } from "@/services/query/participant";
import { Participant } from "@/types/participant";

export const PesertaDetailContainer = ({ id }: { id: string }) => {
  const cookie = useCookies();
  const session = cookie?.get("session");

  const { isLoading, isError, data, error, refetch } = useQuery({
    queryKey: [id],
    queryFn: async () => await getParticipantById(id, session),
  });

  const participants: Participant = data?.data;

  if (isLoading) {
    return <span>Loading...</span>;
  }

  if (isError) {
    return <span>Error: {error.message}</span>;
  }

  return <Details participants={participants} callback={refetch} />;
};

export default PesertaDetailContainer;
