import { BsDownload } from "react-icons/bs";

import { Button } from "@/components/atoms/Button";
import { Card, CardBody, CardHeader } from "@/components/atoms/Card";
import { Divider } from "@/components/atoms/Divider";
import { SectionTitle, Subtitle, Title } from "@/components/atoms/Text";
import { Container } from "@/components/template/Container";

const DokumenContainer = () => {
  const documentList = [
    {
      name: "Ketentuan Lomba",
      src: "/assets/pdf/ketentuan-lomba.pdf",
    },
    {
      name: "Guideline TITN",
      src: "/assets/pdf/guideline.pdf",
    },
  ];

  return (
    <Container className="space-y-4">
      <Card>
        <CardHeader className="flex-col items-start">
          <div>
            <Title className="text-primary" text="Dokumen TITN" />
            <Subtitle
              text="List dokumen TITN untuk keperluan lomba dan peserta"
              className="!text-sm"
            />
          </div>
        </CardHeader>

        <Divider />

        <CardBody className="items-center justify-center">
          <div className="mx-auto flex w-full flex-wrap items-center justify-center gap-4 py-6">
            {documentList.map((document, index) => (
              <Card
                key={index}
                className="min-w-[200px] cursor-pointer"
                isHoverable
              >
                <CardHeader className="pb-0">
                  <SectionTitle
                    text={document.name}
                    className="w-full text-center text-secondary"
                  />
                </CardHeader>
                <CardBody>
                  <Button
                    startContent={<BsDownload />}
                    size="sm"
                    color="primary"
                    variant="ghost"
                  >
                    <a href={document.src} download>
                      Download
                    </a>
                  </Button>
                </CardBody>
              </Card>
            ))}
          </div>
        </CardBody>
      </Card>
    </Container>
  );
};

export default DokumenContainer;
