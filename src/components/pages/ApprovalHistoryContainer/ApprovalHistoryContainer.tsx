import { Suspense } from "react";

import { Divider } from "@/components/atoms/Divider";
import { Subtitle, Title } from "@/components/atoms/Text";
import TableApprovalHistory from "@/components/organisms/TableApprovalHistory/TableApprovalHistory";

export const ApprovalHistoryContainer = () => {
  return (
    <div className="space-y-4">
      <div>
        <Title text="History Penyetujuan" className="text-primary" />
        <Subtitle
          text="Daftar Penerimaan pengajuan peserta TITN"
          className="!text-sm"
        />
      </div>

      <Divider />

      <Suspense fallback={null}>
        <TableApprovalHistory />
      </Suspense>
    </div>
  );
};
