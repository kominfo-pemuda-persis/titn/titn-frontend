"use client";

import { Key, ReactNode, useEffect, useMemo, useState } from "react";
import dynamic from "next/dynamic";
import { usePathname, useRouter, useSearchParams } from "next/navigation";

import { Divider } from "@/components/atoms/Divider";
import { Subtitle, Title } from "@/components/atoms/Text";
import { Tab, Tabs } from "@/components/molecules/Tabs/Tabs";

const TablePeserta = dynamic(
  () => import("@/components/organisms/TablePeserta/TablePeserta"),
  {
    ssr: false,
  }
);

const TablePesertaNonAnggota = dynamic(
  () =>
    import(
      "@/components/organisms/TablePesertaNonAnggota/TablePesertaNonAnggota"
    ),
  {
    ssr: false,
  }
);

enum ActiveTabs {
  ANGGOTA = "anggota",
  NON_ANGGOTA = "non-anggota",
}

export const PesertaContainer = () => {
  const router = useRouter();
  const pathname = usePathname();
  const searchParams = useSearchParams();

  const [activeTab, setActiveTab] = useState<ActiveTabs>(ActiveTabs.ANGGOTA);

  let tabs: {
    id: ActiveTabs.ANGGOTA | ActiveTabs.NON_ANGGOTA;
    label: string;
    content?: ReactNode;
  }[] = useMemo(
    () => [
      {
        id: ActiveTabs.ANGGOTA,
        label: "Anggota",
        content: <TablePeserta key={ActiveTabs.ANGGOTA} />,
      },
      {
        id: ActiveTabs.NON_ANGGOTA,
        label: "Partisipan Non-Anggota",
        content: <TablePesertaNonAnggota key={ActiveTabs.NON_ANGGOTA} />,
      },
    ],
    []
  );

  const handleClick = (id: Key) => {
    const params = new URLSearchParams(searchParams.toString());
    params.set("tab", id as string);
    params.delete("page");

    const newParams = params.toString();

    router.replace(pathname + "?" + newParams);
  };

  useEffect(() => {
    if (searchParams.has("tab")) {
      const page = searchParams.get("tab") as ActiveTabs;
      setActiveTab(page);
    }
  }, [searchParams]);

  return (
    <div className="space-y-4">
      <div>
        <Title text="List Peserta" className="text-primary" />
        <Subtitle
          text="List peserta yang terdaftar pada event TITN"
          className="!text-sm"
        />
      </div>

      <Divider />

      <Tabs
        defaultSelectedKey={activeTab}
        selectedKey={activeTab}
        aria-label="Dynamic tabs"
        variant="underlined"
        color="primary"
        items={tabs}
        onSelectionChange={handleClick}
        className="!mt-0"
      >
        {(item) => (
          <Tab key={item.id} title={item.label}>
            {item.content}
          </Tab>
        )}
      </Tabs>
    </div>
  );
};

export default PesertaContainer;
