"use client";

import { Card, CardBody, CardHeader } from "@/components/atoms/Card";
import { Divider } from "@/components/atoms/Divider";
import { Subtitle, Title } from "@/components/atoms/Text";
import FormSyahadahWrapper from "@/components/organisms/FormSyahadahWrapper/FormSyahadahWrapper";

export const ConfirmSyahadahContainer = () => {
  return (
    <div className="mx-auto my-2 max-w-5xl">
      <Card shadow="sm">
        <CardHeader className="flex flex-col items-center space-y-2 text-center">
          <Title
            text="Upload Daftar Penerima"
            className="font-bold text-primary"
          />
          <Subtitle
            text="Silahkan Upload Daftar Penerima E-Syahadah untuk Juara TITN"
            className="!text-medium text-secondary"
          />
        </CardHeader>

        <Divider />

        <CardBody>
          <FormSyahadahWrapper />
        </CardBody>
      </Card>
    </div>
  );
};

export default ConfirmSyahadahContainer;
