import { Card, CardBody, CardHeader } from "@/components/atoms/Card";
import { Divider } from "@/components/atoms/Divider";
import { Subtitle, Title } from "@/components/atoms/Text";
import PendaftaranWrapper from "@/components/organisms/PendaftaranWrapper/PendaftaranWrapper";
import { FullScreenContainer } from "@/components/template/Container";

export const DaftarContainer = () => {
  return (
    <FullScreenContainer>
      <Card shadow="sm" className="my-16">
        <CardHeader className="flex flex-col items-center space-y-2 text-center">
          <Title
            text="Formulir Pendaftaran Calon Peserta TITN"
            className="font-bold text-primary"
          />
          <Subtitle
            text="Silahkan isi formulir"
            className="!text-medium text-secondary"
          />
        </CardHeader>

        <Divider />

        <CardBody>
          <PendaftaranWrapper />
        </CardBody>
      </Card>
    </FullScreenContainer>
  );
};

export default DaftarContainer;
