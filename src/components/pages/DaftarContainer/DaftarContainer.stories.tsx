import React from "react";
import { Meta } from "@storybook/react";

import Providers from "@/providers";
import { DaftarContainer } from "./DaftarContainer";

export default {
  title: "Pages/Daftar",
  component: DaftarContainer,
  argTypes: {},
} as Meta<typeof DaftarContainer>;

export const DefaultPage = () => (
  <Providers>
    <DaftarContainer />
  </Providers>
);
