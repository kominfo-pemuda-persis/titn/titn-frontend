import { Card, CardBody, CardHeader } from "@/components/atoms/Card";
import { Divider } from "@/components/atoms/Divider";
import { Subtitle, Title } from "@/components/atoms/Text";
import { PendaftaranPengunjung } from "@/components/organisms/PendaftaranPengunjung";
import { FullScreenContainer } from "@/components/template/Container";

export const DaftarPengunjung = () => {
  return (
    <FullScreenContainer>
      <Card shadow="sm" className="my-16 w-full min-w-max max-w-2xl">
        <CardHeader className="flex flex-col items-center space-y-2 p-8 text-center">
          <Title
            text="Pendaftaran Pengunjung TITN"
            className="font-bold text-primary"
          />
          <Subtitle
            text="Silahkan isi formulir"
            className="!text-medium text-secondary"
          />
        </CardHeader>

        <Divider />

        <CardBody className="p-8">
          <PendaftaranPengunjung />
        </CardBody>
      </Card>
    </FullScreenContainer>
  );
};

export default DaftarPengunjung;
