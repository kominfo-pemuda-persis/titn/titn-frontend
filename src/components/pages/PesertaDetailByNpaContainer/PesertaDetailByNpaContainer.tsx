"use client";

import { useCookies } from "next-client-cookies";
import { useQuery } from "@tanstack/react-query";

import { Spinner } from "@/components/atoms/Spinner";
import Details from "@/components/organisms/Participant/Details";
import { getParticipantByNpa } from "@/services/query/participant";
import { Participant } from "@/types";

const PesertaDetailByNpaContainer = ({ npa }: { npa: string }) => {
  const cookie = useCookies();
  const session = cookie?.get("session");

  const { isLoading, isError, data, error, refetch } = useQuery({
    queryKey: [npa],
    queryFn: async () => await getParticipantByNpa(npa, session),
  });

  const participants: Participant = data?.data;

  if (isLoading) {
    return (
      <div className="flex w-full justify-center">
        <Spinner size="lg" color="primary" />
      </div>
    );
  }

  if (isError) {
    return <></>;
  }

  return <Details participants={participants} callback={refetch} />;
};

export default PesertaDetailByNpaContainer;
