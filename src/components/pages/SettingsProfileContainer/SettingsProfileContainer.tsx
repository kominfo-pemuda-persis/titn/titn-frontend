"use client";

import { useEffect, useState } from "react";
import { useAtomValue } from "jotai";

import { Card, CardBody, CardHeader } from "@/components/atoms/Card";
import { Chip } from "@/components/atoms/Chip";
import { Divider } from "@/components/atoms/Divider";
import Loader from "@/components/atoms/Loader/Loader";
import { Title } from "@/components/atoms/Text";
import PesertaDetailByNpaContainer from "@/components/pages/PesertaDetailByNpaContainer/PesertaDetailByNpaContainer";
import { currentUser } from "@/states/user";

const SettingsProfileContainer = () => {
  const { user } = useAtomValue(currentUser);

  const [mounted, setMounted] = useState<boolean>(false);

  useEffect(() => {
    user && setMounted(true);
  }, [user]);

  if (!mounted) return <Loader />;

  return (
    <Card>
      <CardHeader className="flex items-center justify-between gap-3 ">
        <div className="space-y-2">
          <Title text={`Halo, ${user?.nama}!`} />
          <div className="flex flex-wrap gap-2">
            <Chip variant="shadow" className="bg-green-500 text-white">
              {user?.npa}
            </Chip>
            <Chip color="primary" variant="dot">
              {user?.statusAktif}
            </Chip>
            {user?.role.map((role, index) => (
              <Chip color="primary" variant="dot" key={index}>
                {role.displayName}
              </Chip>
            ))}
          </div>
        </div>
      </CardHeader>

      <Divider />

      <CardBody>
        <PesertaDetailByNpaContainer npa={user?.npa as string} />
      </CardBody>
    </Card>
  );
};

export default SettingsProfileContainer;
