import { Suspense } from "react";
import { BsUpload } from "react-icons/bs";

import { Button } from "@/components/atoms/Button";
import { Divider } from "@/components/atoms/Divider";
import { Link } from "@/components/atoms/Link";
import { Subtitle, Title } from "@/components/atoms/Text";
import TableRekap from "@/components/organisms/TableRekap/TableRekap";

const ListRekapContainer = () => {
  return (
    <div className="space-y-4">
      <div className="flex flex-col items-start justify-between gap-4 xl:flex-row xl:items-center">
        <div>
          <Title text="Rekap Pengajuan Peserta" className="text-primary" />
          <Subtitle
            text="List rekap pengajuan peserta TITN"
            className="!text-sm"
          />
        </div>

        <div className="flex flex-wrap gap-4">
          <Button
            startContent={<BsUpload size={15} />}
            size="sm"
            color="primary"
            as={Link}
            href="/rekap/upload-syahadah"
            variant="shadow"
          >
            Upload E-Syahadah
          </Button>
        </div>
      </div>

      <Divider />

      <Suspense fallback={null}>
        <TableRekap />
      </Suspense>
    </div>
  );
};

export default ListRekapContainer;
