"use client";

import { useEffect, useState } from "react";
import { useAtomValue } from "jotai/react";

import { Card, CardHeader } from "@/components/atoms/Card";
import Loader from "@/components/atoms/Loader/Loader";
import { Subtitle, Title } from "@/components/atoms/Text";
import StatisticList from "@/components/organisms/Statistic/StatisticList";
import PesertaDetailByNpaContainer from "@/components/pages/PesertaDetailByNpaContainer/PesertaDetailByNpaContainer";
import { Container } from "@/components/template/Container";
import { currentUser, permission } from "@/states/user";

export const DashboardContainer = () => {
  const { user } = useAtomValue(currentUser);
  const { currentRole } = useAtomValue(permission);

  const [mounted, setMounted] = useState<boolean>(false);

  useEffect(() => {
    user && setMounted(true);
  }, [user]);

  if (!mounted) return <Loader />;

  return (
    <Container className="space-y-4">
      <Card>
        <CardHeader className="flex-col items-start">
          <Subtitle text={`Selamat datang,`} className="!text-sm" />
          <Title className="font-bold text-primary" text={user?.nama} />
        </CardHeader>
      </Card>

      {!currentRole?.isAdmin ? (
        <PesertaDetailByNpaContainer npa={user?.npa as string} />
      ) : (
        <StatisticList />
      )}
    </Container>
  );
};

export default DashboardContainer;
