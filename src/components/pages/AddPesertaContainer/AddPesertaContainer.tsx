"use client";

import { Card, CardBody, CardHeader } from "@/components/atoms/Card";
import { Divider } from "@/components/atoms/Divider";
import { Subtitle, Title } from "@/components/atoms/Text";
import PendaftaranWrapper from "@/components/organisms/PendaftaranWrapper/PendaftaranWrapper";

const AddPesertaContainer = () => (
  <div className="mx-auto my-2 max-w-2xl">
    <Card shadow="sm">
      <CardHeader className="flex flex-col items-center space-y-2 text-center">
        <Title
          text="Formulir Pendaftaran Calon Peserta TITN"
          className="font-bold text-primary"
        />
        <Subtitle
          text="Silahkan isi formulir dibawah"
          className="!text-medium text-secondary"
        />
      </CardHeader>

      <Divider />

      <CardBody>
        <PendaftaranWrapper />
      </CardBody>
    </Card>
  </div>
);

export default AddPesertaContainer;
