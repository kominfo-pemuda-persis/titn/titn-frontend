import { Metadata } from "next";

import { Card, CardBody, CardHeader } from "@/components/atoms/Card";
import { Divider } from "@/components/atoms/Divider";
import { Subtitle, Title } from "@/components/atoms/Text";
import SpinnerForm from "@/components/organisms/SpinnerForm";

export const metadata: Metadata = {
  title: "Spinner Peserta",
};

export const SpinnerContainer = () => {
  return (
    <Card>
      <CardHeader className="flex-col space-y-2 py-6 text-center">
        <Title text="Spinner" className="font-semibold text-primary" />
        <Subtitle
          text="Silahkan pilih metode spinner dibawah ini"
          className="!text-sm font-light text-secondary "
        />
      </CardHeader>

      <Divider />

      <CardBody>
        <SpinnerForm />
      </CardBody>
    </Card>
  );
};

export default SpinnerContainer;
