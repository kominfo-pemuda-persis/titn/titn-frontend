import React from "react";
import { Meta } from "@storybook/react";

import Providers from "@/providers";
import { CheckinContainer } from "./CheckinContainer";

export default {
  title: "Pages/CheckIn",
  component: CheckinContainer,
  argTypes: {},
} as Meta<typeof CheckinContainer>;

export const DefaultPage = () => (
  <Providers>
    <CheckinContainer />
  </Providers>
);
