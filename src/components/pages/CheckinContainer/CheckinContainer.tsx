import { Metadata } from "next";

import { Card, CardBody, CardHeader } from "@/components/atoms/Card";
import { Divider } from "@/components/atoms/Divider";
import { Subtitle, Title } from "@/components/atoms/Text";
import CheckInForm from "@/components/organisms/CheckinForm";

export const metadata: Metadata = {
  title: "Check In Peserta",
};

export const CheckinContainer = () => {
  return (
    <Card>
      <CardHeader className="flex-col space-y-2 py-6 text-center">
        <Title text="Check-in" className="font-semibold text-primary" />
        <Subtitle
          text="Silahkan pilih metode check in dibawah ini"
          className="!text-sm font-light text-secondary "
        />
      </CardHeader>

      <Divider />

      <CardBody>
        <CheckInForm />
      </CardBody>
    </Card>
  );
};

export default CheckinContainer;
