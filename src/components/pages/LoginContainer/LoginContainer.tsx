"use client";

import { Suspense } from "react";

import { Footer } from "@/components/organisms/Footer";
import LoginFormServer from "@/components/organisms/LoginFormServer";
import { FullScreenContainer } from "@/components/template/Container";

const LoginContainer = () => (
  <FullScreenContainer>
    <div className="relative flex content-center items-center">
      <Suspense>
        <LoginFormServer />
      </Suspense>
    </div>

    <Footer />
  </FullScreenContainer>
);

export { LoginContainer };
