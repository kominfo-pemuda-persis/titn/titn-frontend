import { Meta } from "@storybook/react";

import { LoginContainer } from "@/components/pages/LoginContainer";
import Providers from "@/providers";

export default {
  title: "pages/Login",
  component: LoginContainer,
  argTypes: {},
} as Meta<typeof LoginContainer>;

export const Default = () => (
  <Providers>
    <LoginContainer />
  </Providers>
);
