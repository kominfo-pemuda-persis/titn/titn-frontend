import { ReactNode } from "react";

import { Footer } from "@/components/organisms/Footer";
import DashboardNavigationBar from "@/components/organisms/NavigationBar/Dashboard";
import { Sidebar } from "@/components/organisms/Sidebar";
import { SidebarMobile } from "@/components/organisms/SidebarMobile";
import { Container } from "@/components/template/Container";

const MainAdminLayout = ({ children }: { children: ReactNode }) => (
  <main className="flex h-screen">
    <Sidebar />
    <section className="relative grid w-full grid-rows-12">
      <DashboardNavigationBar />
      <Container className="row-span-11 mx-auto w-full overflow-y-auto px-2 sm:px-12">
        {children}
      </Container>
      <SidebarMobile />
      <Footer withoutLogo className="!relative row-span-1 self-center" />
    </section>
  </main>
);

export default MainAdminLayout;
