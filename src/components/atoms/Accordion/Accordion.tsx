import type {
  AccordionItemIndicatorProps as NextUiAccordionItemIndicatorProps,
  AccordionItemProps as NextUiAccordionItemProps,
  AccordionProps as NextUiAccordionProps,
} from "@nextui-org/accordion";

import {
  Accordion as NextUiAccordion,
  AccordionItem as NextUiAccordionItem,
  useAccordion as nextUiUseAccordion,
  useAccordionItem as nextUiUseAccordionItem,
} from "@nextui-org/accordion";

export const AccordionItem = NextUiAccordionItem;
export const Accordion = NextUiAccordion;
export const useAccordionItem = nextUiUseAccordionItem;
export const useAccordion = nextUiUseAccordion;

export interface AccordionProps extends NextUiAccordionProps {}
export type AccordionItemIndicatorProps = NextUiAccordionItemIndicatorProps;
export type AccordionItemProps = NextUiAccordionItemProps;
