"use client";

import { Chrono } from "react-chrono";
import { TimelineProps } from "react-chrono/dist/models/TimelineModel";

interface ExtentedTimelineProps extends TimelineProps {
  hideControls?: boolean;
  showSingle?: boolean;
}

const Timeline = ({
  items,
  mode,
  itemWidth,
  showAllCardsHorizontal,
  disableNavOnKey,
  scrollable,
  hideControls,
  showSingle,
  classNames,
  disableToolbar,
}: ExtentedTimelineProps) => (
  <Chrono
    items={items}
    mode={mode}
    hideControls={hideControls}
    cardHeight={"auto"}
    itemWidth={itemWidth}
    showSingle={showSingle}
    showAllCardsHorizontal={showAllCardsHorizontal}
    disableNavOnKey={disableNavOnKey}
    scrollable={scrollable}
    classNames={classNames}
    disableToolbar={disableToolbar}
    theme={{
      primary: "#37b05b",
      secondary: "#f59e0b",
      cardTitleColor: "#37b05b",
      cardForeColor: "#37b05b",
      titleColor: "#f59e0b",
      titleColorActive: "white",
    }}
  />
);

export default Timeline;
