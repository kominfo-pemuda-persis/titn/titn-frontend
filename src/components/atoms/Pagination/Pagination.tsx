import type {
  PaginationProps as NextPaginationProps,
  PaginationItemRenderProps as NextUiPaginationItemRenderProps,
} from "@nextui-org/pagination";

import {
  Pagination as NextPagination,
  PaginationItemType as NextUiPaginationItemType,
  usePagination as NextUiUsePagination,
} from "@nextui-org/pagination";

export const Pagination = NextPagination;
export const usePagination = NextUiUsePagination;

export type PaginationItemRenderProps = NextUiPaginationItemRenderProps;
export const PaginationItemType = NextUiPaginationItemType;
