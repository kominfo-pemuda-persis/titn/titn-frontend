import type {
  CircularProgressProps as NextUICircularProgressProps,
  ProgressProps as NextUIProgressProps,
} from "@nextui-org/progress";

import {
  CircularProgress as NextUICircularProgress,
  Progress as NextUIProgress,
  useProgress as useNextUIProgress,
} from "@nextui-org/progress";

// export types
export type ProgressProps = NextUIProgressProps;
export type CircularProgressProps = NextUICircularProgressProps;

// export hooks
export const useProgress = useNextUIProgress;

// export component

export const Progress = NextUIProgress;
export const CircularProgress = NextUICircularProgress;
