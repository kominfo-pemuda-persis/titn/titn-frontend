// type
import type {
  TooltipPlacement as NextUiTooltipPlacement,
  TooltipProps as NextUiTooltipProps,
} from "@nextui-org/tooltip";

import {
  // component
  Tooltip as NextUiTooltip,
  // hook
  useTooltip as nextUiUseTooltip,
} from "@nextui-org/tooltip";

// export type
export interface TooltipProps extends NextUiTooltipProps {}
export type TooltipPlacement = NextUiTooltipPlacement;

// export component
export const Tooltip = NextUiTooltip;

// export hook
export const useTooltip = nextUiUseTooltip;
