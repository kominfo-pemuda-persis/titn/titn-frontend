// type
import type {
  CardFooterProps as NextUiCardFooterProps,
  CardProps as NextUiCardProps,
} from "@nextui-org/card";

import {
  // component
  Card as NextUiCard,
  CardBody as NextUiCardBody,
  CardFooter as NextUiCardFooter,
  CardHeader as NextUiCardHeader,
  // hook
  useCard as nextUiUseCard,
} from "@nextui-org/card";

// export type
export interface CardProps extends NextUiCardProps {}
export interface CardFooterProps extends NextUiCardFooterProps {}

// export component
export const Card = NextUiCard;
export const CardHeader = NextUiCardHeader;
export const CardBody = NextUiCardBody;
export const CardFooter = NextUiCardFooter;

// export hook
export const useCard = nextUiUseCard;
