// type
import type {
  BreadcrumbItemProps as NextUiBreadcrumbItemProps,
  BreadcrumbsProps as NextUiBreadcrumbsProps,
} from "@nextui-org/breadcrumbs";

import {
  BreadcrumbItem as NextUiBreadcrumbItem,
  // component
  Breadcrumbs as NextUiBreadcrumbs,
  useBreadcrumbItem as nextUiUseBreadcrumbItem,
  // hook
  useBreadcrumbs as nextUiUseBreadcrumbs,
} from "@nextui-org/breadcrumbs";

// export type
export interface BreadcrumbsProps extends NextUiBreadcrumbsProps {}
export interface BreadcrumbItemProps extends NextUiBreadcrumbItemProps {}

// export component
export const Breadcrumbs = NextUiBreadcrumbs;
export const BreadcrumbItem = NextUiBreadcrumbItem;

// export hook
export const useBreadcrumbs = nextUiUseBreadcrumbs;
export const useBreadcrumbItem = nextUiUseBreadcrumbItem;
