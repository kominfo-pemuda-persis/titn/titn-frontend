"use client";

import { useEffect, useState } from "react";

const PDFReader = ({ src }: { src?: string }) => {
  const [isMobile, setIsMobile] = useState<boolean>(true);

  useEffect(() => {
    console.log(window.innerWidth);
    if (window.innerWidth < 900) {
      setIsMobile(true);
    } else {
      setIsMobile(false);
    }
  }, []);

  return (
    <>
      {!isMobile && (
        <iframe
          src={src}
          className="h-full w-full"
          title="PDF Reader"
          style={{
            height: "80vh",
            minWidth: "900px",
          }}
        />
      )}
    </>
  );
};

export default PDFReader;
