import toast, { ToastOptions } from "react-hot-toast";

const Toast = ({
  message = "",
  type,
  options,
  promiseFn,
  promiseLoadingMessage = " Loading ...",
  promiseSuccessMessage = "Success",
  promiseErrorMessage = "Error",
}: {
  message?: string;
  type: "error" | "success" | "loading" | "fetching";
  options?: ToastOptions;
  promiseFn?: () => Promise<any>;
  promiseLoadingMessage?: string;
  promiseSuccessMessage?: string;
  promiseErrorMessage?: string;
}) => {
  const toastConfig: ToastOptions = {
    position: "bottom-right",
    duration: type === "fetching" ? undefined : 2000,
    ...options,
  };

  if (type === "error") {
    return toast.error(message, toastConfig);
  } else if (type === "loading") {
    return toast.loading(message, toastConfig);
  } else if (type === "fetching" && promiseFn) {
    toast.promise(
      promiseFn(),
      {
        loading: promiseLoadingMessage,
        success: (data) =>
          promiseSuccessMessage ? promiseSuccessMessage : `Data: ${data}`,
        error: (error) =>
          promiseErrorMessage ? promiseErrorMessage : `Error: ${error}`,
      },
      toastConfig
    );
  } else {
    return toast.success(message, toastConfig);
  }
};

export default Toast;
