"use client";

import type { InputProps as NextUiInputProps } from "@nextui-org/input";

import { useState } from "react";
import {
  Input as NextUiInput,
  useInput as nextUiUseInput,
} from "@nextui-org/input";
import { EyeFilledIcon, EyeSlashFilledIcon } from "@nextui-org/shared-icons";

export type InputProps = NextUiInputProps;
export interface PasswordInputProps extends InputProps {}

export const Input = NextUiInput;

export const PasswordInput = (props: PasswordInputProps) => {
  const [isVisible, setIsVisible] = useState<boolean>(false);

  const toggleVisibility = () => setIsVisible(!isVisible);

  return (
    <Input
      {...props}
      endContent={
        <button
          className="focus:outline-none"
          type="button"
          onClick={toggleVisibility}
        >
          {isVisible ? (
            <EyeSlashFilledIcon className="pointer-events-none text-2xl text-default-400" />
          ) : (
            <EyeFilledIcon className="pointer-events-none text-2xl text-default-400" />
          )}
        </button>
      }
      type={isVisible ? "text" : "password"}
    />
  );
};

export const useInput = nextUiUseInput;
