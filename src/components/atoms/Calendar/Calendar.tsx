// type
import type {
  CalendarProps as NextUiCalendarProps,
  DateValue as NextUiDateValue,
  RangeCalendarProps as NextUiRangeCalendarProps,
  RangeValue as NextUiRangeValue,
} from "@nextui-org/calendar";

import {
  // component
  Calendar as NextUiCalendar,
  // context
  CalendarProvider as NextUiCalendarProvider,
  RangeCalendar as NextUiRangeCalendar,
  // hook
  useCalendar as nextUiUseCalendar,
  useCalendarContext as nextUiUseCalendarContext,
  useRangeCalendar as nextUiUseRangeCalendar,
} from "@nextui-org/calendar";

// export type
export type CalendarProps = NextUiCalendarProps;
export type RangeCalendarProps = NextUiRangeCalendarProps;
export type DateValue = NextUiDateValue;
export interface RangeValue<T> extends NextUiRangeValue<T> {}

// export context
export const CalendarProvider = NextUiCalendarProvider;
export const useCalendarContext = nextUiUseCalendarContext;

// export hook
export const useCalendar = nextUiUseCalendar;
export const useRangeCalendar = nextUiUseRangeCalendar;

// export component
export const Calendar = NextUiCalendar;
export const RangeCalendar = NextUiRangeCalendar;
