import {
  IScannerProps,
  Scanner as ReactScanner,
} from "@yudiel/react-qr-scanner";

export const Scanner = ReactScanner;
export type ScannerProps = IScannerProps;
