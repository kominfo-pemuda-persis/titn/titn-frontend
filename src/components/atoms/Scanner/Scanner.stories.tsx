import React from "react";
import { Meta } from "@storybook/react";

import { Scanner, ScannerProps } from "./Scanner";

export default {
  title: "Atoms/Scanner",
  component: Scanner,
} as Meta<typeof Scanner>;

const defaultProps: ScannerProps = {
  onScan: (results) => {
    alert(results);
  },
};

const Template = (args: ScannerProps) => (
  <div className="max-w-[400px]">
    <Scanner {...args} />
  </div>
);

export const Default = {
  render: Template,

  args: {
    ...defaultProps,
    "aria-label": "Loading...",
  },
};
