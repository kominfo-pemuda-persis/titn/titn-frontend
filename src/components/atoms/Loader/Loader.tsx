import { Card, CardBody } from "@/components/atoms/Card";
import { Spinner } from "@/components/atoms/Spinner";

const Loader = ({
  inline = false,
  loadingText = "Please wait...",
}: {
  inline?: boolean;
  loadingText?: string;
}) => {
  return (
    <>
      {inline ? (
        <Spinner label={loadingText} size="lg" color="primary" />
      ) : (
        <Card fullWidth className="h-full">
          <CardBody className="items-center justify-center">
            <Spinner label={loadingText} size="lg" color="primary" />
          </CardBody>
        </Card>
      )}
    </>
  );
};

export default Loader;
