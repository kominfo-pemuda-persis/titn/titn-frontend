import QRCode from "react-qr-code";

const QRCodeGenerator = ({
  size = 256,
  value,
}: {
  size?: number;
  value: string;
}) => (
  <div
    style={{
      height: "auto",
      margin: "0 auto",
      maxWidth: size,
      width: "100%",
      background: "white",
      padding: "16px",
    }}
  >
    <QRCode
      size={size}
      style={{ height: "auto", maxWidth: "100%", width: "100%" }}
      value={value}
      viewBox={`0 0 ${size} ${size}`}
    />
  </div>
);

export default QRCodeGenerator;
