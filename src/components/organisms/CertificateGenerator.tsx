/* eslint-disable @next/next/no-img-element */
"use client";

import { useRef, useState } from "react";
import domtoimage from "dom-to-image";

import { Button } from "@/components/atoms/Button";
import { Spinner } from "@/components/atoms/Spinner";

const IDCardGenerator = ({
  data: { name, npa, pd, type, event },
  className,
}: {
  data: {
    id: string;
    name: string;
    npa: string;
    pd: string;
    type: string;
    event: string[];
  };
  className?: string;
}) => {
  const [generatingCard, setGeneratingCard] = useState<boolean>(false);
  const cardRef = useRef<HTMLDivElement | null>(null);

  const handleDownload = async () => {
    setGeneratingCard(true);
    cardRef.current?.classList.remove(
      "left-[-150%]",
      "top-[-150%]",
      "scale-[0.25]"
    );
    domtoimage
      .toJpeg(cardRef.current as HTMLElement, {
        height: 2000,
        width: 1400,
      })
      .then((dataUrl) => {
        cardRef.current?.classList.add(
          "left-[-150%]",
          "top-[-150%]",
          "scale-[0.25]"
        );
        setGeneratingCard(false);

        const link = document.createElement("a");
        link.href = dataUrl;
        link.download = "certificate.png";
        link.click();
      });
  };

  return (
    <div className={`mx-auto space-y-4 overflow-clip ${className}`}>
      <div className="relative">
        {generatingCard && (
          <div className="align-center absolute z-20 flex h-[500px] w-[350px] justify-center overflow-hidden bg-white shadow-md">
            <Spinner />
          </div>
        )}
        {/* DEBUG 
        <div className="relative shadow-md">
      */}
        {/* NODEBUG
        <div className="relative h-[500px] w-[350px] overflow-hidden shadow-md">
      */}
        <div className="relative z-10 h-[500px] w-[350px] overflow-hidden shadow-md">
          {/* DEBUG 
          className="relative h-[2000px] w-[1400px]"
        */}
          {/* NODEBUG
          className="relative left-[-150%] top-[-150%] h-[2000px] w-[1400px] scale-[0.25]"
        */}
          <div
            ref={cardRef}
            className="relative left-[-150%] top-[-150%] h-[2000px] w-[1400px] scale-[0.25]"
          >
            <div
              className="absolute bottom-0 left-0 right-0 top-0 min-h-full w-full bg-cover bg-center bg-no-repeat"
              style={{
                backgroundImage: `url('/assets/certificate-template.png')`,
              }}
            />
            <div className="absolute left-0 right-0 top-[660px] flex w-[1400px] max-w-[1400px] justify-center overflow-hidden  whitespace-nowrap text-[64px] font-bold italic !text-black underline">
              <h6>{name.toUpperCase()}</h6>
            </div>
            <p className="absolute left-0 top-[770px] flex w-[1400px] justify-center overflow-hidden text-ellipsis whitespace-nowrap text-[28px] font-medium !text-white">
              NPA: {npa || "-"}
            </p>
            <div className="absolute left-0 top-[803px] flex w-[1400px] justify-center overflow-hidden text-ellipsis whitespace-nowrap text-[38px] font-bold !text-black">
              <span>{pd}</span>
            </div>
            <div className="absolute left-0 right-0 top-[900px] flex w-[1400px] max-w-[1400px] justify-center overflow-hidden  whitespace-nowrap text-[92px] font-extrabold !text-black">
              <span>{type}</span>
            </div>
          </div>
        </div>
      </div>
      <Button
        fullWidth
        type="button"
        variant="solid"
        color="primary"
        onClick={handleDownload}
      >
        Download
      </Button>

      {/* {generatedImage && (
        <div>
          <h3>Generated Image:</h3>
          <img
            src={generatedImage}
            alt="Generated ID Card"
            className=" h-[500px] w-[350px] border-1 shadow-md"
          />
        </div>
      )} */}
    </div>
  );
};

export default IDCardGenerator;
