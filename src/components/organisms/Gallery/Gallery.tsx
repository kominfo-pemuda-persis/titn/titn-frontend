"use client";

import Image from "next/image";
import { EffectCards } from "swiper/modules";

import { Carousel } from "@/components/molecules/Carousel";
import Swiper from "@/components/molecules/Swiper/Swiper";

import "swiper/css/effect-cards";

export const SwiperGallery = ({
  slideImages,
}: {
  slideImages: {
    src: string;
    alt: string;
  }[];
}) => (
  <Swiper
    effect="cards"
    modules={[EffectCards]}
    grabCursor
    slides={slideImages}
  />
);

export const Gallery = ({
  slideImages,
  className = "",
}: {
  slideImages: {
    src: string;
    alt: string;
  }[];
  className?: string;
}) => (
  <Carousel
    autoSlideInterval={3000}
    className={`h-full w-full rounded-xl ${className}`}
  >
    {slideImages.map((slide, key) => (
      <Image
        key={key}
        alt={slide.alt}
        src={slide.src}
        width={1200}
        height={500}
        quality={85}
        className="object-centers h-full min-h-full w-full min-w-full max-w-full object-contain"
      />
    ))}
  </Carousel>
);
