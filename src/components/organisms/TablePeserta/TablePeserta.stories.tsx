import { Meta } from "@storybook/react";

import Providers from "@/providers";
import TablePeserta from "./TablePeserta";

export default {
  title: "Organisms/TablePeserta",
  component: TablePeserta,
  argTypes: {},
} as Meta<typeof TablePeserta>;

export const DefaultPage = () => (
  <html lang="en">
    <body>
      <Providers>
        <TablePeserta />
      </Providers>
    </body>
  </html>
);
