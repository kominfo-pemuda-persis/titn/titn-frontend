import { Meta } from "@storybook/react";

import Providers from "@/providers";
import TablePesertaNonAnggota from "./TablePesertaNonAnggota";

export default {
  title: "Organisms/TablePesertaNonAnggota",
  component: TablePesertaNonAnggota,
  argTypes: {},
} as Meta<typeof TablePesertaNonAnggota>;

export const DefaultPage = () => (
  <html lang="en">
    <body>
      <Providers>
        <TablePesertaNonAnggota />
      </Providers>
    </body>
  </html>
);
