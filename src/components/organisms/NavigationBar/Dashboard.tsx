"use client";

import { ReactNode } from "react";
import Link from "next/link";
import { usePathname, useRouter } from "next/navigation";
import {
  Avatar,
  BreadcrumbItem,
  Breadcrumbs,
  Dropdown,
  DropdownItem,
  DropdownMenu,
  DropdownSection,
  DropdownTrigger,
  Navbar,
  NavbarContent,
  NavbarItem,
  User,
} from "@nextui-org/react";
import { useAtomValue, useSetAtom } from "jotai/react";

import ThemeSwitcher from "@/components/molecules/ThemeSwitcher/ThemeSwitcher";
import { ROUTE_LOGIN, ROUTE_SETTINGS_PROFILE } from "@/constant/routes";
import { clearUserData } from "@/services/actions";
import { currentUser, loggedUser } from "@/states/user";
import { getUserAvatar } from "@/utils/externalAssets";
import { capitalize } from "@/utils/text";

const DashboardNavigationBar = () => {
  const { user } = useAtomValue(currentUser);
  const setUser = useSetAtom(loggedUser);
  const router = useRouter();

  const avatarImg = getUserAvatar(user);
  const pathname = usePathname();
  const pathArr = pathname.split("/");
  let breadcrumbsList: ReactNode[] = [];

  if (pathArr.length > 0) {
    const BreadCrumbItems = pathArr.map((route, i) => {
      const link = pathArr.slice(0, i + 1).join("/");
      return (
        <BreadcrumbItem key={`breadcrumb-${i}`} href={link}>
          {capitalize(route.replace("-", " "))}
        </BreadcrumbItem>
      );
    });

    breadcrumbsList = BreadCrumbItems;
  }

  const handleLogout = async () => {
    setUser(null);
    clearUserData();
    router.replace(ROUTE_LOGIN);
  };

  return (
    <>
      <div
        className="absolute top-0 h-60 w-full bg-contain"
        style={{ backgroundImage: "url(/assets/bg_TITN.png)" }}
      >
        <div className="h-full w-full backdrop-blur"></div>
        <div className="relative -top-1/2 h-1/2 bg-gradient-to-t from-white to-transparent dark:from-black"></div>
      </div>
      <Navbar maxWidth="full" isBlurred={false} className="bg-transparent">
        {pathArr.length > 2 && (
          <NavbarContent as="div">
            <Breadcrumbs
              variant="solid"
              separator="/"
              itemClasses={{
                separator: "px-2",
              }}
            >
              {breadcrumbsList}
            </Breadcrumbs>
          </NavbarContent>
        )}
        <NavbarContent as="div" justify="end">
          <NavbarItem>
            <Dropdown placement="bottom-end">
              <DropdownTrigger>
                <Avatar
                  isBordered
                  as="button"
                  className="transition-transform"
                  color="secondary"
                  size="sm"
                  src={avatarImg}
                />
              </DropdownTrigger>
              <DropdownMenu
                aria-label="Profile Actions"
                color="secondary"
                variant="flat"
              >
                <DropdownItem key="profile" className="gap-2">
                  <User
                    name={user?.nama}
                    className="capitalize"
                    description={`as ${user?.role[0].name.replace("_", " ")}`}
                    avatarProps={{
                      src: avatarImg,
                    }}
                  />
                </DropdownItem>
                <DropdownItem key="settings" href={ROUTE_SETTINGS_PROFILE}>
                  My Profile
                </DropdownItem>
                <DropdownItem
                  key="logout"
                  color="danger"
                  onPress={handleLogout}
                >
                  Log Out
                </DropdownItem>
              </DropdownMenu>
            </Dropdown>
          </NavbarItem>
          <NavbarItem>
            <ThemeSwitcher />
          </NavbarItem>
        </NavbarContent>
      </Navbar>
    </>
  );
};
export default DashboardNavigationBar;
