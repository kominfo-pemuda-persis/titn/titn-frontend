import { Meta } from "@storybook/react";

import { NavigationBar } from "./NavigationBar";

export default {
  title: "Organisms/NavigationBar",
  component: NavigationBar,
} as Meta<typeof NavigationBar>;

export const Default = {};
