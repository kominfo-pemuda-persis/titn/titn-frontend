"use client";

import { useState } from "react";
import Image from "next/image";
import { usePathname } from "next/navigation";

import { Button } from "@/components/atoms/Button";
import { Divider } from "@/components/atoms/Divider";
import { Link } from "@/components/atoms/Link";
import {
  Navbar,
  NavbarBrand,
  NavbarContent,
  NavbarItem,
  NavbarMenu,
  NavbarMenuItem,
  NavbarMenuToggle,
} from "@/components/molecules/Navbar";
import ThemeSwitcher from "@/components/molecules/ThemeSwitcher/ThemeSwitcher";
import {
  NAV_HOME_ITEMS,
  ROUTE_DASHBOARD,
  ROUTE_HOME,
  ROUTE_LOGIN,
} from "@/constant/routes";
import useFetchUser from "@/hooks/useFetchUser";
import smoothScroll from "@/utils/smoothScroll";

export const NavigationBar = () => {
  const { currentUser, isLoading } = useFetchUser();

  const pathname = usePathname();
  const [isMenuOpen, setIsMenuOpen] = useState<boolean>(false);

  return (
    <Navbar
      isBlurred
      className={`fixed z-50 w-full px-4 py-2 ${isMenuOpen ? "" : "bg-transparent"} `}
      shouldHideOnScroll={pathname !== ROUTE_HOME}
      onMenuOpenChange={setIsMenuOpen}
    >
      <NavbarContent>
        <NavbarMenuToggle
          aria-label={isMenuOpen ? "Close menu" : "Open menu"}
          className="sm:hidden"
        />

        <NavbarBrand>
          {pathname === ROUTE_HOME ? (
            <Link
              href="/#hero"
              onClick={(e) => smoothScroll(e, "#hero")}
              className="space-x-2"
            >
              <Image
                src="/assets/logo.png"
                alt="LOGO TITN"
                width="0"
                height="0"
                priority
                sizes="(max-width: 640px) 25px, (max-width: 768px) 25px, (max-width: 1024px) 25px, 25px"
                className="hidden  h-full w-full  max-w-[25px] sm:block"
              />
              <p className="text-xl font-bold text-primary">TITN</p>
            </Link>
          ) : (
            <Link href={ROUTE_HOME} className="space-x-2">
              <Image
                src="/assets/logo.png"
                alt="LOGO TITN"
                width="0"
                height="0"
                priority
                sizes="(max-width: 640px) 25px, (max-width: 768px) 25px, (max-width: 1024px) 25px, 25px"
                className="hidden  h-full w-full  max-w-[25px] sm:block"
              />
              <p className="text-xl font-bold text-primary">TITN</p>
            </Link>
          )}
        </NavbarBrand>
      </NavbarContent>

      <NavbarMenu>
        <NavbarMenuItem key={0}>
          <Divider />
        </NavbarMenuItem>

        <NavbarMenuItem key={1}>
          <Link
            color="foreground"
            href={ROUTE_LOGIN}
            size="lg"
            className="w-full py-2"
          >
            Masuk
          </Link>
        </NavbarMenuItem>
        <NavbarMenuItem key={2}>
          <Link
            color="foreground"
            href="/daftar"
            size="lg"
            className="w-full py-2"
          >
            Daftar Peserta / Simpatisan
          </Link>
        </NavbarMenuItem>
        <NavbarMenuItem key={3}>
          <Link
            color="foreground"
            href="/pengunjung"
            size="lg"
            className="w-full py-2"
          >
            Daftar Pengunjung
          </Link>
        </NavbarMenuItem>
      </NavbarMenu>

      {pathname === ROUTE_HOME ? (
        <NavbarContent justify="center" className="hidden flex-wrap sm:flex">
          {NAV_HOME_ITEMS.map((item, key) => (
            <NavbarItem key={key}>
              <Link
                size="sm"
                color="secondary"
                href={item.href}
                onClick={(e) => smoothScroll(e, item.href)}
              >
                {item.text}
              </Link>
            </NavbarItem>
          ))}
        </NavbarContent>
      ) : null}
      <NavbarContent justify="end">
        {currentUser ? (
          <NavbarItem className="hidden sm:block" key={0}>
            <Button
              as={Link}
              type="button"
              color="primary"
              href={ROUTE_DASHBOARD}
              variant="light"
              size="sm"
              isLoading={isLoading}
            >
              Dashboard
            </Button>
          </NavbarItem>
        ) : (
          <NavbarItem className="hidden sm:block" key={0}>
            <Button
              as={Link}
              type="button"
              color="primary"
              href={ROUTE_LOGIN}
              variant="light"
              size="sm"
              isLoading={isLoading}
            >
              Masuk
            </Button>
          </NavbarItem>
        )}
        <NavbarItem className="hidden sm:block" key={1}>
          <Button
            as={Link}
            type="button"
            color="primary"
            href="/daftar"
            variant="ghost"
            size="sm"
            isDisabled={isLoading}
          >
            Daftar
          </Button>
        </NavbarItem>
        <NavbarItem>
          <ThemeSwitcher />
        </NavbarItem>
      </NavbarContent>
    </Navbar>
  );
};
