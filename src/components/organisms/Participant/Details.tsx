"use client";

import { useState } from "react";
import { Dropzone, FileMosaic } from "@files-ui/react";
import { Input } from "@nextui-org/react";
import { useMutation, useQuery } from "@tanstack/react-query";
import { useAtomValue } from "jotai";
import { BsFloppyFill } from "react-icons/bs";
import { IoMdCloseCircle } from "react-icons/io";
import { z } from "zod";

import { Button } from "@/components/atoms/Button";
import {
  Card,
  CardBody,
  CardFooter,
  CardHeader,
} from "@/components/atoms/Card";
import { Chip } from "@/components/atoms/Chip";
import { Divider } from "@/components/atoms/Divider";
import { Image } from "@/components/atoms/Image";
import { Paragraph, SubsectionTitle } from "@/components/atoms/Text";
import Toast from "@/components/atoms/Toast";
import ImageUpload from "@/components/molecules/ImageUpload/ImageUpload";
import { Select, Selection, SelectItem } from "@/components/molecules/Select";
import CertificateGenerator from "@/components/organisms/CertificateGenerator";
import IDCardGenerator from "@/components/organisms/IDCardGenerator";
import { updateParticipantData } from "@/services/mutation/participant";
import { getEvents } from "@/services/query/events";
import { currentUser, permission } from "@/states/user";
import {
  BaseResponse,
  DataAnggotaSchema,
  DataSimpatisanSchema,
  EventType,
  Participant,
} from "@/types";
import { eventItem } from "@/types/events";
import { getColorBasedOnEventType, getColorBasedOnStatus } from "@/utils";
import { getUserAvatar } from "@/utils/externalAssets";

const Details = ({
  participants,
  callback,
}: {
  participants: Participant;
  callback: () => void;
}) => {
  const [isUpdate, setIsUpdate] = useState(false);
  const [userAvatarUrl, setUserAvatarUrl] = useState("");
  const [events, setEvents] = useState<eventItem[]>([]);
  const [selectedEventIds, setSelectedEventIds] = useState<Selection>(
    new Set([])
  );
  const [selectedSize, setSelectedSize] = useState(
    participants?.clothes_size || ""
  );
  const [anggotaData, setAnggotaData] = useState<{
    email: string | undefined;
    phone: string | undefined;
  }>({
    email: participants?.email,
    phone: participants?.phone,
  });
  const [error, setError] = useState<string>("");
  const { currentRole } = useAtomValue(permission);
  const { user } = useAtomValue(currentUser);
  const isParticipantPanitia = participants?.event_details?.find(
    (event) => event.id === "21" || event.id === "22"
  );
  const isAnggota = participants?.npa;
  const hasIDCard = ["APPROVED", "CHECKED_IN"].includes(participants?.status);
  const isSelf =
    participants?.npa === user?.npa || participants?.email === user?.email;
  const isSuperAdmin = currentRole.isSuperAdmin;
  const isAdmin =
    currentRole.isAdminPC ||
    currentRole.isAdminPD ||
    currentRole.isAdminPP ||
    currentRole.isAdminPW;

  const isAllowUpdateInfo = isSuperAdmin || isAdmin || isSelf;
  const isAllowUpdateSize = isSelf
    ? isSuperAdmin || isAdmin || (isAnggota && !hasIDCard)
    : isSuperAdmin || (isAdmin && !hasIDCard);
  const isAllowUpdateEvent = isSuperAdmin;

  const isUpdateAllowed =
    isAllowUpdateInfo || isAllowUpdateSize || isAllowUpdateEvent;

  const sportEventIds = events
    .filter((event) => event.type === "OLAHRAGA")
    .map((event) => event.id);
  const scienceEventIds = events
    .filter((event) => event.type === "ILMIAH")
    .map((event) => event.id);

  const sizes = ["M", "L", "XL", "XXL", "XXXL"];

  useQuery({
    queryKey: ["events"],
    queryFn: async () => {
      const results = await getEvents();
      setEvents(
        results.content.filter(
          (event: { id: string; name: string; type: EventType }) =>
            event.id !== "21" && event.id !== "22"
        ) ?? []
      );
      return results;
    },
  });

  const { mutate, isPending } = useMutation({
    mutationFn: updateParticipantData,
    onSuccess: (resp: BaseResponse) => {
      if (resp.success) {
        callback();

        setError("");
        Toast({ message: "Berhasil Mengupdate Participant", type: "success" });
        setIsUpdate(false);
      } else {
        setError(resp.message ?? "Gagal Mengupdate Participant");
        Toast({
          message: error || "Gagal Mengupdate Participant",
          type: "error",
        });
        setIsUpdate(false);
      }
    },
  });

  const toggleIsUpdate = (isUpdate: boolean) => {
    setIsUpdate(isUpdate);
    setSelectedEventIds(
      new Set(participants.event_details?.map((event) => event.id) || [])
    );
    setUserAvatarUrl(participants?.foto || participants?.photo || "");
    setSelectedSize(participants?.clothes_size || "");
  };

  const removeSelectedEvent = (id: string) => {
    if (typeof selectedEventIds === "object") {
      const newIds = selectedEventIds.difference(new Set([id]));
      setSelectedEventIds(newIds);
    }
  };

  const onEventSelectionChange = (selection: Selection) => {
    setSelectedEventIds(selection);
    if (typeof selection === "object" && typeof selectedEventIds === "object") {
      if (selection.has("20") && !selectedEventIds.has("20")) {
        setSelectedEventIds(new Set(["20"]));
      } else if (selectedEventIds.has("20")) {
        setSelectedEventIds(new Set(selection.difference(new Set(["20"]))));
      } else if (
        selectedEventIds.intersection(new Set(sportEventIds)).size > 0 &&
        selection
          .difference(new Set(selectedEventIds))
          .intersection(new Set(sportEventIds)).size > 0
      ) {
        const scineceEventIdsSet = selection.difference(new Set(sportEventIds));
        const sportEventId = selection.difference(selectedEventIds);
        setSelectedEventIds(scineceEventIdsSet.union(sportEventId));
      } else if (
        selectedEventIds.intersection(new Set(scienceEventIds)).size > 0 &&
        selection
          .difference(new Set(selectedEventIds))
          .intersection(new Set(scienceEventIds)).size > 0
      ) {
        const sportEvents = selection.difference(new Set(scienceEventIds));
        const scienceEventId = selection.difference(selectedEventIds);
        setSelectedEventIds(sportEvents.union(scienceEventId));
      }
    }
  };

  const onSizeChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
    setSelectedSize(e.target.value);
  };

  const onSubmit = async () => {
    try {
      const event_ids = Array.from(selectedEventIds).map((event_id) =>
        parseInt(event_id.toString())
      );
      const newData = {
        participant_id: participants.participant_id,
        npa: participants.npa || "",
        full_name: participants.full_name,
        email: anggotaData.email,
        phone: anggotaData.phone || "",
        nik: participants.nik,
        pc_code: participants.pc_code,
        pc_name: participants.pc_name,
        pd_code: participants.pd_code,
        pd_name: participants.pd_name,
        pw_code: participants.pw_code,
        pw_name: participants.pw_name,
        event_ids,
        clothes_size: selectedSize,
        photo: userAvatarUrl,
      };

      const parsed = isAnggota
        ? DataAnggotaSchema.parse(newData)
        : DataSimpatisanSchema.parse(newData);

      mutate(parsed);
    } catch (e) {
      if (e instanceof z.ZodError) {
        const messages = e.issues.map((err) => {
          return err.message + "\n";
        });
        setError(messages.join(""));
        Toast({ message: messages.join(""), type: "error" });
      } else {
        setError(e as string);
        Toast({ message: e as string, type: "error" });
      }
    }
  };

  return (
    <div
      className={`mx-auto flex flex-col items-center gap-8 ${hasIDCard ? "max-w-5xl" : "w-full"}`}
    >
      <div className="w-full space-y-6">
        <Card shadow="sm">
          <CardHeader className="flex items-center justify-between gap-3 bg-primary bg-opacity-85 !py-2">
            <SubsectionTitle
              text="Profile"
              className="font-semibold text-white"
            />
            {isUpdateAllowed ? (
              !isUpdate ? (
                <Button color="secondary" onClick={() => toggleIsUpdate(true)}>
                  Edit Data Peserta
                </Button>
              ) : (
                <Button color="danger" onClick={() => toggleIsUpdate(false)}>
                  Batalkan Edit
                </Button>
              )
            ) : (
              <></>
            )}
          </CardHeader>
          <CardBody className="grid grid-cols-1 gap-4 py-8 md:grid-cols-3">
            <div className="col-span-1 flex items-start justify-center">
              <Card
                isFooterBlurred
                radius="lg"
                className="min-w-40 border-none"
              >
                {!isUpdate ? (
                  <Image
                    alt="Woman listing to music"
                    className="object-cover"
                    height={200}
                    src={getUserAvatar(participants)}
                    width={200}
                  />
                ) : (
                  <ImageUpload
                    name="foto"
                    category="calon_peserta"
                    disabled={isPending || !isAllowUpdateInfo}
                    onChange={(value: string) => {
                      if (isPending) return;

                      setUserAvatarUrl(value);
                    }}
                    value={userAvatarUrl || getUserAvatar(participants)}
                    classNames={{
                      wrapper: "w-fit h-fit rounded-xl mx-auto",
                      image:
                        "w-[200px] h-[200px] aspect-square object-cover border-2 border-gray-200",
                    }}
                    placeholder={
                      <Dropzone
                        accept="image/*"
                        label="Upload Avatar Participant"
                        maxFiles={1}
                        className="aspect-square max-h-[200px] max-w-[200px]"
                        maxFileSize={2 * 1024 * 1024}
                      >
                        <FileMosaic alwaysActive preview />
                      </Dropzone>
                    }
                  />
                )}
                {isAnggota && !isUpdate && (
                  <CardFooter className="absolute bottom-1 z-10 ml-1 w-[calc(100%_-_8px)] justify-between overflow-hidden rounded-large border-1 border-white/20 py-1 shadow-small before:rounded-xl before:bg-white/10">
                    <p className="text-medium text-white/80">
                      {participants?.npa}
                    </p>

                    <Chip size="sm" color="secondary">
                      {isAnggota ? "Anggota" : "Non-Anggota"}
                    </Chip>
                  </CardFooter>
                )}
              </Card>
            </div>

            <div className="col-span-2">
              <Divider className="my-4 block md:hidden" />

              <div className="grid grid-cols-3 gap-2 text-sm">
                <span className="font-medium">Full Name</span>
                <span className="col-span-2">: {participants?.full_name}</span>

                <span className="font-medium">Email</span>
                {isUpdate ? (
                  <span className="col-span-2">
                    <Input
                      id="email"
                      name="email"
                      placeholder="example@example.com"
                      type="email"
                      value={anggotaData.email}
                      onChange={(e) =>
                        setAnggotaData({
                          ...anggotaData,
                          email: e.target.value,
                        })
                      }
                      disabled={!isAllowUpdateInfo}
                      isRequired
                    />
                  </span>
                ) : (
                  <span className="col-span-2">: {participants?.email}</span>
                )}

                <span className="font-medium">Phone</span>
                {isUpdate ? (
                  <span className="col-span-2">
                    <Input
                      id="phone"
                      name="phone"
                      placeholder="08987654321"
                      maxLength={15}
                      type="phone"
                      value={anggotaData.phone}
                      onChange={(e) =>
                        setAnggotaData({
                          ...anggotaData,
                          phone: e.target.value,
                        })
                      }
                      disabled={!isAllowUpdateInfo}
                      isRequired
                    />
                  </span>
                ) : (
                  <span className="col-span-2">: {participants?.phone}</span>
                )}

                {participants?.nik && (
                  <>
                    <span className="font-medium">NIK</span>
                    <span className="col-span-2">: {participants?.nik}</span>
                  </>
                )}

                <span className="font-medium">Pimpinan Cabang</span>
                <span className="col-span-2">: {participants?.pc_name}</span>
                <span className="font-medium">Pimpinan Daerah</span>
                <span className="col-span-2">: {participants?.pd_name}</span>
                <span className="font-medium">Pimpinan Wilayah</span>
                <span className="col-span-2">: {participants?.pw_name}</span>
              </div>
            </div>
          </CardBody>
        </Card>

        <Card shadow="sm">
          <CardHeader className="flex items-center justify-between gap-3 bg-primary bg-opacity-85 !py-2">
            <SubsectionTitle
              text="Perlombaan"
              className="font-semibold text-white"
            />
          </CardHeader>

          <CardBody className="grid grid-cols-1 gap-4 py-8 md:grid-cols-3">
            <div className="col-span-3">
              <div className="grid grid-cols-3 items-start gap-2 text-sm">
                {(isAnggota || isParticipantPanitia) && (
                  <>
                    <div className="font-medium">Event yang diikuti</div>
                    <div className="col-span-2 flex items-start gap-2">
                      <span>: </span>
                      <div className="flex w-full flex-wrap gap-2">
                        {!isUpdate || isParticipantPanitia ? (
                          participants?.event_details?.map((event, index) => (
                            <Chip
                              color={`${getColorBasedOnEventType(event.type as EventType)}`}
                              key={index}
                              size="sm"
                              variant="dot"
                            >
                              {event.name}
                            </Chip>
                          ))
                        ) : (
                          <Select
                            items={events}
                            variant="bordered"
                            isDisabled={isPending || !isAllowUpdateEvent}
                            isMultiline={true}
                            selectionMode="multiple"
                            placeholder="Pilih Acara Yang akan di ikuti"
                            selectedKeys={selectedEventIds}
                            onSelectionChange={onEventSelectionChange}
                            classNames={{
                              base: "w-full",
                              trigger: "min-h-4 py-2",
                            }}
                            renderValue={(events) => {
                              return (
                                <div className="flex flex-wrap gap-2">
                                  {events.map((event) => (
                                    <Chip
                                      color={`${getColorBasedOnEventType(event.data?.type as EventType)}`}
                                      key={event.key}
                                      size="sm"
                                      variant="dot"
                                      className="flex flex-row items-center"
                                      endContent={
                                        <IoMdCloseCircle
                                          className="text-danger"
                                          size={14}
                                          onClick={(e) => {
                                            if (isPending) return;
                                            e.stopPropagation();
                                            removeSelectedEvent(
                                              event.data?.id || ""
                                            );
                                          }}
                                        />
                                      }
                                    >
                                      <div className="min-w-fit">
                                        {event.data?.name}
                                      </div>
                                    </Chip>
                                  ))}
                                </div>
                              );
                            }}
                          >
                            {(event) => (
                              <SelectItem key={event.id} textValue={event.name}>
                                <div className="flex flex-wrap gap-2">
                                  <Chip
                                    color={`${getColorBasedOnEventType(event.type as EventType)}`}
                                    key={event.id}
                                    size="sm"
                                    variant="dot"
                                  >
                                    {event.name}
                                  </Chip>
                                </div>
                              </SelectItem>
                            )}
                          </Select>
                        )}
                      </div>
                    </div>
                  </>
                )}
                <Paragraph
                  text="Ukuran Baju"
                  className="!text-sm font-medium"
                />
                <div className="col-span-2 flex items-center gap-2">
                  <span>: </span>
                  {!isUpdate ? (
                    <b>{participants?.clothes_size}</b>
                  ) : (
                    <Select
                      variant="bordered"
                      placeholder="Pilih Ukuran Baju"
                      isDisabled={isPending || !isAllowUpdateSize}
                      selectedKeys={[selectedSize]}
                      onChange={onSizeChange}
                      classNames={{
                        base: "w-full",
                        trigger: "min-h-2 py-2",
                      }}
                    >
                      {sizes.map((size) => (
                        <SelectItem key={size} textValue={size}>
                          {size}
                        </SelectItem>
                      ))}
                    </Select>
                  )}
                </div>

                <div className="!text-sm font-medium">Status</div>
                <div className="col-span-2">
                  :{" "}
                  <Chip
                    color={`${getColorBasedOnStatus(participants.status)}`}
                    size="sm"
                    className="text-white"
                    variant="solid"
                  >
                    {participants?.status}
                  </Chip>
                </div>
              </div>
            </div>
          </CardBody>
        </Card>
        {isUpdate && (
          <Button
            type="button"
            className="w-full"
            color="primary"
            isDisabled={isPending}
            onClick={onSubmit}
            startContent={<BsFloppyFill />}
            variant="shadow"
          >
            Simpan
          </Button>
        )}
      </div>

      <div className="flex flex-row flex-wrap items-center gap-4">
        {hasIDCard && (
          <>
            <Divider className="my-4 block xl:hidden" />
            <IDCardGenerator
              data={{
                id: participants?.participant_id,
                name: participants?.full_name,
                npa: participants?.npa,
                pd: participants?.pd_name,
                avatarUrl: getUserAvatar({
                  foto: participants?.foto,
                  photo: participants?.photo,
                }),
                event:
                  participants?.event_details?.map((event) => event.name) || [],
                type: participants.event_details?.find(
                  (event) => event.id === "21"
                )
                  ? "PANITIA"
                  : participants.event_details?.find(
                        (event) => event.id === "22"
                      )
                    ? "OFFICIAL"
                    : "PESERTA",
              }}
              className="px-4 pb-2"
            />
          </>
        )}

        {participants.status == "CHECKED_IN" && (
          <>
            <Divider className="my-4 block xl:hidden" />
            <CertificateGenerator
              data={{
                id: participants?.participant_id,
                name: participants?.full_name,
                npa: participants?.npa,
                pd: participants?.pd_name,
                event:
                  participants?.event_details?.map((event) => event.name) || [],
                type: participants.event_details?.find(
                  (event) => event.id === "21"
                )
                  ? "PANITIA"
                  : participants.event_details?.find(
                        (event) => event.id === "22"
                      )
                    ? "OFFICIAL"
                    : "PESERTA",
              }}
              className="px-4 pb-2"
            />
          </>
        )}
      </div>
    </div>
  );
};

export default Details;
