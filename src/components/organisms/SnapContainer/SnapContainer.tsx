import { forwardRef, ReactNode, Ref } from "react";

const SnapContainer = (
  {
    items,
    className = "",
  }: {
    items: ReactNode[];
    className?: string;
  },
  ref: Ref<HTMLDivElement>
) => (
  <div
    ref={ref}
    className={
      "z-0 h-screen flex-grow snap-y snap-mandatory overflow-y-scroll " +
      className
    }
  >
    {items.map((item, index) => (
      <div
        key={index}
        className="snap-center snap-always"
        id={`section-${index}`}
      >
        {item}
      </div>
    ))}
  </div>
);

export default forwardRef(SnapContainer);
