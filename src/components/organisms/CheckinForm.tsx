"use client";

import { useRef, useState } from "react";
import { useCookies } from "next-client-cookies";
import { useMutation, useQuery } from "@tanstack/react-query";
import { SwipeableButton } from "react-swipeable-button";

import { Avatar } from "@/components/atoms/Avatar";
import { Button } from "@/components/atoms/Button";
import { Card, CardBody } from "@/components/atoms/Card";
import { Chip } from "@/components/atoms/Chip";
import { Input } from "@/components/atoms/Input";
import Loader from "@/components/atoms/Loader/Loader";
import { Scanner, ScannerProps } from "@/components/atoms/Scanner";
import { SubsectionTitle, Subtitle } from "@/components/atoms/Text";
import { Tab, Tabs } from "@/components/molecules/Tabs/Tabs";
import ModalConfirmation from "@/components/organisms/Modal/ModalConfirmation";
import { postCheckin } from "@/services/mutation/participant";
import {
  getParticipantById,
  listParticipant,
} from "@/services/query/participant";
import { BaseResponse, Participant, ParticipantStatus } from "@/types";
import { getUserAvatar } from "@/utils/externalAssets";

const CheckInForm = () => {
  const [isScannerOpen, setIsScannerOpen] = useState<boolean>(true);
  const [namaPeserta, setNamaPeserta] = useState<string>("");
  const [selectedParticipant, setSelectedParticipant] =
    useState<Participant | null>(null);
  const [participants, setParticipants] = useState<Participant[]>(
    [] as Participant[]
  );
  const [success, setSuccess] = useState<boolean>(false);
  const [error, setError] = useState<string>("");
  const [isOpenModalCheckin, setIsOpenModalCheckin] = useState<boolean>(false);
  const formRef = useRef<HTMLFormElement>(null);
  const cookie = useCookies();
  const session = cookie?.get("session");

  const { data, isPending, mutate } = useMutation({
    mutationFn: postCheckin,
    onSuccess: (resp: BaseResponse) => {
      if (resp.success) {
        setSuccess(true);
        setError("");
        setNamaPeserta("");
        setParticipants([]);
        setIsOpenModalCheckin(false);
      } else {
        setSuccess(true);
        setError(resp.message ?? "Gagal Checkin Peserta");
        setParticipants([]);
        setIsOpenModalCheckin(false);
      }
    },
    onError: (resp: BaseResponse) => {
      setSuccess(true);
      setError(resp.message ?? "Gagal Checkin Peserta");
    },
  });

  const {
    isLoading,
    isError,
    data: checkedInData,
  } = useQuery({
    queryKey: [data?.data?.data?.participant_id],
    queryFn: async () =>
      await getParticipantById(data?.data?.data?.participant_id, session),
  });

  const getScannerResults: ScannerProps["onScan"] = (results) => {
    mutate({ code: results[0].rawValue });
  };

  const handleOpenModal = (participant: Participant) => {
    setSelectedParticipant(participant);
    setIsOpenModalCheckin(true);
  };

  useQuery({
    queryKey: ["participant", namaPeserta],
    queryFn: async () => {
      setParticipants([]);

      const { data } = await listParticipant({
        keyword: namaPeserta,
        session,
        status: ParticipantStatus.APPROVED,
      });

      const results = data?.content ?? [];
      setParticipants(results);
      return results;
    },
    retry: 0,
    enabled: namaPeserta.length > 2,
  });

  const handleClick = () => {
    mutate({ code: selectedParticipant?.participant_id as string });
  };

  return (
    <>
      <Tabs
        fullWidth
        aria-label="Metode Check In"
        onSelectionChange={(menu) => {
          setIsScannerOpen(menu === "scanner");
        }}
        className="mx-auto w-full max-w-xl py-4"
        variant="solid"
        color="primary"
      >
        <Tab key="scanner" title="QR Code">
          <div className="flex w-full justify-center">
            <div className="relative h-[300px] w-[300px] overflow-hidden rounded-xl md:h-[500px] md:w-[500px]">
              <Scanner
                paused={success || isPending}
                allowMultiple
                onScan={getScannerResults}
                formats={[
                  "aztec",
                  "code_128",
                  "code_39",
                  "code_93",
                  "codabar",
                  "databar",
                  "databar_expanded",
                  "data_matrix",
                  "dx_film_edge",
                  "ean_13",
                  "ean_8",
                  "itf",
                  "maxi_code",
                  "micro_qr_code",
                  "pdf417",
                  "qr_code",
                  "rm_qr_code",
                  "upc_a",
                  "upc_e",
                  "linear_codes",
                  "matrix_codes",
                  "unknown",
                ]}
              />
            </div>
          </div>
        </Tab>
        <Tab key="nama" title="Nama Partisipan">
          <div className="mx-auto w-full max-w-xl space-y-4">
            <form
              ref={formRef}
              id="checkin-form"
              onSubmit={(e) => e.preventDefault()}
            >
              <Input
                name="nama_peserta"
                type="text"
                label="Masukan Nama Peserta"
                value={namaPeserta}
                placeholder="Masukan kode peserta anda"
                isRequired={!isScannerOpen}
                onChange={(event) => {
                  if (event.target.value === "") {
                    setParticipants([]);
                  }
                  setNamaPeserta(event.target.value);
                }}
              />
            </form>

            <div className="space-y-1">
              {participants?.map((participant, index) => (
                <Card
                  key={index}
                  className="cursor-pointer border duration-300 hover:border-primary hover:shadow-primary hover:ease-in"
                >
                  <CardBody
                    className="flex w-full flex-row items-start justify-between space-x-2"
                    onClick={() => handleOpenModal(participant)}
                  >
                    <div className="space-y-1">
                      <SubsectionTitle
                        text={participant.full_name}
                        className=" text-primary"
                      />

                      <small className="text-xs text-gray-500">
                        {participant.participant_id}
                      </small>

                      <div className="flex flex-wrap gap-1">
                        {participant.event_details?.map((event, index) => (
                          <Chip
                            key={index}
                            className="capitalize"
                            color="primary"
                            size="sm"
                            variant="dot"
                          >
                            {event.name}
                          </Chip>
                        ))}
                      </div>
                    </div>

                    {participant.clothes_size && (
                      <Chip
                        className="capitalize"
                        color="secondary"
                        size="sm"
                        variant="flat"
                      >
                        {participant.clothes_size}
                      </Chip>
                    )}
                  </CardBody>
                </Card>
              ))}
            </div>
          </div>
        </Tab>
      </Tabs>

      <ModalConfirmation
        size="lg"
        isOpen={isOpenModalCheckin}
        onClose={() => setIsOpenModalCheckin(false)}
        isDismissable
        title="Data Peserta"
        message={
          <Card className="rounded-none border-none shadow-none">
            <CardBody className="flex w-full flex-col items-center justify-center space-y-4 shadow">
              <Avatar
                size="lg"
                className="h-40 w-40 shadow"
                src={getUserAvatar(selectedParticipant)}
              />
              <div className="space-y-2">
                <SubsectionTitle
                  text={selectedParticipant?.full_name}
                  className=" text-primary"
                />

                <div className="flex flex-col space-y-1">
                  <small className="text-xs text-gray-500">
                    {selectedParticipant?.participant_id}
                  </small>

                  <small className="text-xs text-gray-500">
                    {selectedParticipant?.phone}
                  </small>

                  <small className="text-xs text-gray-500">
                    {selectedParticipant?.email}
                  </small>
                </div>

                <div className="flex flex-wrap items-center gap-1">
                  {selectedParticipant?.clothes_size && (
                    <Chip className="capitalize" color="secondary" size="sm">
                      {selectedParticipant?.clothes_size}
                    </Chip>
                  )}

                  <Chip
                    className="capitalize"
                    color="secondary"
                    size="sm"
                    variant="dot"
                  >
                    {selectedParticipant?.status}
                  </Chip>
                  {selectedParticipant?.event_details?.map((event, index) => (
                    <Chip
                      key={index}
                      className="capitalize"
                      color="primary"
                      size="sm"
                      variant="dot"
                    >
                      {event.name}
                    </Chip>
                  ))}
                </div>
              </div>
            </CardBody>
          </Card>
        }
        footer={
          <SwipeableButton
            onSuccess={handleClick}
            text="Check In Sekarang"
            text_unlocked="Check In Diproses"
            color="#37b05b"
          />
        }
      />

      <ModalConfirmation
        size="lg"
        isOpen={success}
        isDismissable
        onClose={() => {
          setSuccess(false);
          if (!error || isScannerOpen) {
            setParticipants([]);
            formRef.current?.reset();
          }
        }}
        message={
          error.length > 0 ? (
            <p className="capitalize text-red-500">{error}</p>
          ) : (
            <div className="space-y-4">
              <Subtitle text="Berhasil check-in!" className="text-green-500" />
              {isError ? (
                <p className="text-red-500">Gagal mendapatkan info peserta!</p>
              ) : isLoading ? (
                <Loader />
              ) : (
                <Card className="rounded-none border-none shadow-none">
                  <CardBody className="flex w-full flex-col items-center justify-center space-y-4 shadow">
                    <Avatar
                      size="lg"
                      className="h-40 w-40 shadow"
                      src={getUserAvatar(checkedInData?.data)}
                    />
                    <div className="space-y-2">
                      <SubsectionTitle
                        text={checkedInData?.data?.full_name}
                        className=" text-primary"
                      />

                      <div className="flex flex-col space-y-1">
                        <small className="text-xs text-gray-500">
                          {checkedInData?.data?.participant_id}
                        </small>

                        <small className="text-xs text-gray-500">
                          {checkedInData?.data?.phone}
                        </small>

                        <small className="text-xs text-gray-500">
                          {checkedInData?.data?.email}
                        </small>
                      </div>

                      <div className="flex flex-wrap items-center gap-1">
                        {checkedInData?.data?.clothes_size && (
                          <Chip
                            className="capitalize"
                            color="secondary"
                            size="sm"
                          >
                            {checkedInData?.data?.clothes_size}
                          </Chip>
                        )}

                        <Chip
                          className="capitalize"
                          color="secondary"
                          size="sm"
                          variant="dot"
                        >
                          {checkedInData?.data?.status}
                        </Chip>
                        {checkedInData?.data?.event_details?.map(
                          (event: any, index: number) => (
                            <Chip
                              key={index}
                              className="capitalize"
                              color="primary"
                              size="sm"
                              variant="dot"
                            >
                              {event.name}
                            </Chip>
                          )
                        )}
                      </div>
                    </div>
                  </CardBody>
                </Card>
              )}
            </div>
          )
        }
        footer={
          <Button
            variant="solid"
            size="md"
            autoFocus
            className="w-full text-white"
            color={error.length > 0 ? "danger" : "success"}
            onPress={() => {
              setSelectedParticipant(null);
              setSuccess(false);
              if (!error || isScannerOpen) {
                setParticipants([]);
                formRef.current?.reset();
              }
            }}
          >
            OK
          </Button>
        }
      />
    </>
  );
};

export default CheckInForm;
