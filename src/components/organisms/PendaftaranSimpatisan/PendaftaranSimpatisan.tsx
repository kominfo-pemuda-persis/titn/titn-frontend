"use client";

import { FormEvent, useRef, useState } from "react";
import { useMutation, useQuery } from "@tanstack/react-query";
import { z } from "zod";

import { Button } from "@/components/atoms/Button";
import { Checkbox } from "@/components/atoms/Checkbox";
import { Image } from "@/components/atoms/Image";
import { Input } from "@/components/atoms/Input";
import { Spinner } from "@/components/atoms/Spinner";
import {
  Autocomplete,
  AutocompleteItem,
} from "@/components/molecules/Autocomplete";
import ImageUpload from "@/components/molecules/ImageUpload/ImageUpload";
import {
  Modal,
  ModalBody,
  ModalContent,
  ModalFooter,
  ModalHeader,
} from "@/components/molecules/Modal";
import { Radio, RadioGroup } from "@/components/molecules/RadioGroup";
import { postRegisterParticipant } from "@/services/mutation/participant";
import { getPCByPD } from "@/services/query/pc";
import { getPDByPW } from "@/services/query/pd";
import { getPW } from "@/services/query/pw";
import {
  BaseResponse,
  DataAnggota,
  DataSimpatisanSchema,
  ResponsePc,
  ResponsePd,
  ResponsePw,
} from "@/types";

export const PendaftaranSimpatisan = ({
  isPanitia,
}: {
  isPanitia?: boolean;
}) => {
  const [pws, setPws] = useState<ResponsePw[]>([] as ResponsePw[]);
  const [pds, setPds] = useState<ResponsePd[]>([] as ResponsePd[]);
  const [pcs, setPcs] = useState<ResponsePc[]>([] as ResponsePc[]);
  const [selectedPw, setSelectedPw] = useState<string | null>(null);
  const [selectedPd, setSelectedPd] = useState<string | null>(null);
  const [selectedPc, setSelectedPc] = useState<string | null>(null);
  const [imageUrl, setImageUrl] = useState<string>("");
  const [isFakeFetch, setIsFakeFetch] = useState<boolean>(false);
  const [success, setSuccess] = useState<boolean>(false);
  const [error, setError] = useState<string>("");

  useQuery({
    queryKey: ["pws"],
    queryFn: async () => {
      const results = await getPW();
      setPws(results ?? []);
      return results;
    },
  });

  const { refetch: refetchPds } = useQuery({
    queryKey: ["pds-" + selectedPw],
    queryFn: async () => {
      if (selectedPw) {
        const results = await getPDByPW(selectedPw);
        setPds(results ?? []);
        return results;
      }
      return [];
    },
  });

  const { refetch: refetchPcs } = useQuery({
    queryKey: ["pcs-" + selectedPd],
    queryFn: async () => {
      if (selectedPd) {
        const results = await getPCByPD(selectedPd);
        setPcs(results ?? []);
        return results;
      }
      return [];
    },
  });

  const { mutate, isPending } = useMutation({
    mutationFn: postRegisterParticipant,
    onSuccess: (resp: BaseResponse) => {
      if (resp.success) {
        setSuccess(true);
        setError("");
      } else {
        setSuccess(true);
        setError(resp.message ?? "Gagal Mendaftarkan Simpatisan");
      }
    },
  });

  const formRef = useRef<HTMLFormElement>(null);

  const onSubmit = async (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault();

    try {
      const formData = new FormData(event.currentTarget);
      const nik = formData.get("nik") as string;
      const full_name = formData.get("nama") as string;
      const email = formData.get("email") as string;
      const pc_name = formData.get("pc") as string;
      const pd_name = formData.get("pd") as string;
      const pw_name = formData.get("pw") as string;
      const phone = formData.get("phone") as string;
      const clothes_size = formData.get("size") as string;
      const photo = formData.get("fotoUrl") as string;
      if (selectedPw === null || selectedPd === null || selectedPc === null) {
        setSuccess(true);
        setError("Silahkan pilih wilayah yang sesuai");
      }
      const payload = {
        nik,
        full_name,
        email,
        pc_name,
        pd_name,
        pw_name,
        pw_code: selectedPw,
        pc_code: selectedPc,
        pd_code: selectedPd,
        photo,
        clothes_size,
        npa: "",
        participant_id: "",
        phone,
        event_ids: isPanitia ? [21] : [20],
      };

      const parsed = DataSimpatisanSchema.parse(payload);
      mutate(parsed as DataAnggota);
    } catch (e) {
      if (e instanceof z.ZodError) {
        setSuccess(true);
        const messages = e.issues.map((err) => {
          return err.message + "\n";
        });
        setError(messages.join(""));
      }
    }
  };

  return (
    <div className="container mx-auto max-w-4xl px-4">
      <form ref={formRef} id="register-simpatisan-form" onSubmit={onSubmit}>
        <div className="grid gap-6 md:grid-cols-2">
          <div className="flex flex-col gap-2 space-y-4">
            <div>
              <Input
                labelPlacement="outside"
                id="nik"
                placeholder="Masukan Nik Anda"
                name="nik"
                label="NIK"
                isRequired
                maxLength={16}
                disabled={isPending}
              />
            </div>
            <div>
              <Input
                labelPlacement="outside"
                id="nama"
                label="Nama"
                name="nama"
                placeholder="Mochamad Rafli"
                isRequired
                disabled={isPending}
              />
            </div>
            <div>
              <Input
                labelPlacement="outside"
                id="email"
                placeholder="example@example.com"
                name="email"
                type="email"
                label="Email"
                isRequired
                disabled={isPending}
              />
            </div>
            <div>
              <Input
                labelPlacement="outside"
                id="phone"
                placeholder="08987654321"
                type="phone"
                name="phone"
                label="Phone"
                isRequired
                disabled={isPending}
                maxLength={15}
              />
            </div>
            <div>
              <Autocomplete
                labelPlacement="outside"
                label="Pimpinan Wilayah"
                name="pw"
                id="pw"
                placeholder="Cari Pimpinan Wilayah Anda"
                className="w-full"
                isRequired
                selectedKey={selectedPw}
                onSelectionChange={(item) => {
                  setIsFakeFetch(true);
                  setSelectedPw(item as string);
                  setSelectedPd(null);
                  setSelectedPc(null);
                  setIsFakeFetch(false);
                  refetchPds();
                }}
              >
                {isFakeFetch ? (
                  <AutocompleteItem
                    key="loading"
                    isReadOnly
                    textValue="Loading..."
                  >
                    <div className="flex w-full justify-center">
                      <Spinner />
                    </div>
                  </AutocompleteItem>
                ) : (
                  pws.map((pw) => (
                    <AutocompleteItem key={pw.kdPw} textValue={pw.namaWilayah}>
                      <div>{pw.namaWilayah}</div>
                    </AutocompleteItem>
                  ))
                )}
              </Autocomplete>
            </div>
            <div>
              <Autocomplete
                labelPlacement="outside"
                label="Pimpinan Daerah"
                name="pd"
                id="pd"
                placeholder="Cari Pimpinan Daerah Anda"
                className="w-full"
                selectedKey={selectedPd}
                isDisabled={!selectedPw}
                isRequired
                onSelectionChange={(item) => {
                  setIsFakeFetch(true);
                  setSelectedPd(null);
                  setSelectedPd(item as string);
                  refetchPcs();
                  setSelectedPc(null);
                  setIsFakeFetch(false);
                }}
              >
                {isFakeFetch ? (
                  <AutocompleteItem
                    key="loading"
                    isReadOnly
                    textValue="Loading..."
                  >
                    <div className="flex w-full justify-center">
                      <Spinner />
                    </div>
                  </AutocompleteItem>
                ) : (
                  pds.map((pd) => (
                    <AutocompleteItem key={pd.kdPd} textValue={pd.namaPd}>
                      <div>{pd.namaPd}</div>
                    </AutocompleteItem>
                  ))
                )}
              </Autocomplete>
            </div>
            <div>
              <Autocomplete
                labelPlacement="outside"
                label="Pimpinan Cabang"
                name="pc"
                id="pc"
                placeholder="Cari Pimpinan Cabang Anda"
                isDisabled={!selectedPd}
                isRequired
                className="w-full"
                selectedKey={selectedPc}
                onSelectionChange={(item) => {
                  setIsFakeFetch(true);
                  setSelectedPc(null);
                  setSelectedPc(item as string);
                  setIsFakeFetch(false);
                }}
              >
                {isFakeFetch ? (
                  <AutocompleteItem
                    key="loading"
                    isReadOnly
                    textValue="Loading..."
                  >
                    <div className="flex w-full justify-center">
                      <Spinner />
                    </div>
                  </AutocompleteItem>
                ) : (
                  pcs.map((pc) => (
                    <AutocompleteItem key={pc.kdPc} textValue={pc.namaPc}>
                      <div>{pc.namaPc}</div>
                    </AutocompleteItem>
                  ))
                )}
              </Autocomplete>
            </div>
            <div>
              <div className="flex w-full justify-center gap-4">
                <div
                  className={`h-fit w-28 rounded-full ${isPending ? "pointer-events-none cursor-none" : ""}`}
                >
                  <ImageUpload
                    name="foto"
                    category="calon_peserta"
                    rounded
                    onChange={(value) => setImageUrl(value)}
                    value={imageUrl}
                    classNames={{ image: "aspect-square object-cover" }}
                  />
                </div>
              </div>
            </div>
          </div>
          <div>
            <div className="space-y-4">
              {isPanitia && (
                <Checkbox disabled isSelected={true}>
                  Mengikuti Acara Sebagai Panitia
                </Checkbox>
              )}
              <div>
                <Image
                  src="/assets/atribut/baju.png"
                  className="h-auto w-full rounded object-contain object-center"
                  alt="Gambar Baju"
                />
                <Image
                  src="/assets/atribut/topi.png"
                  className="h-auto w-full rounded object-contain object-center"
                  alt="Gambar Topi"
                />
                <div className="font-semibold">Ukuran Baju</div>
                <div className="flex items-center gap-2">
                  <RadioGroup isRequired isDisabled={isPending} name="size">
                    <Radio value="S">S</Radio>
                    <Radio value="M">M</Radio>
                    <Radio value="L">L</Radio>
                    <Radio value="XL">XL</Radio>
                    <Radio value="XXL">XXL</Radio>
                    <Radio value="XXXL">XXXL</Radio>
                  </RadioGroup>
                </div>
              </div>
            </div>
          </div>
        </div>
        <h4 className="my-4 w-full text-sm text-gray-600 dark:text-gray-200">
          *Notes: Biaya Pendaftaran sejumlah Rp. 200.000
        </h4>
        <Button
          isDisabled={isPending}
          type="submit"
          className="mt-2 w-full bg-[#89d09d]"
        >
          Submit Pendaftaran
        </Button>
      </form>
      <Modal
        isOpen={success}
        onClose={() => {
          setSuccess(false);
          if (!error) {
            setImageUrl("");
            formRef.current?.reset();
          }
        }}
      >
        <ModalContent>
          <ModalHeader />
          <ModalBody className="w-full items-center font-semibold">
            {error.length > 0 ? (
              <div className="text-red-500">
                {error.split("\n").map((e) => (
                  <p key={e}>{e}</p>
                ))}
              </div>
            ) : (
              <div className="text-green-500">Pendaftaran Berhasil</div>
            )}
          </ModalBody>
          <ModalFooter className="w-full justify-center">
            <Button
              color={error.length > 0 ? "danger" : "success"}
              onPress={() => {
                setSuccess(false);
                if (!error) {
                  setImageUrl("");
                  formRef.current?.reset();
                }
              }}
            >
              {error ? "Ok" : "Daftar Kembali"}
            </Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </div>
  );
};
