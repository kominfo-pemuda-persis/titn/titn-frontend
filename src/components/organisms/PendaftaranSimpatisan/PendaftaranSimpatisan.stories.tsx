import React from "react";
import { Meta } from "@storybook/react";

import Providers from "@/providers";
import { PendaftaranSimpatisan } from "./PendaftaranSimpatisan";

export default {
  title: "Organisms/Pendaftaran Simpatisan",
  component: PendaftaranSimpatisan,
  argTypes: {},
} as Meta<typeof PendaftaranSimpatisan>;

const defaultProps = {};

export const DefaultPage = () => (
  <Providers>
    <PendaftaranSimpatisan />
  </Providers>
);
