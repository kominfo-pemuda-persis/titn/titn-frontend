/* eslint-disable @next/next/no-img-element */
"use client";

import { useEffect, useRef, useState } from "react";
import domtoimage from "dom-to-image";

import { Button } from "@/components/atoms/Button";
import QRCodeGenerator from "@/components/atoms/Qrcode";
import { Spinner } from "@/components/atoms/Spinner";
import { urlToImg } from "@/utils/urlToImg";

const IDCardGenerator = ({
  data: { id, name, avatarUrl, npa, pd, type, event },
  className,
}: {
  data: {
    id: string;
    name: string;
    avatarUrl: string;
    npa: string;
    pd: string;
    type: string;
    event: string[];
  };
  className?: string;
}) => {
  const [mounted, setMounted] = useState<boolean>(false);
  const [generatingCard, setGeneratingCard] = useState<boolean>(false);
  const [photo, setPhoto] = useState<string>("");
  const cardRef = useRef<HTMLDivElement | null>(null);

  const handleDownload = async () => {
    setGeneratingCard(true);
    cardRef.current?.classList.remove(
      "left-[-150%]",
      "top-[-150%]",
      "scale-[0.25]"
    );
    domtoimage
      .toJpeg(cardRef.current as HTMLElement, {
        height: 2000,
        width: 1400,
      })
      .then((dataUrl) => {
        cardRef.current?.classList.add(
          "left-[-150%]",
          "top-[-150%]",
          "scale-[0.25]"
        );
        setGeneratingCard(false);

        const link = document.createElement("a");
        link.href = dataUrl;
        link.download = "id-card.png";
        link.click();
      });
  };

  useEffect(() => {
    if (!mounted) {
      try {
        urlToImg(avatarUrl)
          .then((photoBlob) => {
            setPhoto(`data:image/png;base64,${photoBlob}`);
          })
          .catch((error) => {
            console.log(error);
          })
          .finally(() => setMounted(true));
      } catch (error) {
        console.log(error);
      }
    }
  }, [mounted, avatarUrl]);

  if (!mounted) return <Spinner />;

  return (
    <div className={`mx-auto space-y-4 overflow-clip ${className}`}>
      <div className="relative">
        {generatingCard && (
          <div className="align-center absolute z-20 flex h-[500px] w-[350px] justify-center overflow-hidden bg-white shadow-md">
            <Spinner />
          </div>
        )}
        {/* DEBUG 
        <div className="relative shadow-md">
      */}
        {/* NODEBUG
        <div className="relative h-[500px] w-[350px] overflow-hidden shadow-md">
      */}
        <div className="relative z-10 h-[500px] w-[350px] overflow-hidden shadow-md">
          {/* DEBUG 
          className="relative h-[2000px] w-[1400px]"
        */}
          {/* NODEBUG
          className="relative left-[-150%] top-[-150%] h-[2000px] w-[1400px] scale-[0.25]"
        */}
          <div
            ref={cardRef}
            className="relative left-[-150%] top-[-150%] h-[2000px] w-[1400px] scale-[0.25]"
          >
            <div
              className="absolute bottom-0 left-0 right-0 top-0 min-h-full w-full bg-cover bg-center bg-no-repeat"
              style={{ backgroundImage: `url('/assets/id-card.png')` }}
            />
            <div
              className="absolute left-[186px] top-[550px] h-[624px] w-[460px] bg-cover bg-center bg-no-repeat"
              style={{ backgroundImage: `url('${photo}')` }}
            />
            <p className="absolute left-[40px] top-[1235px] w-[656px] max-w-[600px] overflow-hidden text-ellipsis whitespace-nowrap text-[40px] font-medium !text-primary">
              {name}
            </p>
            <p className="absolute left-[40px] top-[1345px] w-[656px] overflow-hidden text-ellipsis whitespace-nowrap text-[40px] font-medium !text-primary">
              {npa}
            </p>
            <p className="absolute left-[40px] top-[1450px] w-[656px] overflow-hidden text-ellipsis whitespace-nowrap text-[40px] font-medium !text-primary">
              {pd}
            </p>
            <p className="absolute left-[40px] top-[1570px] w-[656px] overflow-hidden text-ellipsis whitespace-nowrap text-[40px] font-medium !text-primary">
              {event.join(", ")}
            </p>

            <div className="absolute right-[110px] top-[1200px] w-[656px] overflow-hidden text-ellipsis whitespace-nowrap text-[40px] font-medium !text-primary">
              <QRCodeGenerator value={id} size={450} />
            </div>

            <div className="absolute right-[-230px] top-[1200px] font-bold !text-primary">
              <p className="m-0 -rotate-90 text-[150px]">{type}</p>
            </div>
          </div>
        </div>
      </div>
      <Button
        fullWidth
        type="button"
        variant="solid"
        color="primary"
        onClick={handleDownload}
      >
        Download
      </Button>

      {/* {generatedImage && (
        <div>
          <h3>Generated Image:</h3>
          <img
            src={generatedImage}
            alt="Generated ID Card"
            className=" h-[500px] w-[350px] border-1 shadow-md"
          />
        </div>
      )} */}
    </div>
  );
};

export default IDCardGenerator;
