import React from "react";
import { Meta } from "@storybook/react";

import Providers from "@/providers";
import PendaftaranWrapper from "./PendaftaranWrapper";

export default {
  title: "Organisms/Pendaftaran Wrapper",
  component: PendaftaranWrapper,
  argTypes: {},
} as Meta<typeof PendaftaranWrapper>;

const defaultProps = {};

export const DefaultPage = () => (
  <Providers>
    <PendaftaranWrapper />
  </Providers>
);
