"use client";

import { Tab, Tabs } from "@/components/molecules/Tabs/Tabs";
import { PendaftaranAnggota } from "@/components/organisms/PendaftaranAnggota";
import { PendaftaranSimpatisan } from "@/components/organisms/PendaftaranSimpatisan";

const PendaftaranWrapper = ({ isPanitia = false }: { isPanitia?: boolean }) => {
  return (
    <Tabs defaultSelectedKey="anggota" variant="underlined" color="primary">
      <Tab key="anggota" title="Anggota">
        <PendaftaranAnggota isPanitia={isPanitia} />
      </Tab>
      <Tab key="simpatisan" title="Simpatisan">
        <PendaftaranSimpatisan isPanitia={isPanitia} />
      </Tab>
    </Tabs>
  );
};

export default PendaftaranWrapper;
