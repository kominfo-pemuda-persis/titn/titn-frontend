"use client";

import { useCallback, useEffect, useState } from "react";
import { usePathname } from "next/navigation";
import { useAtomValue } from "jotai";
import { IconType } from "react-icons";
import { CiMenuBurger } from "react-icons/ci";

import { Button } from "@/components/atoms/Button";
import { SidebarItem } from "@/components/molecules/SidebarItem";
import { NAV_DASHBOARD_ITEMS } from "@/constant/routes";
import { permission } from "@/states/user";

export const SidebarMobile = () => {
  const pathname = usePathname();
  const { routeChecker } = useAtomValue(permission);
  const [moreOpen, setMoreOpen] = useState<boolean>(false);
  const [navigationItem, setNavigationItem] =
    useState<
      { title: string; href: string; icon: IconType; isActive: boolean }[]
    >(NAV_DASHBOARD_ITEMS);

  const checkPathname = useCallback(() => {
    const formattedPathname = `/${pathname.split("/")[1]}`;
    const navPathsIndex = navigationItem.findIndex(
      (item) => item.href === formattedPathname
    );

    if (navPathsIndex === -1) return;
    const activeNav = navigationItem[navPathsIndex];
    if (activeNav.isActive) return;

    if (navPathsIndex > 3) {
      const navItems = navigationItem.filter(
        (item) => item.href !== formattedPathname
      );
      const firstHalf = navItems.slice(0, 2);
      const secondHalf = navItems.slice(2);
      setNavigationItem([...firstHalf, activeNav, ...secondHalf]);
    } else {
      setNavigationItem(
        navigationItem.map((item) => {
          return {
            ...item,
            isActive: item.href === formattedPathname,
          };
        })
      );
    }
  }, [navigationItem, pathname]);

  useEffect(() => {
    checkPathname();
  }, [checkPathname]);

  return (
    <>
      <div className="bottom-0 h-20 md:hidden" />
      <aside className="fixed bottom-10 left-0 z-[20] w-full bg-white px-1 dark:bg-black md:hidden">
        <div
          className={
            "flex flex-wrap items-start gap-3 rounded-t-xl px-3 py-2 transition delay-150 duration-300 ease-in-out " +
            (!moreOpen ? "hidden opacity-0" : "opacity-100")
          }
        >
          {navigationItem
            .slice(3)
            .map(
              (item, index) =>
                routeChecker(item.href) && (
                  <SidebarItem
                    key={index}
                    title={item.title}
                    icon={item.icon}
                    isActive={item.isActive}
                    href={item.href}
                    handleClick={() => setMoreOpen(false)}
                  />
                )
            )}
        </div>
        <div className="flex items-center justify-evenly rounded-xl border border-gray-200 px-1 py-2 dark:border-gray-700">
          {navigationItem
            .slice(0, 3)
            .map(
              (item, index) =>
                routeChecker(item.href) && (
                  <SidebarItem
                    key={index}
                    title={item.title}
                    icon={item.icon}
                    isActive={item.isActive}
                    href={item.href}
                    handleClick={() => setMoreOpen(false)}
                  />
                )
            )}
          {navigationItem.length > 3 && (
            <Button
              isIconOnly
              className="rounded-full bg-transparent"
              onClick={() => setMoreOpen(!moreOpen)}
            >
              <CiMenuBurger />
            </Button>
          )}
        </div>
      </aside>
    </>
  );
};
