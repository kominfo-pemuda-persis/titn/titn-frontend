import { Meta } from "@storybook/react";

import TableCalonPeserta from "@/components/organisms/TableCalonPeserta/TableCalonPeserta";
import Providers from "@/providers";

export default {
  title: "Organisms/TableCalonPeserta",
  component: TableCalonPeserta,
  argTypes: {},
} as Meta<typeof TableCalonPeserta>;

export const DefaultPage = () => (
  <html lang="en">
    <body>
      <Providers>
        <TableCalonPeserta />
      </Providers>
    </body>
  </html>
);
