import { ReactNode, Suspense, useCallback, useMemo } from "react";
import { useCookies } from "next-client-cookies";
import { getKeyValue } from "@nextui-org/react";
import { useQuery } from "@tanstack/react-query";

import { Button } from "@/components/atoms/Button";
import { Chip } from "@/components/atoms/Chip";
import {
  Modal,
  ModalBody,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalProps,
} from "@/components/molecules/Modal";
import TableTemplate from "@/components/molecules/Table/Template";
import { RowPeserta } from "@/constant/data/peserta";
import { listParticipant } from "@/services/query/participant";
import { Participant } from "@/types";
import { getColorBasedOnEventType } from "@/utils";

interface ModalDetailRekapProps extends Omit<ModalProps, "children"> {
  title?: string;
  recapId: number | null;
  closeText?: string;
  handleOpen?: () => void;
  children?: ReactNode;
  isOpen: boolean;
  onClose: () => void;
  isDisabled?: boolean;
  isLoading?: boolean;
  footer?: ReactNode;
}

const ModalDetailRekap = ({
  size = "md",
  backdrop = "blur",
  isDismissable = false,
  isKeyboardDismissDisabled = true,
  title,
  recapId,
  closeText = "Close",
  handleOpen,
  isOpen,
  onClose,
  isDisabled,
  footer,
}: ModalDetailRekapProps) => {
  const cookie = useCookies();
  const session = cookie?.get("session");
  const {
    data: dataDetailRekap,
    isFetching: isFetchingDetailRekap,
    refetch: refetchDetailRekap,
    error: errorDetailRekap,
  } = useQuery({
    queryKey: [recapId],
    enabled: isOpen,
    queryFn: async ({ signal }) => {
      return await listParticipant({
        pageSize: 1000,
        recapId,
        signal,
        session,
      });
    },
  });

  const renderCell = useCallback((peserta: RowPeserta, columnKey: string) => {
    switch (columnKey) {
      case "events":
        return (
          <div className="flex flex-wrap gap-1">
            {peserta?.events?.map((event, index) => (
              <Chip
                color={`${getColorBasedOnEventType(getKeyValue(event.type, columnKey))}`}
                key={index}
                size="sm"
                variant="dot"
              >
                {getKeyValue(event.name, columnKey)}
              </Chip>
            ))}
          </div>
        );

      default:
        return getKeyValue(peserta, columnKey);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const filteredParticipants = useMemo(
    () =>
      dataDetailRekap?.data?.content.map((participant: Participant) => ({
        key: participant.participant_id,
        id: participant.participant_id,
        npa: participant.npa || "-",
        fullName: participant.full_name || "-",
        clothesSize: participant.clothes_size || "-",
        events:
          participant.event_details?.map((event) => ({
            name: event.name,
            type: event.type,
          })) || "-",
      })),
    [dataDetailRekap?.data]
  );

  return (
    <Modal
      size={size}
      placement="center"
      isDismissable={isDismissable}
      isKeyboardDismissDisabled={isKeyboardDismissDisabled}
      backdrop={backdrop}
      isOpen={isOpen}
      onClose={onClose}
    >
      <ModalContent>
        {(setModalClose) => (
          <>
            <ModalHeader className="flex flex-col gap-1">{title}</ModalHeader>
            {recapId && (
              <ModalBody>
                <Suspense>
                  <TableTemplate
                    baseClassName="w-full max-h-[400] h-full"
                    response={dataDetailRekap?.data}
                    isFetching={isFetchingDetailRekap}
                    emptyContent="Calon peserta tidak ditemukan"
                    renderCell={renderCell}
                    isCompact
                    columns={[
                      {
                        name: "ID",
                        uid: "id",
                        sortable: false,
                      },
                      {
                        name: "NPA",
                        uid: "npa",
                        sortable: false,
                      },
                      { name: "NAMA", uid: "fullName", sortable: false },
                      {
                        name: "EVENTS",
                        uid: "events",
                        sortable: false,
                      },
                      {
                        name: "UK. BAJU",
                        uid: "clothesSize",
                        sortable: true,
                      },
                    ]}
                    items={filteredParticipants}
                  />
                </Suspense>
              </ModalBody>
            )}

            <ModalFooter>
              {footer ? (
                footer
              ) : (
                <>
                  <Button
                    isDisabled={isDisabled}
                    size="sm"
                    color="danger"
                    variant="light"
                    onPress={setModalClose}
                  >
                    {closeText}
                  </Button>
                </>
              )}
            </ModalFooter>
          </>
        )}
      </ModalContent>
    </Modal>
  );
};

export default ModalDetailRekap;
