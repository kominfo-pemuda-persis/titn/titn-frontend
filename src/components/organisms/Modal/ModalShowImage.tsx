import { ReactNode } from "react";
import Image from "next/image";

import { Button } from "@/components/atoms/Button";
import { Link } from "@/components/atoms/Link";
import {
  Modal,
  ModalBody,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalProps,
} from "@/components/molecules/Modal";
import { isValidURL } from "@/utils/validUrl";

interface ModalShowImageProps extends Omit<ModalProps, "children"> {
  title?: string;
  url: string;
  cancelText?: string;
  confirmText?: string;
  handleOpen: () => void;
  children?: ReactNode;
  isOpen: boolean;
  onClose: () => void;
  isDisabled?: boolean;
  isLoading?: boolean;
}

const ModalShowImage = ({
  size = "md",
  backdrop = "blur",
  isDismissable = false,
  isKeyboardDismissDisabled = true,
  title,
  cancelText = "Close",
  confirmText = "Download",
  isOpen,
  onClose,
  url,
}: ModalShowImageProps) => {
  function checkImage(url: string) {
    if (isValidURL(url)) {
      return (
        <Image alt="nextui logo" src={url} layout="fill" objectFit="contain" />
      );
    } else {
      return <p>image URL is not valid url</p>;
    }
  }
  return (
    <Modal
      size={size}
      isDismissable={isDismissable}
      isKeyboardDismissDisabled={isKeyboardDismissDisabled}
      backdrop={backdrop}
      isOpen={isOpen}
      onClose={onClose}
      hideCloseButton
    >
      <ModalContent>
        <>
          {title && (
            <ModalHeader className="flex flex-col gap-1">{title}</ModalHeader>
          )}
          <ModalBody>
            <div className="h-[70vh]">{checkImage(url)}</div>
          </ModalBody>
          <ModalFooter>
            <Button color="danger" onPress={onClose}>
              {cancelText}
            </Button>
            <Button href={url} as={Link} color="primary" showAnchorIcon>
              {confirmText}
            </Button>
          </ModalFooter>
        </>
      </ModalContent>
    </Modal>
  );
};

export default ModalShowImage;
