import { ReactNode } from "react";

import { Button } from "@/components/atoms/Button";
import {
  Modal,
  ModalBody,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalProps,
} from "@/components/molecules/Modal";

interface ModalConfirmationProps extends Omit<ModalProps, "children"> {
  title?: string;
  message?: ReactNode;
  cancelText?: string;
  confirmText?: string;
  handleOpen?: () => void;
  children?: ReactNode;
  isOpen: boolean;
  onClose: () => void;
  isDisabled?: boolean;
  isLoading?: boolean;
  footer?: ReactNode;
}

const ModalConfirmation = ({
  size = "md",
  backdrop = "blur",
  isDismissable = false,
  isKeyboardDismissDisabled = true,
  title,
  message,
  cancelText = "No",
  confirmText = "Yes",
  handleOpen,
  isOpen,
  onClose,
  isDisabled,
  isLoading,
  footer,
}: ModalConfirmationProps) => (
  <Modal
    size={size}
    isDismissable={isDismissable}
    isKeyboardDismissDisabled={isKeyboardDismissDisabled}
    backdrop={backdrop}
    isOpen={isOpen}
    onClose={onClose}
  >
    <ModalContent>
      {(setModalClose) => (
        <>
          <ModalHeader className="flex flex-col gap-1">{title}</ModalHeader>
          {message && <ModalBody>{message}</ModalBody>}

          <ModalFooter>
            {footer ? (
              footer
            ) : (
              <>
                <Button
                  isDisabled={isDisabled}
                  size="sm"
                  color="danger"
                  variant="light"
                  onPress={setModalClose}
                >
                  {cancelText}
                </Button>
                <Button
                  isDisabled={isDisabled}
                  size="sm"
                  isLoading={isLoading}
                  color="primary"
                  onPress={() => {
                    handleOpen && handleOpen();
                    setModalClose();
                  }}
                >
                  {confirmText}
                </Button>
              </>
            )}
          </ModalFooter>
        </>
      )}
    </ModalContent>
  </Modal>
);

export default ModalConfirmation;
