import React from "react";
import { Meta } from "@storybook/react";

import Providers from "@/providers";
import { PendaftaranAnggota } from "./PendaftaranAnggota";

export default {
  title: "Organisms/Pendaftaran Anggota",
  component: PendaftaranAnggota,
  argTypes: {},
} as Meta<typeof PendaftaranAnggota>;

const defaultProps = {};

export const DefaultPage = () => (
  <Providers>
    <PendaftaranAnggota />
  </Providers>
);
