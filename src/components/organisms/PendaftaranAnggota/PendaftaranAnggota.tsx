"use client";

import { FormEvent, useRef, useState } from "react";
import { useMutation, useQuery } from "@tanstack/react-query";
import { useAtomValue } from "jotai/react";
import { z } from "zod";

import { Button } from "@/components/atoms/Button";
import { Checkbox } from "@/components/atoms/Checkbox";
import { Divider } from "@/components/atoms/Divider";
import { Image } from "@/components/atoms/Image";
import { Input } from "@/components/atoms/Input";
import { Paragraph, SectionTitle, Subtitle } from "@/components/atoms/Text";
import ListKompetisi from "@/components/molecules/FormItem/ListKompetisi";
import UkuranBaju from "@/components/molecules/FormItem/UkuranBaju";
import ImageUpload from "@/components/molecules/ImageUpload/ImageUpload";
import {
  Modal,
  ModalBody,
  ModalContent,
  ModalFooter,
  ModalHeader,
} from "@/components/molecules/Modal";
import { Radio, RadioGroup } from "@/components/molecules/RadioGroup";
import { postRegisterParticipant } from "@/services/mutation/participant";
import { getUserByNpa } from "@/services/query/user";
import { selectedEventsState } from "@/states/form";
import { BaseResponse, DataAnggota, DataAnggotaSchema } from "@/types";

export const PendaftaranAnggota = ({
  isPanitia = false,
}: {
  isPanitia?: boolean;
}) => {
  const userSelectedEvents = useAtomValue(selectedEventsState);
  const [anggota, setAnggota] = useState<DataAnggota>({} as DataAnggota);
  const [npa, setNpa] = useState<string>("");
  const [isOpenModal, setIsOpenModal] = useState<boolean>(false);
  const [error, setError] = useState<string>("");
  const [npaError, setNpaError] = useState<string>("");
  const [panitiaId, setPanitiaId] = useState<string>("21");
  const eventRef = useRef(null);
  const clothesSizeRef = useRef(null);
  const formRef = useRef<HTMLFormElement>(null);

  useQuery({
    queryKey: ["anggota", npa],
    queryFn: async ({ signal }) => {
      const result = await getUserByNpa(npa, { signal });

      if (!result.data) {
        setNpaError(result.response.data.errors[0]);
        setAnggota({} as DataAnggota);
        formRef.current?.reset();
      } else {
        setNpaError("");

        const { nama, email, pc, pd, pw, foto, noTelpon, id } = result.data;

        setAnggota((prev) => ({
          ...prev,
          participant_id: (id as string) ?? "",
          phone: noTelpon ?? "",
          full_name: nama ?? "",
          email: email ?? "",
          pc_name: pc.namaPc ?? "",
          pd_name: pd.namaPd ?? "",
          pw_name: pw.namaWilayah ?? "",
          pc_code: pc.kdPc ?? "",
          pd_code: pd.kdPd ?? "",
          pw_code: pw.kdPw ?? "",
          photo: foto ?? "",
        }));
      }
      return result;
    },
    retry: 0,
    enabled: npa.length === 7,
  });

  const getNpa = async (npa: string) => {
    setNpa(npa);
  };

  const { mutate, isPending } = useMutation({
    mutationFn: postRegisterParticipant,
    onSuccess: (resp: BaseResponse) => {
      if (resp.success) {
        setIsOpenModal(true);
        setError("");
      } else {
        setIsOpenModal(true);
        setError(resp.message ?? "Gagal Mendaftarkan Anggota");
      }
    },
  });

  const onSubmit = async (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault();

    try {
      const formData = new FormData(event.currentTarget);
      const event_ids = isPanitia
        ? [Number(panitiaId)]
        : userSelectedEvents.events;
      const clothes_size = formData.get("clothes_size") as string;

      if (event_ids.length === 0 || clothes_size === "invalid") {
        setIsOpenModal(true);
        setError(
          "Gagal Mendaftarkan Anggota. Silahkan pilih event dan pilih ukuran baju."
        );
        return;
      }

      const payload = { ...anggota, npa, event_ids, clothes_size };
      const parsed = DataAnggotaSchema.parse(payload);
      mutate(parsed);

      formRef.current?.reset();
    } catch (e) {
      if (e instanceof z.ZodError) {
        setIsOpenModal(true);
        const messages = e.issues.map((err) => {
          return err.message + "\n";
        });
        setError(messages.join(""));
      } else {
        setIsOpenModal(true);
        setError(e as string);
      }
    }
  };

  return (
    <div className="container mx-auto max-w-4xl px-4">
      {error ? (
        <div className="m-3 w-full rounded-2xl bg-red-100 p-2 text-center">
          <p className="text-red-500">{error}</p>
        </div>
      ) : null}
      <form ref={formRef} id="register-anggota-form" onSubmit={onSubmit}>
        <div className="grid gap-6 md:grid-cols-2">
          <div className="flex min-w-[243.5px] flex-col gap-2 space-y-4">
            <div>
              <Input
                maxLength={7}
                labelPlacement="outside"
                label="NPA"
                onChange={(e) => {
                  setNpaError("");
                  getNpa(e.target.value);

                  if (e.target.value.length < 7) {
                    setAnggota({} as DataAnggota);
                  }
                }}
                placeholder="Masukan Kode NPA Anda"
                value={npa}
                isRequired
                disabled={isPending}
              />
              {npaError ? (
                <small className="text-danger-500">{npaError}</small>
              ) : null}
            </div>
            <div>
              <Input
                labelPlacement="outside"
                id="nama"
                label="Nama"
                placeholder="Mochamad Rafli"
                value={anggota?.full_name}
                isReadOnly
                isDisabled
                isRequired
              />
            </div>
            <div>
              <Input
                labelPlacement="outside"
                id="email"
                placeholder="example@example.com"
                type="email"
                label="Email"
                value={anggota?.email}
                isReadOnly
                isDisabled
                isRequired
              />
            </div>
            <div>
              <Input
                labelPlacement="outside"
                id="phone"
                placeholder="+6281234567890"
                type="phone"
                label="Phone"
                value={anggota?.phone}
                isReadOnly
                isDisabled
                isRequired
              />
            </div>
            <div>
              <Input
                labelPlacement="outside"
                label="Pimpinan Wilayah"
                name="pw"
                id="pw"
                placeholder="Cari Pimpinan Wilayah Anda"
                className="w-full"
                isRequired
                isDisabled
                isReadOnly
                value={anggota?.pw_name}
              />
            </div>
            <div>
              <Input
                labelPlacement="outside"
                id="pd"
                label="Pimpinan Daerah"
                className="w-full"
                placeholder="Pilih Pimpinan Daerah Anda"
                isReadOnly
                isDisabled
                isRequired
                value={anggota?.pd_name}
              />
            </div>
            <div>
              <Input
                labelPlacement="outside"
                id="pc"
                label="Pimpinan Cabang"
                className="w-full"
                placeholder="Pilih Pimpinan Cabang Anda"
                isReadOnly
                isDisabled
                isRequired
                value={anggota?.pc_name}
              />
            </div>
            <div>
              <div className="flex w-full justify-center gap-4">
                <div
                  className={`h-fit w-28 rounded-full ${isPending ? "pointer-events-none cursor-none" : ""}`}
                >
                  <ImageUpload
                    name="foto"
                    rounded
                    category="calon_peserta"
                    onChange={(value: string) =>
                      setAnggota((prev) => ({ ...prev, photo: value }))
                    }
                    value={anggota?.photo}
                    classNames={{
                      image:
                        "aspect-square object-cover border-2 border-gray-200",
                    }}
                  />
                </div>
              </div>
            </div>
          </div>
          <div className="space-y-4">
            {!isPanitia ? (
              <ListKompetisi isDisabled={isPending} ref={eventRef} />
            ) : (
              <RadioGroup
                className="mt-2 flex flex-col gap-2"
                value={panitiaId}
                onValueChange={setPanitiaId}
                isDisabled={isPending}
              >
                <Radio value="21">Mengikuti Acara Sebagai Panitia</Radio>
                <Radio value="22">Mengikuti Acara Sebagai Official</Radio>
              </RadioGroup>
            )}

            <div className="space-y-2">
              <Image
                src="/assets/atribut/baju.png"
                className="h-auto w-full rounded object-contain object-center"
                alt="Gambar Baju"
              />
              <Image
                src="/assets/atribut/topi.png"
                className="h-auto w-full rounded object-contain object-center"
                alt="Gambar Topi"
              />
              <UkuranBaju
                isDisabled={isPending}
                value={anggota?.clothes_size as string}
                handleOnChange={(e) =>
                  setAnggota((prev) => ({
                    ...prev,
                    clothes_size: e.target.value,
                  }))
                }
                ref={clothesSizeRef}
              />
            </div>
          </div>
        </div>
        <h4 className="my-4 w-full text-sm text-gray-600 dark:text-gray-200">
          *Notes: Biaya Pendaftaran sejumlah Rp. 200.000
        </h4>
        <Button
          type="submit"
          isDisabled={isPending}
          className="mt-2 w-full bg-[#89d09d]"
        >
          Submit Pendaftaran
        </Button>
      </form>
      <Modal
        isOpen={isOpenModal}
        onClose={() => {
          setIsOpenModal(false);
          if (!error) {
            setNpa("");
            setAnggota({} as DataAnggota);
            formRef.current?.reset();
          }
          setError("");
        }}
      >
        <ModalContent>
          <ModalHeader />
          <ModalBody className="w-full items-center">
            {error.length > 0 ? (
              <>
                <Subtitle
                  text="Oops..."
                  className="font-semibold text-green-500"
                />
                <Divider />

                <ul className="list-inside list-disc">
                  {error
                    .split("\n")
                    .filter(Boolean)
                    .map((e) => (
                      <li key={e} className="list-item text-red-500">
                        <span className="text-sm">
                          {e.charAt(0).toUpperCase() + e.slice(1).toLowerCase()}
                        </span>
                      </li>
                    ))}
                </ul>
              </>
            ) : (
              <>
                <SectionTitle
                  text="Pendaftaran Berhasil"
                  className="text-green-500"
                />
                <Divider />
                <Paragraph text="Silahkan periksa email anda!" />
              </>
            )}
          </ModalBody>
          <ModalFooter className="w-full justify-center">
            <Button
              size="sm"
              color={error.length > 0 ? "danger" : "success"}
              onPress={() => {
                setIsOpenModal(false);
                if (!error) {
                  setNpa("");
                  setAnggota({
                    npa: "",
                    full_name: "",
                    email: "",
                    phone: "",
                    pw_name: "",
                    pc_name: "",
                    pd_name: "",
                    photo: "",
                    clothes_size: "invalid",
                    event_ids: [],
                  });
                  formRef.current?.reset();
                }
                setError("");
              }}
            >
              {error ? "Kembali" : "Ok"}
            </Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </div>
  );
};
