"use client";

import { Key, useCallback, useEffect, useMemo, useState } from "react";
import { useCookies } from "next-client-cookies";
import dynamic from "next/dynamic";
import { useRouter, useSearchParams } from "next/navigation";
import { getKeyValue, useDisclosure } from "@nextui-org/react";
import { useQuery } from "@tanstack/react-query";
import { useAtom } from "jotai";
import { BsEnvelope, BsSend, BsTrash, BsWhatsapp } from "react-icons/bs";

import { Button } from "@/components/atoms/Button";
import { Chip } from "@/components/atoms/Chip";
import Toast from "@/components/atoms/Toast";
import {
  Dropdown,
  DropdownItem,
  DropdownMenu,
  DropdownTrigger,
} from "@/components/molecules/Dropdown";
import TableTemplate from "@/components/molecules/Table/Template";
import { COLUMNS_SPINNER, RowPeserta } from "@/constant/data/peserta";
import {
  deleteParticipant,
  updateSendNotification,
} from "@/services/mutation/participant";
import { listParticipant } from "@/services/query/participant";
import { tableStates } from "@/states/table";
import { Participant, ParticipantStatus } from "@/types/participant";
import { NotificationType, Sort } from "@/types/utils";
import { getColorBasedOnEventType, getColorBasedOnStatus } from "@/utils";
import { sleep } from "@/utils/debounce";

const ModalConfirmation = dynamic(
  () => import("@/components/organisms/Modal/ModalConfirmation"),
  {
    ssr: false,
  }
);

const TablePesertaSpinned = () => {
  const router = useRouter();
  const { onOpen, isOpen, onClose } = useDisclosure();
  const [
    { searchValue, page, rowsPerPage, sortDescriptor, statusFilter },
    setTableConfig,
  ] = useAtom(tableStates);
  const cookie = useCookies();
  const session = cookie?.get("session");

  const searchParams = useSearchParams();
  const [modalValue, setModalValue] = useState<{
    title: string;
    message: string;
    onConfirm: () => void;
  }>({
    title: "",
    message: "",
    onConfirm: () => {},
  });
  const [isLoading, setIsLoading] = useState<boolean>(false);

  const { data, isFetching, refetch, error } = useQuery({
    queryKey: [
      searchValue,
      rowsPerPage,
      page,
      Array.from(statusFilter),
      sortDescriptor.column,
      sortDescriptor.direction,
    ],
    enabled: Array.from(statusFilter).length !== 0,
    queryFn: async ({ signal }) => {
      await sleep(750);

      return await listParticipant({
        keyword: searchValue,
        pageSize: rowsPerPage,
        pageNo: page > 0 ? page - 1 : 0,
        signal,
        isAnggota: true,
        status: ParticipantStatus.CHECKED_IN,
        session,
        isSpinner: true,
        sortBy: sortDescriptor.column as string,
        sortOrder:
          sortDescriptor.direction === "ascending" ? Sort.ASC : Sort.DESC,
      });
    },
  });

  const response = useMemo(
    () => (isFetching ? [] : data?.data),
    [data?.data, isFetching]
  );

  const filteredItems: RowPeserta[] = useMemo(() => {
    let mappedData = response?.content?.map(
      (item: Participant, index: number) => {
        return {
          key: item.participant_id,
          npa: item.npa || "-",
          fullName: item.full_name || "-",
          email: item.email || "-",
          phone: item.phone || "-",
          createdAt: new Date(item.created_at)
            .toLocaleDateString("id-ID", {
              year: "numeric",
              month: "long",
              day: "numeric",
              hour: "numeric",
              minute: "numeric",
              timeZoneName: "short",
            })
            .replace("pukul", ""),
          updatedAt: new Date(item.updated_at)
            .toLocaleDateString("id-ID", {
              year: "numeric",
              month: "long",
              day: "numeric",
              hour: "numeric",
              minute: "numeric",
              timeZoneName: "short",
            })
            .replace("pukul", ""),
          pcName: item.pc_name || "-",
          pdName: item.pd_name || "-",
          pwName: item.pw_name || "-",
          status: item.status || "-",
          clothesSize: item.clothes_size || "-",
          events:
            item.event_details?.map((event) => ({
              name: event.name,
              type: event.type,
            })) || "-",
          no: page === 1 ? index + 1 : (page - 1) * rowsPerPage + index + 1,
        };
      }
    );

    return mappedData;
  }, [page, response?.content, rowsPerPage]);

  const handleDelete = async (id: string) => {
    setModalValue({
      title: "Hapus Peserta",
      message: "Apakah Anda yakin ingin menghapus peserta ini?",
      onConfirm: async () => {
        setIsLoading(true);
        const response = await deleteParticipant(id);
        if (response.success) {
          Toast({
            message: "Peserta berhasil dihapus",
            type: "success",
          });
          refetch();
        } else {
          Toast({
            message: response.message as string,
            type: "error",
          });
          console.log(response.message);
        }
        setIsLoading(false);
      },
    });
    onOpen();
  };

  const getDownloadableData = async () => {
    try {
      const participantsForDownload = await listParticipant({
        keyword: searchValue,
        pageSize: 1500,
        isAnggota: true,
        status:
          Array.from(statusFilter).length > 0
            ? (Array.from(statusFilter)[0] as ParticipantStatus)
            : undefined,
        session,
      });

      if (participantsForDownload.status === "OK") {
        return participantsForDownload.data?.content?.map(
          (item: Participant, index: number) => ({
            No: index + 1,
            NPA: item.npa || "-",
            Nama: item.full_name?.replace(/,/g, "") || "-",
            Email: item.email || "-",
            Phone: item.phone || "-",
            PC: item.pc_name || "-",
            PD: item.pd_name || "-",
            PW: item.pw_name || "-",
            Status: item.status || "-",
            "Ukuran Baju": item.clothes_size || "-",
            CreatedAt: new Date(item.created_at)
              .toLocaleDateString("id-ID", {
                year: "numeric",
                month: "long",
                day: "numeric",
                hour: "numeric",
                minute: "numeric",
                timeZoneName: "short",
              })
              .replace("pukul", ""),
            Events:
              item.event_details?.map((event) => event.name).join(",") || "-",
          })
        );
      } else {
        throw new Error(participantsForDownload.message);
      }
    } catch (error: any) {
      console.log(error);

      Toast({
        message: error.message,
        type: "error",
      });
    }
  };

  const handleSendNotification = async (key: Key, participantId: string) => {
    setModalValue({
      title: "Kirim Ulang Notifikasi",
      message: `Apakah Anda ingin kirim ulang notifikasi ${key} untuk id ${participantId}?`,
      onConfirm: async () => {
        try {
          setIsLoading(true);

          Toast({
            message: "Mengirimkan notifikasi",
            type: "loading",
          });

          const response = await updateSendNotification({
            type: key as NotificationType,
            participantId,
            isForce: true,
          });

          if (response.success) {
            Toast({
              message: "Berhasil mengirimkan notifikasi",
              type: "success",
            });
          } else {
            throw new Error(response.message);
          }
        } catch (error: any) {
          console.log(error);

          Toast({
            message: error.message,
            type: "error",
          });
        } finally {
          setIsLoading(false);
        }
      },
    });
    onOpen();
  };

  const renderCell = useCallback((peserta: RowPeserta, columnKey: string) => {
    switch (columnKey) {
      case "events":
        return (
          <div className="flex flex-wrap gap-1">
            {peserta?.events?.map((event, index) => (
              <Chip
                color={`${getColorBasedOnEventType(getKeyValue(event.type, columnKey))}`}
                key={index}
                size="sm"
                variant="dot"
              >
                {getKeyValue(event.name, columnKey)}
              </Chip>
            ))}
          </div>
        );

      case "status":
        return (
          <Chip
            color={`${getColorBasedOnStatus(getKeyValue(peserta, columnKey))}`}
            size="sm"
            variant="flat"
          >
            {getKeyValue(peserta, columnKey)}
          </Chip>
        );

      case "actions":
        return (
          <div className="relative flex items-center justify-center gap-2">
            {peserta.status === ParticipantStatus.APPROVED && (
              <Dropdown>
                <DropdownTrigger>
                  <Button
                    variant="light"
                    color="primary"
                    title="Resend"
                    size="sm"
                    isIconOnly
                  >
                    <BsSend size={20} />
                  </Button>
                </DropdownTrigger>
                <DropdownMenu
                  variant="faded"
                  aria-label="Action resend notification"
                  onAction={(key) => handleSendNotification(key, peserta.key)}
                >
                  <DropdownItem
                    key={NotificationType.EMAIL}
                    startContent={<BsEnvelope />}
                  >
                    Email
                  </DropdownItem>
                  <DropdownItem
                    key={NotificationType.WA}
                    startContent={<BsWhatsapp />}
                  >
                    WhatsApp
                  </DropdownItem>
                  <DropdownItem
                    key={NotificationType.ALL}
                    startContent={<BsSend />}
                  >
                    Email & WhatsApp
                  </DropdownItem>
                </DropdownMenu>
              </Dropdown>
            )}

            <Button
              isIconOnly
              size="sm"
              variant="light"
              color="secondary"
              title="Delete"
              onClick={() => handleDelete(peserta.key)}
            >
              <BsTrash size={20} />
            </Button>
          </div>
        );

      default:
        return getKeyValue(peserta, columnKey);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    const hasFilter = searchParams.has("filter");

    setTableConfig((prev) => ({
      ...prev,
      statusFilter: hasFilter
        ? new Set([searchParams.get("filter") as string])
        : new Set([ParticipantStatus.APPROVED]),
      sortDescriptor: {
        column: "updatedAt",
        direction: "descending",
      },
    }));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [searchParams]);

  useEffect(() => {
    if (error) {
      Toast({
        message: "Gagal mendapatkan data",
        type: "error",
      });
    }
  }, [error]);

  return (
    <>
      <TableTemplate
        response={response}
        isFetching={isFetching}
        emptyContent="Peserta tidak ditemukan"
        renderCell={renderCell}
        columns={COLUMNS_SPINNER}
        items={filteredItems}
        onRowAction={(key) => router.push(`/peserta/${key}`)}
        downloadOptions={{
          fileName: "DATA_PESERTA",
          isEnabled: true,
          getData: getDownloadableData,
        }}
      />

      <ModalConfirmation
        isOpen={isOpen}
        onClose={onClose}
        title={modalValue.title}
        message={modalValue.message}
        handleOpen={modalValue.onConfirm}
        isDisabled={isLoading}
        isLoading={isLoading}
      />
    </>
  );
};

export default TablePesertaSpinned;
