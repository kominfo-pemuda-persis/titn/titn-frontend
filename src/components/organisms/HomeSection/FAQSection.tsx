"use client";

import Link from "next/link";

import { Accordion, AccordionItem } from "@/components/atoms/Accordion";
import { SectionTitle, Subtitle } from "@/components/atoms/Text";
import { FullScreenContainer } from "@/components/template/Container";

const FAQSection = ({ className = "" }: { className?: string }) => {
  const faqContent = [
    {
      title: "Siapa saja peserta TITN Pemuda Persis ?",
      content: (
        <>
          Peserta TITN Pemuda Persis 2024 ini terdiri dari Anggota Pemuda Persis
          dengan status aktif di{" "}
          <a
            href="https://anaonline.id"
            target="_blank"
            className="text-primary underline"
          >
            anaonline.id
          </a>{" "}
          dan kader pemuda dengan status non anggota atau non aktif,
        </>
      ),
    },
    {
      title:
        "Apakah simpatisan dapat mengikuti event kompetisi dan rekreasi olahraga di TITN Pemuda Persis ?",
      content: (
        <>
          Peserta TITN Pemuda Persis 2024 yang berasal dari kader pemuda dengan
          status non anggota / non aktif tidak dapat mengikuti even kompetisi
          atau rekreasi olahraga.
        </>
      ),
    },
    {
      title:
        "Apa saja jenis acara yang akan diadakan dalam TITN Pemuda Persis?",
      content: (
        <ul className="list-inside list-disc">
          <li className="list-item">
            <b>Orasi Ilmiah</b>: Presentasi dan diskusi tentang isu-isu terkini
            dalam Islam dan pemuda.
          </li>
          <li className="list-item">
            <b>TalkShow</b>: Diskusi interaktif dengan narasumber terkait
            tema-tema relevan.
          </li>
          <li className="list-item">
            <b>Workshop</b>: Kegiatan praktis untuk meningkatkan keterampilan
            dan pengetahuan peserta.
          </li>
          <li className="list-item">
            <b>Manifesto Pemuda Persis</b>: Penyampaian visi, misi, dan komitmen
            Pemuda Persis.
          </li>
          <li className="list-item">
            <b>Kompetisi dan Rekreasi Olahraga</b>: Lomba dan kegiatan olahraga
            untuk mempererat hubungan antarpeserta.
          </li>
          <li className="list-item">
            <b>Charity Summit</b>: Diskusi tentang aksi sosial dan kemanusiaan.
          </li>
          <li className="list-item">
            <b>Bazzar dan Expo</b>: Pameran produk dan inovasi dari peserta.
          </li>
          <li className="list-item">
            <b>Pentas Seni</b>: Pertunjukan seni dan budaya.
          </li>
        </ul>
      ),
    },
    {
      title: "Apa tujuan dari TITN Pemuda Persis ?",
      content: (
        <ul className="list-inside list-disc">
          <li className="list-item">
            <b>Menguatkan mentalitas kepemimpinan kader</b>: Mengembangkan
            kepemimpinan yang berbasis nilai-nilai Islam.
          </li>
          <li className="list-item">
            <b>Soliditas jam&apos;iyyah</b>: Meningkatkan kebersamaan dan
            kekompakan antaranggota Pemuda Persis.
          </li>
          <li className="list-item">
            <b>Sinergitas dakwah</b>: Memperkuat kerjasama dalam menyebarkan
            dakwah Islam.
          </li>
          <li className="list-item">
            <b>Menjalin silaturrahim</b>: Membangun hubungan baik antarpeserta.
          </li>
          <li className="list-item">
            <b>Kolaborasi dan rekreasi kader</b>: Memberikan ruang pembinaan
            secara jasadiyah, ruhiyah, fikriyah, dan amaliyah.
          </li>
        </ul>
      ),
    },
    {
      title: "Berapa biaya infaq peserta TITN Pemuda Persis 2024 ?",
      content: (
        <>
          Biaya infaq TITN Pemuda Persis 2024 senilai <b>Rp. 200.000</b>,- via
          Pimpinan Cabang masing-masing untuk kemudian ditransferkan ke rekening
          kepanitiaan
        </>
      ),
    },
    {
      title: "Seperti apa aturan aturan perlombaan yang ada?",
      content: (
        <>
          Sudah kami tuangkan dalam halaman{" "}
          <Link href="/ketentuan" className="text-primary underline">
            ketentuan lomba
          </Link>
          .
        </>
      ),
    },
  ];

  return (
    <FullScreenContainer id="faq" className={className}>
      <div className="space-y-4 text-center">
        <Subtitle
          text="Frequently Asked Question"
          className="font-bold uppercase text-primary"
        />
        <Accordion variant="splitted" className="w-full max-w-[800px]">
          {faqContent.map((faq, index) => (
            <AccordionItem
              key={index}
              aria-label={faq.title}
              title={
                <SectionTitle
                  className="font-medium text-primary"
                  text={faq.title}
                />
              }
              className="text-left"
            >
              {faq.content}
            </AccordionItem>
          ))}
        </Accordion>
      </div>
    </FullScreenContainer>
  );
};

export default FAQSection;
