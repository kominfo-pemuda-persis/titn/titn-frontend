import { Image } from "@/components/atoms/Image";
import { Paragraph, SectionTitle } from "@/components/atoms/Text";
import { FullScreenContainer } from "@/components/template/Container";
import { sponsorSmall } from "@/constant/data/sponsor";

const SponsorSection = ({ className = "" }: { className?: string }) => (
  <FullScreenContainer id="hero" className={className}>
    <div className="relative max-w-3xl space-y-12 text-center">
      <div className="space-y-4 md:space-y-8">
        <SectionTitle
          text="Sponsored by :"
          className="text-center !text-2xl text-primary"
        />

        <div className="flex flex-wrap items-center justify-center gap-2 md:gap-2">
          {sponsorSmall?.map((sponsor, index) => (
            <div
              key={index}
              className="flex h-full flex-col items-center justify-between"
            >
              <Image
                src={sponsor.logo}
                alt={sponsor.name}
                width={0}
                height={0}
                sizes="(max-width: 640px) 100px, (max-width: 768px) 100px, (max-width: 1024px) 100px, 100px"
                className="h-full w-full max-w-[100px] md:max-w-[200px] "
                title={sponsor.name}
              />
              <Paragraph
                text={sponsor.name}
                className="text-xs text-secondary md:text-sm"
              />
            </div>
          ))}
        </div>
      </div>
    </div>
  </FullScreenContainer>
);

export default SponsorSection;
