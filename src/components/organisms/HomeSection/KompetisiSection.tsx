import Link from "next/link";

import { Card, CardFooter } from "@/components/atoms/Card";
import { Image } from "@/components/atoms/Image";
import { FullScreenContainer } from "@/components/template/Container";
import { DATA_KOMPETISI } from "@/constant/data/kompetisi";

const KompetisiSection = ({ className = "" }: { className?: string }) => {
  return (
    <FullScreenContainer id="kompetisi" className={className}>
      {DATA_KOMPETISI.map((item, index) => (
        <div className="w-full" key={index}>
          <section className="bg-muted w-full py-12 md:py-24 lg:py-32">
            <div
              className={`container flex items-center gap-6 px-4 md:px-6 lg:gap-12 ${index % 2 === 0 ? "flex-col lg:flex-row" : "flex-col-reverse lg:flex-row-reverse"}`}
            >
              <div className="space-y-4">
                <h2 className="text-3xl font-bold tracking-tighter md:text-4xl/tight">
                  {item.title}
                </h2>
                <p className="text-muted-foreground max-w-[600px] md:text-xl/relaxed lg:text-base/relaxed xl:text-xl/relaxed">
                  {item.description}
                </p>
                <Link
                  href="/daftar"
                  className="focus-visible:ring-ring inline-flex h-10 items-center justify-center rounded-md bg-primary px-8 text-sm font-medium text-primary-foreground shadow transition-colors hover:bg-primary/90 focus-visible:outline-none focus-visible:ring-1 disabled:pointer-events-none disabled:opacity-50"
                  prefetch={false}
                >
                  Daftar Sekarang
                </Link>
              </div>
              <div className="grid grid-cols-3 gap-4">
                {item.listKompetisi.map((cabang, index) => (
                  <Card
                    key={index}
                    isFooterBlurred
                    radius="lg"
                    className="border-none"
                  >
                    <Image
                      alt={cabang.title}
                      className="object-cover"
                      height={200}
                      width={200}
                      src={`/assets/kompetisi/${cabang.path}.webp`}
                    />
                    <CardFooter className="absolute bottom-1 z-10 overflow-hidden rounded-large border-1 border-white/20 py-1 shadow-small before:rounded-xl before:bg-white/10">
                      <p className="w-full text-center text-tiny font-bold text-white/80">
                        {cabang.title}
                      </p>
                    </CardFooter>
                  </Card>
                ))}
              </div>
            </div>
          </section>
        </div>
      ))}
    </FullScreenContainer>
  );
};

export default KompetisiSection;
