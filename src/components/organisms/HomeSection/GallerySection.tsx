import { Subtitle } from "@/components/atoms/Text";
import { Gallery } from "@/components/organisms/Gallery/Gallery";
import { FullScreenContainer } from "@/components/template/Container";

const GallerySection = ({ className = "" }: { className?: string }) => {
  const slideImages = [
    {
      src: "/assets/gallery/titn_1.webp",
      alt: "titn-gallery-1",
    },
    {
      src: "/assets/gallery/titn_2.webp",
      alt: "titn-gallery-2",
    },
    {
      src: "/assets/gallery/titn_3.webp",
      alt: "titn-gallery-3",
    },
    {
      src: "/assets/gallery/titn_4.webp",
      alt: "titn-gallery-4",
    },
    {
      src: "/assets/gallery/titn_5.webp",
      alt: "titn-gallery-5",
    },
    {
      src: "/assets/gallery/titn_6.webp",
      alt: "titn-gallery-6",
    },
    {
      src: "/assets/gallery/titn_7.webp",
      alt: "titn-gallery-7",
    },
    {
      src: "/assets/gallery/titn_8.webp",
      alt: "titn-gallery-8",
    },
  ];

  return (
    <FullScreenContainer id="gallery" className={className}>
      <div className="max-h-[75vh] w-full space-y-4 text-center">
        <Subtitle
          text="Gallery Sejarah TITN"
          className="font-bold uppercase text-primary"
        />
        <Gallery slideImages={slideImages} className="max-h-[500px] " />
      </div>
    </FullScreenContainer>
  );
};

export default GallerySection;
