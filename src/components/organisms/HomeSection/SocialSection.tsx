import Link from "next/link";

import { Card } from "@/components/atoms/Card";
import { Paragraph, Subtitle } from "@/components/atoms/Text";
import { Footer } from "@/components/organisms/Footer";
import { FullScreenContainer } from "@/components/template/Container";
import { SocialLinks } from "@/constant/social";

const SocialSection = ({ className = "" }: { className?: string }) => (
  <FullScreenContainer id="social" className={`relative ${className}`}>
    <div className="space-y-4 text-center ">
      <div className="space-y-2">
        <Subtitle
          text="Media Sosial"
          className="font-bold uppercase text-primary"
        />
        <Paragraph text="Kunjungilah social media kami dibawah ini" />
      </div>

      <Card className="grid grid-cols-1 gap-4 px-4 py-8 text-center sm:gap-8 md:grid-cols-2 md:grid-rows-2">
        {SocialLinks.map((link, index) => (
          <Link
            key={index}
            href={link.href}
            target="_blank"
            className={link.className}
          >
            <link.icon className="h-8 w-8 sm:h-14 sm:w-14" />
            <span>{link.handler}</span>
          </Link>
        ))}
      </Card>

      <Footer />
    </div>
  </FullScreenContainer>
);
export default SocialSection;
