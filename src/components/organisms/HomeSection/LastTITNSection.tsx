import { Paragraph, Subtitle } from "@/components/atoms/Text";
import { FullScreenContainer } from "@/components/template/Container";

const LastTITNSection = ({ className = "" }: { className?: string }) => {
  return (
    <FullScreenContainer id="dokumentasi" className={className}>
      <div className="space-y-4 text-center">
        <div className="space-y-2">
          <Subtitle
            text="Dokumentasi TITN Pemuda Persis Sebelumnya"
            className="font-bold uppercase text-primary"
          />
          <Paragraph text="Video dokumentasi TITN VIII Pemuda Persis yang sebelumnya di Gn. Puntang pada tanggal 8 - 10 September 2017" />
        </div>
        <div className="relative overflow-auto rounded-lg">
          <iframe
            width={800}
            height={500}
            src="https://www.youtube.com/embed/Nv76HO2HxDg?si=iyQNLxaa-z2As2zK&amp;controls=0"
            title="YouTube video player"
            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
            referrerPolicy="strict-origin-when-cross-origin"
            className="aspect-video h-full w-full rounded-lg shadow-lg"
            allowFullScreen
          />
        </div>
      </div>
    </FullScreenContainer>
  );
};

export default LastTITNSection;
