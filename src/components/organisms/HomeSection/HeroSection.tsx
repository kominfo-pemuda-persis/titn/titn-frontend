import { Image } from "@/components/atoms/Image";
import { Paragraph, Subtitle, Title } from "@/components/atoms/Text";
import { HeroButton } from "@/components/molecules/HeroSection/HeroButton";
import { FullScreenContainer } from "@/components/template/Container";
import { sponsorLarge, sponsorMedium } from "@/constant/data/sponsor";

const HeroSection = ({ className = "" }: { className?: string }) => (
  <FullScreenContainer id="hero" className={className}>
    <div className="relative max-w-3xl space-y-12 text-center">
      <div className="flex items-end justify-center space-x-2">
        <Image
          src="/assets/logo.png"
          alt="TITN Logo"
          width={0}
          height={0}
          sizes="(max-width: 640px) 110px, (max-width: 768px) 110px, (max-width: 1024px) 110px, 110px"
          className="h-full w-full max-w-[75px] md:max-w-[110px]"
        />
        <div className="space-y-2 text-left">
          <Title
            text="Temu Ilmiah &"
            className="px-2 text-2xl font-extrabold uppercase text-black dark:text-white md:text-5xl"
          />
          <Title
            text="Taaruf Nasional"
            className="text-nowrap px-2 text-2xl font-extrabold uppercase text-black dark:text-white md:text-5xl"
          />
          <Title
            text="Pemuda Persis"
            className="w-fit bg-primary px-2 text-2xl font-extrabold uppercase text-white dark:text-black md:text-5xl"
          />
          <Title
            text="2024"
            className="px-2 text-2xl font-extrabold uppercase text-black dark:text-white md:text-5xl"
          />
        </div>
      </div>

      <div className="space-y-2">
        <Title
          text="Dakwah Berkelanjutan Pemuda Persatuan Islam"
          className="font-bold uppercase text-primary"
        />
        <Subtitle
          text="'Sustainable Leaders, Sustainable Futures'"
          className="font-medium italic"
        />
      </div>

      <HeroButton />

      <div className="!-mb-28 mt-auto space-y-2">
        <Paragraph
          text="Sponsored by"
          className="pb-4 text-center text-sm text-primary"
        />

        <div className="flex flex-wrap items-center justify-center gap-4">
          <Image
            src={sponsorLarge[0].logo}
            alt={sponsorLarge[0].name}
            width={0}
            height={0}
            sizes="(max-width: 640px) 90px, (max-width: 768px) 90px, (max-width: 1024px) 90px, 90px"
            className="h-full w-full max-w-[50px] md:max-w-[90px]"
          />
          <Image
            src={sponsorLarge[1].logo}
            alt={sponsorLarge[1].name}
            width={0}
            height={0}
            sizes="(max-width: 640px) 175px, (max-width: 768px) 175px, (max-width: 1024px) 175px, 175px"
            className="h-full w-full max-w-[100px] md:max-w-[175px]"
          />
          <Image
            src={sponsorLarge[2].logo}
            alt={sponsorLarge[2].name}
            width={0}
            height={0}
            sizes="(max-width: 640px) 175px, (max-width: 768px) 175px, (max-width: 1024px) 175px, 175px"
            className="h-full w-full max-w-[100px] md:max-w-[175px]"
          />
        </div>

        <div className="flex flex-wrap items-center justify-center gap-4">
          {sponsorMedium?.map((sponsor, index) => (
            <Image
              key={index}
              src={sponsor.logo}
              alt={sponsor.name}
              width={0}
              height={0}
              sizes="(max-width: 640px) 90px, (max-width: 768px) 90px, (max-width: 1024px) 90px, 90px"
              className="h-full w-full max-w-[50px] md:max-w-[90px]"
            />
          ))}
        </div>
      </div>
    </div>
  </FullScreenContainer>
);

export default HeroSection;
