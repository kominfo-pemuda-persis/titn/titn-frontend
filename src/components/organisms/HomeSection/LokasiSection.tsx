import { Paragraph, Subtitle } from "@/components/atoms/Text";
import { FullScreenContainer } from "@/components/template/Container";
import { GoogleMapLink } from "@/constant/social";

const LokasiSection = ({ className = "" }: { className?: string }) => (
  <FullScreenContainer id="lokasi" className={className}>
    <div className="space-y-4 text-center">
      <div className="space-y-2">
        <Subtitle
          text="Tempat & Waktu"
          className="font-bold uppercase text-primary"
        />
        <Paragraph text="Bumi Perkemahan Rancaupas, Rancabali, Kabupaten Bandung | 26 - 30 September 2024" />
      </div>

      <div className="w-[1000px] min-w-full max-w-[90vw]">
        <iframe
          title="Google map Ranca Upas"
          src={GoogleMapLink}
          width="1000"
          height="450"
          style={{
            border: 0,
          }}
          allowFullScreen
          loading="lazy"
          referrerPolicy="no-referrer-when-downgrade"
          className="w-full rounded-xl border border-secondary bg-dark shadow-xl"
        />
      </div>
    </div>
  </FullScreenContainer>
);

export default LokasiSection;
