import { Paragraph, Subtitle } from "@/components/atoms/Text";
import { HeroButton } from "@/components/molecules/HeroSection";
import { SwiperGallery } from "@/components/organisms/Gallery/Gallery";
import { FullScreenContainer } from "@/components/template/Container";

const AboutSection = ({ className = "" }: { className?: string }) => {
  const slideImages = [
    {
      src: "/assets/gallery/titn_1.webp",
      alt: "titn-gallery-1",
    },
    {
      src: "/assets/gallery/titn_2.webp",
      alt: "titn-gallery-2",
    },
    {
      src: "/assets/gallery/titn_3.webp",
      alt: "titn-gallery-3",
    },
    {
      src: "/assets/gallery/titn_4.webp",
      alt: "titn-gallery-4",
    },
    {
      src: "/assets/gallery/titn_5.webp",
      alt: "titn-gallery-5",
    },
    {
      src: "/assets/gallery/titn_6.webp",
      alt: "titn-gallery-6",
    },
  ];

  return (
    <FullScreenContainer id="tentang" className={className}>
      <div className="grid grid-rows-1 items-center space-x-0 space-y-4 md:grid-cols-2 md:flex-row md:space-x-24 md:space-y-0">
        <SwiperGallery slideImages={slideImages} />

        <div className="space-y-4 text-left">
          <Subtitle
            text="Tentang TITN"
            className="font-bold uppercase text-primary"
          />
          <Paragraph
            className="text-justify"
            text={
              <>
                <b>TITN (Temu Ilmiah dan Ta&apos;aruf Nasional)</b> adalah
                kegiatan yang diselenggarakan Pimpinan Pusat Pemuda Persis yang
                berisi seminar, workshop, perlombaan, serta silaturahmi dan
                tukar gagasan antar anggota Pemuda se-Indonesia.
              </>
            }
          />
          <HeroButton />
        </div>
      </div>
    </FullScreenContainer>
  );
};

export default AboutSection;
