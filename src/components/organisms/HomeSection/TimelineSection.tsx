import { Subtitle } from "@/components/atoms/Text";
import Timeline from "@/components/atoms/Timeline/Timeline";
import { FullScreenContainer } from "@/components/template/Container";
import { TIMELINE_ITEMS } from "@/constant/data/timeline";

const TimelineSection = () => {
  return (
    <FullScreenContainer id="timeline">
      <div className="space-y-4 text-center">
        <Subtitle
          text="Timeline TITN"
          className="mt-6 font-bold uppercase text-primary"
        />
        <div className="relative w-full max-w-[90vw]">
          <Timeline
            mode="VERTICAL_ALTERNATING"
            items={TIMELINE_ITEMS}
            disableNavOnKey
            hideControls
            scrollable
            disableToolbar
          />
        </div>
      </div>
    </FullScreenContainer>
  );
};

export default TimelineSection;
