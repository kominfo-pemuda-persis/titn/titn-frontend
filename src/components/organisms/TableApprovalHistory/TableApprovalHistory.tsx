"use client";

import { useCallback, useEffect, useMemo, useState } from "react";
import { useCookies } from "next-client-cookies";
import dynamic from "next/dynamic";
import { getKeyValue, useDisclosure } from "@nextui-org/react";
import { useQuery } from "@tanstack/react-query";
import { useAtom } from "jotai";
import { BsEye } from "react-icons/bs";

import { Button } from "@/components/atoms/Button";
import Toast from "@/components/atoms/Toast";
import TableTemplate from "@/components/molecules/Table/Template";
import { COLUMNS_APPROVAL } from "@/constant/data/recap";
import { listRecaps } from "@/services/query/recaps";
import { tableStates } from "@/states/table";
import { Recap, RecapStatus, RowApproval } from "@/types";
import { Sort } from "@/types/utils";
import { sleep } from "@/utils/debounce";

const ModalShowImage = dynamic(
  () => import("@/components/organisms/Modal/ModalShowImage"),
  {
    ssr: false,
  }
);

const TableApprovalHistory = () => {
  const [{ searchValue, page, rowsPerPage, sortDescriptor }] =
    useAtom(tableStates);

  const cookie = useCookies();
  const session = cookie?.get("session");

  const {
    onOpen: onOpenModalImage,
    isOpen: isOpenModalImage,
    onClose: onCloseModalImage,
  } = useDisclosure();

  const [modalShowImage, setModalShowImage] = useState<{
    url: string;
  }>({
    url: "",
  });
  const { data, isFetching, error } = useQuery({
    queryKey: [
      searchValue,
      rowsPerPage,
      page,
      sortDescriptor.column,
      sortDescriptor.direction,
    ],
    queryFn: async ({ signal }) => {
      await sleep(750);

      return await listRecaps({
        keyword: searchValue,
        pageSize: rowsPerPage,
        pageNo: page > 0 ? page - 1 : 0,
        signal,
        session,
        status: RecapStatus.APPROVED,
        sortBy: sortDescriptor.column as string,
        sortOrder:
          sortDescriptor.direction === "ascending" ? Sort.ASC : Sort.DESC,
      });
    },
  });

  const response = useMemo(
    () => (isFetching ? [] : data?.data),
    [data?.data, isFetching]
  );

  const filteredItems: RowApproval[] = useMemo(() => {
    let mappedData = response?.content?.map((item: Recap, index: number) => ({
      key: item.recap_id,
      npa: item.npa || "-",
      fullName: item.full_name || "-",
      pcName: item.pc_name || "-",
      pdName: item.pd_name || "-",
      pwName: item.pw_name || "-",
      createdAt: new Date(item.created_at)
        .toLocaleDateString("id-ID", {
          year: "numeric",
          month: "long",
          day: "numeric",
          hour: "numeric",
          minute: "numeric",
          timeZoneName: "short",
        })
        .replace("pukul", ""),
      updatedAt: new Date(item.updated_at)
        .toLocaleDateString("id-ID", {
          year: "numeric",
          month: "long",
          day: "numeric",
          hour: "numeric",
          minute: "numeric",
          timeZoneName: "short",
        })
        .replace("pukul", ""),
      updatedBy: item.updated_by || "-",
      buktiTransfer: item.bukti_transfer || "-",
      no: page === 1 ? index + 1 : (page - 1) * rowsPerPage + index + 1,
    }));

    return mappedData;
  }, [page, response?.content, rowsPerPage]);

  const showModalImage = (rekap: RowApproval, columnKey: string): void => {
    setModalShowImage({
      url: getKeyValue(rekap, columnKey),
    });
    onOpenModalImage();
  };

  const renderCell = useCallback((rekap: RowApproval, columnKey: string) => {
    switch (columnKey) {
      case "buktiTransfer":
        return (
          <Button
            isIconOnly
            onClick={(e) => {
              e.stopPropagation();
              showModalImage(rekap, columnKey);
            }}
            variant="light"
            size="sm"
            color="warning"
            title="Lihat"
          >
            <BsEye size={20} />
          </Button>
        );

      default:
        return getKeyValue(rekap, columnKey);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    if (error) {
      Toast({
        message: "Gagal mendapatkan data",
        type: "error",
      });
    }
  }, [error]);

  return (
    <>
      <TableTemplate
        response={response}
        isFetching={isFetching}
        emptyContent="Rekap tidak ditemukan"
        renderCell={renderCell}
        columns={COLUMNS_APPROVAL}
        items={filteredItems}
      />

      <ModalShowImage
        onClose={onCloseModalImage}
        url={modalShowImage.url}
        isOpen={isOpenModalImage}
        handleOpen={onOpenModalImage}
      />
    </>
  );
};

export default TableApprovalHistory;
