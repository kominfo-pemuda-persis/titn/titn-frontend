import React from "react";
import { Meta, StoryObj } from "@storybook/react";
import { Provider } from "jotai";

import { Sidebar } from "./Sidebar";

export default {
  title: "Organisms/Sidebar",
  component: Sidebar,
  parameters: {
    // Optional parameter to center the component in the Canvas. More info: https://storybook.js.org/docs/configure/story-layout
    layout: "centered",
  },
} as Meta<typeof Sidebar>;

type Story = StoryObj<typeof Sidebar>;

export const DefaultSidebarMenu: Story = {
  args: {},
};
