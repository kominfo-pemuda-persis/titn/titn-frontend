"use client";

import Link from "next/link";
import { usePathname } from "next/navigation";
import { useAtom, useAtomValue } from "jotai";
import { FaChevronLeft, FaChevronRight } from "react-icons/fa6";

import { Button } from "@/components/atoms/Button";
import { Image } from "@/components/atoms/Image";
import { SidebarItem } from "@/components/molecules/SidebarItem";
import { VERSION } from "@/constant";
import { NAV_DASHBOARD_ITEMS, ROUTE_HOME } from "@/constant/routes";
import { collapsed as collapsedState } from "@/states/layouts/sidebar";
import { permission } from "@/states/user";
import { SidebarStyle } from "./sidebar.styles";

export const Sidebar = () => {
  const pathname = usePathname();
  const { routeChecker } = useAtomValue(permission);
  const [collapsed, setCollapsed] = useAtom(collapsedState);

  return (
    <aside className="sticky top-0 z-[20] hidden h-screen md:block">
      <div
        className={SidebarStyle({
          collapsed: collapsed,
        })}
      >
        <div
          className={SidebarStyle.Header({
            collapsed: collapsed,
          })}
        >
          <Link href={ROUTE_HOME}>
            <div className="flex flex-1 items-center gap-2">
              <Image src="/assets/logo.png" alt="LOGO TITN" width={20} />
              {collapsed && (
                <p className="hidden text-2xl font-bold text-primary sm:block">
                  TITN
                </p>
              )}
            </div>
          </Link>
          <Button
            isIconOnly
            className="rounded-full bg-transparent"
            onClick={() => setCollapsed(!collapsed)}
          >
            {collapsed ? <FaChevronLeft /> : <FaChevronRight />}
          </Button>
        </div>
        <div className="flex h-full flex-col justify-between">
          <div className="mt-9 flex flex-col gap-2 px-2">
            {NAV_DASHBOARD_ITEMS.map(
              (item, index) =>
                routeChecker(item.href) && (
                  <SidebarItem
                    key={index}
                    title={item.title}
                    icon={item.icon}
                    isActive={pathname === item.href}
                    href={item.href}
                    collapsed={collapsed}
                  />
                )
            )}
          </div>
        </div>

        <div>
          <div className="flex h-12 items-center justify-center">
            <p className="text-xs text-gray-400">v{VERSION}</p>
          </div>
        </div>
      </div>
    </aside>
  );
};
