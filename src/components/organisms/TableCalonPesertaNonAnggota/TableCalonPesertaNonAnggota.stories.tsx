import { Meta } from "@storybook/react";

import TableCalonPesertaNonAnggota from "@/components/organisms/TableCalonPesertaNonAnggota/TableCalonPesertaNonAnggota";
import Providers from "@/providers";

export default {
  title: "Organisms/TableCalonPesertaNonAnggota",
  component: TableCalonPesertaNonAnggota,
  argTypes: {},
} as Meta<typeof TableCalonPesertaNonAnggota>;

export const DefaultPage = () => (
  <html lang="en">
    <body>
      <Providers>
        <TableCalonPesertaNonAnggota />
      </Providers>
    </body>
  </html>
);
