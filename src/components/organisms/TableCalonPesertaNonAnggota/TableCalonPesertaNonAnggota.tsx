"use client";

import { useCallback, useEffect, useMemo, useState } from "react";
import { useCookies } from "next-client-cookies";
import dynamic from "next/dynamic";
import { useRouter } from "next/navigation";
import { getKeyValue, useDisclosure } from "@nextui-org/react";
import { useQuery } from "@tanstack/react-query";
import { useAtom } from "jotai";
import { BsTrash } from "react-icons/bs";

import { Button } from "@/components/atoms/Button";
import { Chip } from "@/components/atoms/Chip";
import Toast from "@/components/atoms/Toast";
import TableTemplate from "@/components/molecules/Table/Template";
import { COLUMNS_NON_ANGGOTA, RowPeserta } from "@/constant/data/peserta";
import { deleteParticipant } from "@/services/mutation/participant";
import { listParticipant } from "@/services/query/participant";
import { tableStates } from "@/states/table";
import { Participant, ParticipantStatus } from "@/types/participant";
import { Sort } from "@/types/utils";
import { getColorBasedOnStatus } from "@/utils";
import { sleep } from "@/utils/debounce";

const ModalConfirmation = dynamic(
  () => import("@/components/organisms/Modal/ModalConfirmation"),
  {
    ssr: false,
  }
);

const TableCalonPesertaNonAnggota = () => {
  const router = useRouter();
  const { onOpen, isOpen, onClose } = useDisclosure();
  const [{ searchValue, page, rowsPerPage, sortDescriptor }] =
    useAtom(tableStates);

  const cookie = useCookies();
  const session = cookie?.get("session");

  const [modalValue, setModalValue] = useState<{
    title: string;
    message: string;
    onConfirm: () => void;
  }>({
    title: "",
    message: "",
    onConfirm: () => {},
  });
  const [isLoading, setIsLoading] = useState<boolean>(false);

  const { data, isFetching, refetch, error } = useQuery({
    queryKey: [
      searchValue,
      rowsPerPage,
      page,
      sortDescriptor.column,
      sortDescriptor.direction,
    ],
    queryFn: async ({ signal }) => {
      await sleep(750);

      return await listParticipant({
        keyword: searchValue,
        pageSize: rowsPerPage,
        pageNo: page > 0 ? page - 1 : 0,
        signal,
        isAnggota: false,
        status: ParticipantStatus.SUBMITTED,
        session,
        sortBy: sortDescriptor.column as string,
        sortOrder:
          sortDescriptor.direction === "ascending" ? Sort.ASC : Sort.DESC,
      });
    },
  });

  const response = useMemo(
    () => (isFetching ? [] : data?.data),
    [data?.data, isFetching]
  );

  const filteredItems: RowPeserta[] = useMemo(() => {
    let mappedData = response?.content?.map(
      (item: Participant, index: number) => {
        const regDate = item.created_at
          ? Intl.DateTimeFormat("id-ID").format(new Date(item.created_at))
          : "-";
        return {
          key: item.participant_id,
          fullName: item.full_name || "-",
          email: item.email || "-",
          phone: item.phone || "-",
          createdAt: new Date(item.created_at)
            .toLocaleDateString("id-ID", {
              year: "numeric",
              month: "long",
              day: "numeric",
              hour: "numeric",
              minute: "numeric",
              timeZoneName: "short",
            })
            .replace("pukul", ""),
          pcName: item.pc_name || "-",
          pdName: item.pd_name || "-",
          pwName: item.pw_name || "-",
          status: item.status || "-",
          clothesSize: item.clothes_size || "-",
          no: page === 1 ? index + 1 : (page - 1) * rowsPerPage + index + 1,
        };
      }
    );

    return mappedData;
  }, [page, response?.content, rowsPerPage]);

  const handleDelete = async (id: string) => {
    setModalValue({
      title: "Hapus Calon Peserta",
      message: "Apakah Anda yakin ingin menghapus calon peserta ini?",
      onConfirm: async () => {
        setIsLoading(true);
        const response = await deleteParticipant(id);
        if (response.success) {
          Toast({
            message: "Calon peserta berhasil dihapus",
            type: "success",
          });
          refetch();
        } else {
          Toast({
            message: response.message as string,
            type: "error",
          });
          console.log(response.message);
        }
        setIsLoading(false);
      },
    });
    onOpen();
  };

  const renderCell = useCallback((peserta: RowPeserta, columnKey: string) => {
    switch (columnKey) {
      case "status":
        return (
          <Chip
            color={`${getColorBasedOnStatus(getKeyValue(peserta, columnKey))}`}
            size="sm"
            variant="flat"
          >
            {getKeyValue(peserta, columnKey)}
          </Chip>
        );

      case "actions":
        return (
          <div className="relative flex items-center justify-center gap-2">
            <Button
              isIconOnly
              size="sm"
              variant="light"
              color="secondary"
              title="Delete"
              onClick={() => handleDelete(peserta.key)}
            >
              <BsTrash size={20} />
            </Button>
          </div>
        );

      default:
        return getKeyValue(peserta, columnKey);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const getDownloadableData = async () => {
    try {
      const participantsForDownload = await listParticipant({
        keyword: searchValue,
        pageSize: 1500,
        pageNo: 0,
        isAnggota: false,
        status: ParticipantStatus.SUBMITTED,
        session,
      });

      if (participantsForDownload.status === "OK") {
        return participantsForDownload.data.content.map(
          (item: Participant, index: number) => ({
            No: index + 1,
            Nama: item.full_name?.replace(/,/g, "") || "-",
            Email: item.email || "-",
            Phone: item.phone || "-",
            PC: item.pc_name,
            PD: item.pd_name,
            PW: item.pw_name,
            Status: item.status,
            CreatedAt: new Date(item.created_at)
              .toLocaleDateString("id-ID", {
                year: "numeric",
                month: "long",
                day: "numeric",
                hour: "numeric",
                minute: "numeric",
                timeZoneName: "short",
              })
              .replace("pukul", ""),
            "Ukuran Baju": item.clothes_size,
          })
        );
      } else {
        throw new Error(participantsForDownload.message);
      }
    } catch (error: any) {
      console.log(error);

      Toast({
        message: error.message,
        type: "error",
      });
    }
  };

  useEffect(() => {
    if (error) {
      Toast({
        message: "Gagal mendapatkan data",
        type: "error",
      });
    }
  }, [error]);

  const CALON_PESERTA_COLUMNS = COLUMNS_NON_ANGGOTA.filter(
    (col) => col.uid !== "updatedAt"
  );

  return (
    <>
      <TableTemplate
        response={response}
        isFetching={isFetching}
        emptyContent="Calon peserta non-anggota tidak ditemukan"
        renderCell={renderCell}
        columns={CALON_PESERTA_COLUMNS}
        items={filteredItems}
        onRowAction={(key) => router.push(`/calon-peserta/${key}`)}
        downloadOptions={{
          fileName: "DATA_CALON_PESERTA_NON_ANGGOTA",
          isEnabled: true,
          getData: getDownloadableData,
        }}
      />

      <ModalConfirmation
        isOpen={isOpen}
        onClose={onClose}
        title={modalValue.title}
        message={modalValue.message}
        handleOpen={modalValue.onConfirm}
        isDisabled={isLoading}
        isLoading={isLoading}
      />
    </>
  );
};

export default TableCalonPesertaNonAnggota;
