"use client";

import {
  FormEvent,
  Key,
  Suspense,
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState,
} from "react";
import { useCookies } from "next-client-cookies";
import { Dropzone, ExtFile, FileMosaic } from "@files-ui/react";
import { getKeyValue } from "@nextui-org/react";
import { useMutation, useQuery } from "@tanstack/react-query";
import { useAtom } from "jotai";
import { RESET } from "jotai/utils";
import { BsTrash3Fill } from "react-icons/bs";
import { z } from "zod";

import { Button } from "@/components/atoms/Button";
import { Chip } from "@/components/atoms/Chip";
import { Input } from "@/components/atoms/Input";
import { Paragraph, SubsectionTitle } from "@/components/atoms/Text";
import ImageUpload from "@/components/molecules/ImageUpload/ImageUpload";
import TableTemplate from "@/components/molecules/Table/Template";
import ModalConfirmation from "@/components/organisms/Modal/ModalConfirmation";
import { RowPeserta } from "@/constant/data/peserta";
import { postRecaps } from "@/services/mutation/recaps";
import { listParticipant } from "@/services/query/participant";
import { getUserByNpa } from "@/services/query/user";
import { tableStates } from "@/states/table";
import {
  BaseResponse,
  DataRecap,
  DataRecapSchema,
  Participant,
  ParticipantStatus,
} from "@/types";
import { getColorBasedOnEventType } from "@/utils";
import { sleep } from "@/utils/debounce";

const FormRekapWrapper = () => {
  const cookie = useCookies();
  const session = cookie?.get("session");

  const [isOpenModal, setOpenModal] = useState<boolean>(false);
  const [error, setError] = useState<string>("");
  const [namaPeserta, setNamaPeserta] = useState<string>("");
  const [selectedParticipants, setSelectedParticipants] = useState<
    Participant[]
  >([] as Participant[]);
  const [imageSrc, setImageSrc] = useState<string>("");
  const [fileImages, setFileImages] = useState<ExtFile[]>([]);
  const [npa, setNpa] = useState<string>("");
  const [findAnggotaError, setFindAnggotaError] = useState<string>("");

  const formRef = useRef<HTMLFormElement>(null);
  const [{ selectedKeys, searchValue, page, rowsPerPage }, setTableState] =
    useAtom(tableStates);

  const { data, error: fetchError } = useQuery({
    queryKey: ["anggota", npa],
    queryFn: async ({ signal }) => {
      await sleep(500);

      const result = await getUserByNpa(npa, { signal });

      if (!result.data) {
        setFindAnggotaError("Gagal Mendapatkan Anggota dengan NPA: " + npa);
      } else {
        setFindAnggotaError("");
      }

      return result.data;
    },
    retry: 0,
    enabled: npa.length === 7,
  });

  const { data: availableParticipant, isFetching: loadingCalonPeserta } =
    useQuery({
      queryKey: ["participant", namaPeserta, searchValue, rowsPerPage],
      queryFn: async ({ signal }) => {
        await sleep(500);

        return await listParticipant({
          keyword: searchValue,
          pageSize: rowsPerPage,
          pageNo: page > 0 ? page - 1 : 0,
          status: ParticipantStatus.SUBMITTED,
          isRecap: false,
          session,
          signal,
        });
      },
      retry: 0,
    });

  const { mutate, isPending } = useMutation({
    mutationFn: postRecaps,
    onSuccess: (resp: BaseResponse) => {
      if (resp.success) {
        setOpenModal(true);
        setError("");
        setTableState(RESET);
      } else {
        setOpenModal(true);
        setError(resp.message ?? "Gagal Mengirimkan Bukti Transfer");
      }
    },
  });

  const filterAlreadySelectedParticipant = useCallback(
    (participant: Participant[]) =>
      participant?.filter(
        (available_p) =>
          !selectedParticipants
            .map((selected_p) => selected_p.participant_id)
            .includes(available_p.participant_id)
      ),
    [selectedParticipants]
  );

  const filteredParticipants = useMemo(
    () =>
      filterAlreadySelectedParticipant(
        availableParticipant?.data?.content as Participant[]
      )?.map((participant) => ({
        key: participant.participant_id,
        id: participant.participant_id,
        fullName: participant.full_name || "-",
        clothesSize: participant.clothes_size || "-",
        events:
          participant.event_details?.map((event) => ({
            name: event.name,
            type: event.type,
          })) || "-",
      })),
    [availableParticipant?.data?.content, filterAlreadySelectedParticipant]
  );

  const anggotaData = useMemo(
    () => ({
      participant_id: (data?.id as string) ?? "",
      phone: data?.noTelpon ?? "",
      full_name: data?.nama ?? "",
      email: data?.email ?? "",
      pc_name: data?.pc.namaPc ?? "",
      pc_code: data?.pc.kdPc ?? "",
      pd_name: data?.pd.namaPd ?? "",
      pd_code: data?.pd.kdPd ?? "",
      pw_name: data?.pw.namaWilayah ?? "",
      pw_code: data?.pw.kdPw ?? "",
      photo: data?.foto ?? "",
    }),
    [data]
  );

  const handleChangeFileImages = (newFiles: ExtFile[]) => {
    setFileImages(newFiles);
  };

  const filteredKeys: string[] = useMemo(
    () =>
      selectedKeys === "all"
        ? availableParticipant?.data?.content.map(
            (participant: Participant) => participant.participant_id
          )
        : Array.from(selectedKeys),
    [availableParticipant?.data?.content, selectedKeys]
  );

  const renderCell = useCallback((peserta: RowPeserta, columnKey: string) => {
    switch (columnKey) {
      case "events":
        return (
          <div className="flex flex-wrap gap-1">
            {peserta?.events?.map((event, index) => (
              <Chip
                color={`${getColorBasedOnEventType(getKeyValue(event.type, columnKey))}`}
                key={index}
                size="sm"
                variant="dot"
              >
                {getKeyValue(event.name, columnKey)}
              </Chip>
            ))}
          </div>
        );

      default:
        return getKeyValue(peserta, columnKey);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const onSubmit = async (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault();

    try {
      if (event.currentTarget.checkValidity()) {
        const formData = new FormData(event.currentTarget);
        const npa = formData.get("npa") as string;
        const full_name = anggotaData?.full_name;
        const email = anggotaData?.email;
        const phone = anggotaData?.phone;
        // const nums = formData.get("jumlah") as string;
        const nums = filteredKeys.length.toString();
        const bukti_transfer = formData.get("buktiTransferUrl") as string;
        // const participants = selectedParticipants.map(
        //   (profile) => profile.participant_id
        // );
        const participants = filteredKeys;

        try {
          const payload = DataRecapSchema.parse({
            npa,
            full_name,
            email,
            phone,
            pc_name: anggotaData?.pc_name ?? "",
            pd_name: anggotaData?.pd_name ?? "",
            pw_name: anggotaData?.pw_name ?? "",
            pc_code: anggotaData?.pc_code ?? "",
            pd_code: anggotaData?.pd_code ?? "",
            pw_code: anggotaData?.pw_code ?? "",
            bukti_transfer,
            participants,
            nums,
          });

          mutate(payload as DataRecap);
        } catch (e) {
          if (e instanceof z.ZodError) {
            const messages = e.issues.map((err) => {
              return err.message + "\n";
            });
            setError(messages.join(""));
            setOpenModal(true);
          } else {
            setError(e as string);
            setOpenModal(true);
          }
        }
      }
    } catch (error) {
      console.log(error);
    }
  };

  const handleSetSelectedParticipants = (participant: Participant) =>
    setSelectedParticipants((prev) =>
      prev?.filter((p) => p.participant_id !== participant.participant_id)
    );

  const handleCloseModal = () => {
    setError("");
    setOpenModal(false);
    if (!error) {
      formRef.current?.reset();
      setSelectedParticipants([]);
      setNpa("");
      setImageSrc("");
      setFileImages([]);
    }
  };

  const handleSelectionChange = async (item: Key | null) => {
    if (
      selectedParticipants
        .map((participant) => participant.participant_id)
        .includes(item as string)
    ) {
      setSelectedParticipants((prev) =>
        prev?.filter(
          (participant) => participant.participant_id !== (item as string)
        )
      );
    } else {
      setSelectedParticipants((prev) => [
        ...prev,
        ...(availableParticipant?.data?.content?.filter(
          (participant: Participant) =>
            participant.participant_id === (item as string)
        ) as Participant[]),
      ]);
    }
    setNamaPeserta("");
  };

  const handleSetNamaPeserta = (value: string) => setNamaPeserta(value);

  const response = useMemo(
    () => (loadingCalonPeserta ? [] : availableParticipant?.data),
    [availableParticipant?.data, loadingCalonPeserta]
  );

  useEffect(() => {
    if (fetchError) {
      setFindAnggotaError("Gagal Mendapatkan Anggota dengan NPA: " + npa);
      console.log({ fetchError });
    }
  }, [fetchError, npa]);

  return (
    <>
      <form ref={formRef} id="rekap-form" onSubmit={onSubmit} className="px-5">
        {findAnggotaError ? (
          <div className="text-center text-red-500">{findAnggotaError}</div>
        ) : null}
        <div className="grid gap-4">
          <Input
            labelPlacement="outside"
            id="npa"
            placeholder="Masukan NPA Anda"
            name="npa"
            label="NPA Admin Rekap"
            value={npa}
            onChange={(e) => setNpa(e.target.value)}
            isRequired
            isInvalid={false}
            errorMessage="NPA tidak boleh kosong"
            isDisabled={isPending}
            onClear={() => setNpa("")}
            isClearable
          />

          <Input
            labelPlacement="outside"
            id="nama"
            label="Nama Admin Rekap"
            name="nama"
            placeholder="Mochamad Rafli"
            value={anggotaData?.full_name}
            isRequired
            isReadOnly
            isDisabled
          />

          <div className="flex flex-col gap-4 md:flex-row md:gap-2">
            <Input
              labelPlacement="outside"
              id="phone"
              placeholder="+6289876543210"
              name="phone"
              type="phone"
              label="Nomor Telepon"
              value={anggotaData?.phone}
              isRequired
              isReadOnly
              isDisabled
            />

            <Input
              labelPlacement="outside"
              id="email"
              name="email"
              placeholder="example@example.com"
              type="email"
              value={anggotaData?.email}
              label="Email"
              isRequired
              isReadOnly
              isDisabled
            />
          </div>

          <div className="flex flex-col gap-4 md:flex-row md:gap-2">
            <Input
              labelPlacement="outside"
              id="pw"
              name="pw"
              placeholder="Masukan NPA Admin Rekap"
              value={anggotaData?.pw_name}
              label="Pimpinan Wilayah"
              isRequired
              isReadOnly
              isDisabled
            />

            <Input
              labelPlacement="outside"
              id="pd"
              name="pd"
              placeholder="Masukan NPA Admin Rekap"
              value={anggotaData?.pd_name}
              label="Pimpinan Daerah"
              isReadOnly
              isDisabled
              isRequired
            />

            <Input
              labelPlacement="outside"
              id="pc"
              name="pc"
              placeholder="Masukan NPA Admin Rekap"
              value={anggotaData?.pc_name}
              label="Pimpinan Cabang"
              isReadOnly
              isDisabled
              isRequired
            />
          </div>

          {/* <Input
            labelPlacement="outside"
            id="jumlah"
            placeholder="100"
            name="jumlah"
            type="number"
            label="Jumlah Yang Dikonfirmasi"
            value={filteredKeys?.length.toString()}
            min={0}
            isRequired
            isDisabled
          /> */}

          {/* <Autocomplete
            labelPlacement="outside"
            label="Calon Peserta"
            name="participants"
            id="participants"
            placeholder="Cari calon peserta yang akan dikonfirmasi"
            isRequired
            isDisabled={isPending}
            inputValue={namaPeserta}
            onInputChange={handleSetNamaPeserta}
            onSelectionChange={handleSelectionChange}
            isLoading={loadingCalonPeserta}
          >
            {loadingCalonPeserta ? (
              <AutocompleteItem key="loading" isReadOnly textValue="Loading...">
                <div className="flex w-full justify-center">
                  <Spinner />
                </div>
              </AutocompleteItem>
            ) : (
              filteredParticipants?.map((profile) => (
                <AutocompleteItem
                  key={profile.participant_id}
                  textValue={profile.full_name}
                >
                  <div className="flex w-full items-center justify-between space-x-2">
                    <div className="flex items-center gap-2">
                      <SubsectionTitle
                        text={profile.full_name}
                        className=" text-primary"
                      />
                      |
                      <Paragraph text="Uk. " />
                      <div className="flex flex-wrap gap-2">
                        {profile.event && (
                          <Chip
                            className="capitalize"
                            color="primary"
                            size="sm"
                            variant="flat"
                          >
                            {profile.event.name}
                          </Chip>
                        )}
                        {profile.clothes_size && (
                          <Chip
                            className="capitalize"
                            color="secondary"
                            size="sm"
                            variant="flat"
                          >
                            {profile.clothes_size}
                          </Chip>
                        )}
                      </div>
                    </div>
                    <div className="ml-auto text-sm text-gray-500">
                      {profile.participant_id}
                    </div>
                  </div>
                </AutocompleteItem>
              ))
            )}
          </Autocomplete> */}

          <div>
            <div className="flex justify-between space-x-2">
              <Paragraph
                text={
                  <>
                    Pilih Calon Peserta{" "}
                    <Chip variant="solid" color="primary">
                      {filteredKeys?.length.toString()}
                    </Chip>
                  </>
                }
                className="font-sm mb-2 text-sm"
              />
            </div>
            <Suspense>
              <TableTemplate
                baseClassName="w-full h-full"
                selectionMode="multiple"
                response={response}
                isFetching={loadingCalonPeserta}
                emptyContent="Calon peserta tidak ditemukan"
                renderCell={renderCell}
                columns={[
                  {
                    name: "ID",
                    uid: "id",
                    sortable: false,
                  },
                  { name: "NAMA", uid: "fullName", sortable: false },
                  {
                    name: "EVENTS",
                    uid: "events",
                    sortable: false,
                  },
                  {
                    name: "UK. BAJU",
                    uid: "clothesSize",
                    sortable: true,
                  },
                ]}
                items={filteredParticipants}
              />
            </Suspense>
          </div>

          {selectedParticipants.length > 0 && (
            <div className="space-y-2">
              <SubsectionTitle
                text="Peserta yang akan dikonfirmasi"
                className="font-medium text-secondary"
              />
              <div className="flex max-h-[50vh] flex-col space-y-1 overflow-auto rounded-xl border px-2 py-2 dark:border-gray-700">
                {selectedParticipants.map((profile) => (
                  <div
                    className="flex justify-between space-x-2 rounded-md border border-gray-200 p-2 dark:border-gray-700"
                    key={profile.participant_id}
                  >
                    <div className="flex w-full items-center justify-between space-x-2">
                      <div className="flex items-center gap-2">
                        <SubsectionTitle
                          text={profile.full_name}
                          className=" text-primary"
                        />
                        |
                        <Paragraph text="Uk. " />
                        <div className="flex flex-wrap gap-2">
                          {profile.event && (
                            <Chip
                              className="capitalize"
                              color="primary"
                              size="sm"
                              variant="flat"
                            >
                              {profile.event.name}
                            </Chip>
                          )}
                          {profile.clothes_size && (
                            <Chip
                              className="capitalize"
                              color="secondary"
                              size="sm"
                              variant="flat"
                            >
                              {profile.clothes_size}
                            </Chip>
                          )}
                        </div>
                      </div>
                      <div className="ml-auto text-sm text-gray-500">
                        {profile.participant_id}
                      </div>
                    </div>
                    <div
                      className="flex w-10 cursor-pointer items-center justify-center"
                      onClick={() => handleSetSelectedParticipants(profile)}
                    >
                      <BsTrash3Fill className="fill-danger-500" size={20} />
                    </div>
                  </div>
                ))}
              </div>
            </div>
          )}

          <div className="space-y-1">
            <SubsectionTitle
              text="Bukti Transfer"
              className="font-medium text-secondary"
            />
            <Paragraph
              text="Silahkan upload bukti transfer anda"
              className="!text-sm"
            />
            <ImageUpload
              name="buktiTransfer"
              category="rekap"
              value={imageSrc}
              disabled={isPending}
              onChange={(value) => setImageSrc(value)}
              placeholder={
                <Dropzone
                  accept="image/*"
                  label="Upload bukti pembayaran"
                  onChange={handleChangeFileImages}
                  maxFiles={1}
                  maxFileSize={2 * 1024 * 1024}
                  value={fileImages}
                >
                  {fileImages.map((file) => (
                    <FileMosaic key={file.id} {...file} alwaysActive preview />
                  ))}
                </Dropzone>
              }
              classNames={{
                image: "rounded-none max-h-[300px]",
                wrapper: "mx-auto max-w-xl rounded-md",
              }}
            />
          </div>

          <Button
            type="submit"
            className="mt-2 w-full text-white"
            isLoading={isPending}
            color="primary"
          >
            Kirim
          </Button>
        </div>
      </form>

      <ModalConfirmation
        size="lg"
        isOpen={isOpenModal}
        onClose={handleCloseModal}
        isDisabled={isPending}
        isLoading={isPending}
        message={
          error ? (
            <>
              <SubsectionTitle text="Error" className="text-xl text-red-500" />
              <ul className="list-inside list-disc">
                {error
                  .split("\n")
                  .filter(Boolean)
                  .map((e) => (
                    <li key={e}>{e}</li>
                  ))}
              </ul>
            </>
          ) : (
            <div className="text-green-500">Pengiriman Rekap Berhasil</div>
          )
        }
        footer={
          <Button
            color={error ? "danger" : "success"}
            size="sm"
            onPress={handleCloseModal}
          >
            Ok
          </Button>
        }
      />
    </>
  );
};

export default FormRekapWrapper;
