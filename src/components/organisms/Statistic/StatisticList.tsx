"use client";

import { Fragment, useEffect, useMemo, useState } from "react";
import { CardHeader } from "@nextui-org/react";
import { useQuery } from "@tanstack/react-query";
import { useAtomValue } from "jotai/react";
import { FaPeopleGroup } from "react-icons/fa6";

import { Card, CardBody } from "@/components/atoms/Card/Card";
import { Divider } from "@/components/atoms/Divider";
import { Spinner } from "@/components/atoms/Spinner";
import { SectionTitle, SubsectionTitle } from "@/components/atoms/Text";
import {
  Autocomplete,
  AutocompleteItem,
} from "@/components/molecules/Autocomplete";
import { StatisticCard } from "@/components/organisms/Statistic/StatisticCard";
import { getStatistic } from "@/services/query/dashboard";
import { getPCByPD } from "@/services/query/pc";
import { getPDByPW } from "@/services/query/pd";
import { getPW } from "@/services/query/pw";
import { permission } from "@/states/user";
import { ResponsePc, ResponsePd, ResponsePw } from "@/types";
import { StatisticResponse } from "@/types/dashboard";

const StatisticList = () => {
  const { currentRole } = useAtomValue(permission);

  const [pcValue, setPcValue] = useState<string | null>("");
  const [pdValue, setPdValue] = useState<string | null>("");
  const [pwValue, setPwValue] = useState<string | null>("");
  const [pws, setPws] = useState<ResponsePw[]>([] as ResponsePw[]);
  const [pds, setPds] = useState<ResponsePd[]>([] as ResponsePd[]);
  const [pcs, setPcs] = useState<ResponsePc[]>([] as ResponsePc[]);

  const isAllowed = useMemo(
    () =>
      currentRole?.isAdminPC ||
      currentRole?.isAdminPD ||
      currentRole?.isAdminPW ||
      currentRole?.isAdminPP ||
      currentRole?.isTasykilPP ||
      currentRole?.isSuperAdmin,
    [
      currentRole?.isAdminPC,
      currentRole?.isAdminPD,
      currentRole?.isAdminPW,
      currentRole?.isTasykilPP,
      currentRole?.isAdminPP,
      currentRole?.isSuperAdmin,
    ]
  );

  const { data, isFetching, status, refetch } = useQuery({
    queryKey: [pcValue, pdValue, pwValue],
    queryFn: async () => {
      const res = await getStatistic({
        PC: pcValue || "",
        PD: pdValue || "",
        PW: pwValue || "",
      });
      return res;
    },
    enabled: !currentRole.isAnggota,
  });

  const { isLoading: isLoadingPW } = useQuery({
    queryKey: ["pws"],
    queryFn: async () => {
      const results = await getPW();
      setPws(results ?? []);
      return results;
    },
  });

  const { refetch: refetchPds, isLoading: isLoadingPd } = useQuery({
    queryKey: ["pds-" + pwValue],
    queryFn: async () => {
      if (pwValue) {
        const results = await getPDByPW(pwValue);
        setPds(results ?? []);
        return results;
      }
      return [];
    },
  });

  const { refetch: refetchPcs, isLoading: isLoadingPc } = useQuery({
    queryKey: ["pcs-" + pdValue],
    queryFn: async () => {
      if (pdValue) {
        const results = await getPCByPD(pdValue);
        setPcs(results ?? []);
        return results;
      }
      return [];
    },
  });

  const loading = useMemo(
    () => isFetching || isLoadingPW || isLoadingPd || isLoadingPc,
    [isFetching, isLoadingPW, isLoadingPc, isLoadingPd]
  );

  const response = useMemo(
    (): StatisticResponse => (isFetching ? [] : data?.data),
    [data?.data, isFetching]
  );

  const modifiedClothesSizeSummary = useMemo(() => {
    if (response?.clothes_size_summary) {
      const modified = response.clothes_size_summary.reduce(
        (acc, curr) => {
          const found = acc.find((item) => item.pcCode === curr.pcCode);
          if (found) {
            found.values.push(curr);
          } else {
            acc.push({
              pcCode: curr.pcCode,
              pcName: curr.pcName,
              values: [curr],
            });
          }
          return acc;
        },
        [] as {
          pcCode: string;
          pcName: string;
          values: { clothesSize: string; value: number }[];
        }[]
      );
      return modified;
    }
    return [];
  }, [response?.clothes_size_summary]);

  // useEffect(() => {
  //   // call refetch each 10 seconds
  //   const timeoutRefetch = setInterval(() => {
  //     process.env.NODE_ENV !== "development" && refetch();
  //   }, 10000);

  //   return () => clearInterval(timeoutRefetch);
  //   // eslint-disable-next-line react-hooks/exhaustive-deps
  // }, []);

  if (status !== "error" && !isAllowed) {
    return null;
  }

  return (
    <div className="space-y-4">
      {isAllowed && (
        <Card className="sticky top-0 z-20">
          <CardBody className="flex gap-3 md:flex-row">
            <Autocomplete
              labelPlacement="outside"
              label="Pimpinan Wilayah"
              name="pw"
              id="pw"
              placeholder="Cari Pimpinan Wilayah Anda"
              className="w-full"
              isRequired
              selectedKey={pwValue}
              onSelectionChange={(item) => {
                setPwValue(item as string);
                setPdValue(null);
                setPcValue(null);
                refetchPds();
              }}
            >
              {pws.map((pw) => (
                <AutocompleteItem key={pw.kdPw} textValue={pw.namaWilayah}>
                  <div>{pw.namaWilayah}</div>
                </AutocompleteItem>
              ))}
            </Autocomplete>
            <Autocomplete
              labelPlacement="outside"
              label="Pimpinan Daerah"
              name="pd"
              id="pd"
              placeholder="Cari Pimpinan Daerah Anda"
              className="w-full"
              selectedKey={pdValue}
              isDisabled={!pwValue}
              isRequired
              onSelectionChange={(item) => {
                setPdValue(null);
                setPdValue(item as string);
                refetchPcs();
                setPcValue(null);
              }}
            >
              {pds.map((pd) => (
                <AutocompleteItem key={pd.kdPd} textValue={pd.namaPd}>
                  <div>{pd.namaPd}</div>
                </AutocompleteItem>
              ))}
            </Autocomplete>
            <Autocomplete
              labelPlacement="outside"
              label="Pimpinan Cabang"
              name="pc"
              id="pc"
              placeholder="Cari Pimpinan Cabang Anda"
              isDisabled={!pdValue}
              isRequired
              className="w-full"
              selectedKey={pcValue}
              onSelectionChange={(item) => {
                setPcValue(null);
                setPcValue(item as string);
              }}
            >
              {pcs.map((pc) => (
                <AutocompleteItem key={pc.kdPc} textValue={pc.namaPc}>
                  <div>{pc.namaPc}</div>
                </AutocompleteItem>
              ))}
            </Autocomplete>
          </CardBody>
        </Card>
      )}

      <Card>
        <CardHeader>
          <SectionTitle
            text="Rangkuman Data Peserta"
            className="text-xl font-medium"
          />
        </CardHeader>
        <Divider />

        <CardBody className="grid grid-cols-1 gap-2">
          <div>
            <SubsectionTitle
              text="Total Peserta"
              className="mb-2 font-semibold"
            />
            <div className="grid grid-cols-1 gap-2 md:grid-cols-6 md:gap-6 lg:grid-cols-9">
              {loading ? (
                <div className="flex w-full justify-center py-6 md:col-span-2 lg:col-span-3">
                  <Spinner />
                </div>
              ) : (
                <StatisticCard
                  icon={<FaPeopleGroup className="h-6 w-6" />}
                  label="Peserta"
                  value={response?.total_jumlah_peserta ?? 0}
                />
              )}
              {loading ? (
                <div className="flex w-full justify-center py-6 md:col-span-2 lg:col-span-3">
                  <Spinner />
                </div>
              ) : (
                <StatisticCard
                  icon={<FaPeopleGroup className="h-6 w-6" />}
                  label="Check-in Peserta"
                  value={response?.total_jumlah_peserta_checked ?? 0}
                />
              )}
              {loading ? (
                <div className="flex w-full justify-center py-6 md:col-span-2 lg:col-span-3">
                  <Spinner />
                </div>
              ) : (
                <StatisticCard
                  icon={<FaPeopleGroup className="h-6 w-6" />}
                  label="Calon Peserta"
                  value={response?.total_jumlah_calon_peserta ?? 0}
                />
              )}
            </div>
          </div>

          <div>
            <SubsectionTitle
              text="Total Anggota"
              className="mb-2 font-semibold"
            />
            <div className="grid grid-cols-1 gap-2 md:grid-cols-6 md:gap-6 lg:grid-cols-9">
              {loading ? (
                <div className="flex w-full justify-center py-6 md:col-span-2 lg:col-span-3">
                  <Spinner />
                </div>
              ) : (
                <StatisticCard
                  icon={<FaPeopleGroup className="h-6 w-6" />}
                  label="Peserta Anggota"
                  value={response?.total_jumlah_peserta_anggota ?? 0}
                  color="secondary"
                />
              )}
              {loading ? (
                <div className="flex w-full justify-center py-6 md:col-span-2 lg:col-span-3">
                  <Spinner />
                </div>
              ) : (
                <StatisticCard
                  icon={<FaPeopleGroup className="h-6 w-6" />}
                  label="Check-in Peserta Anggota"
                  value={response?.total_jumlah_peserta_anggota_checked ?? 0}
                  color="secondary"
                />
              )}
              {loading ? (
                <div className="flex w-full justify-center py-6 md:col-span-2 lg:col-span-3">
                  <Spinner />
                </div>
              ) : (
                <StatisticCard
                  icon={<FaPeopleGroup className="h-6 w-6" />}
                  label="Calon Peserta Anggota"
                  value={response?.total_jumlah_calon_peserta_anggota ?? 0}
                  color="secondary"
                />
              )}
            </div>
          </div>

          <div>
            <SubsectionTitle
              text="Total Non Anggota"
              className="mb-2 font-semibold"
            />
            <div className="grid grid-cols-1 gap-2 md:grid-cols-6 md:gap-6 lg:grid-cols-9">
              {loading ? (
                <div className="flex w-full justify-center py-6 md:col-span-2 lg:col-span-3">
                  <Spinner />
                </div>
              ) : (
                <StatisticCard
                  icon={<FaPeopleGroup className="h-6 w-6" />}
                  label="Peserta Bukan Anggota"
                  value={response?.total_jumlah_peserta_non_anggota ?? 0}
                  color="red-500"
                />
              )}
              {loading ? (
                <div className="flex w-full justify-center py-6 md:col-span-2 lg:col-span-3">
                  <Spinner />
                </div>
              ) : (
                <StatisticCard
                  icon={<FaPeopleGroup className="h-6 w-6" />}
                  label="Check-in Peserta Bukan Anggota"
                  value={
                    response?.total_jumlah_peserta_non_anggota_checked ?? 0
                  }
                  color="red-500"
                />
              )}
              {loading ? (
                <div className="flex w-full justify-center py-6 md:col-span-2 lg:col-span-3">
                  <Spinner />
                </div>
              ) : (
                <StatisticCard
                  icon={<FaPeopleGroup className="h-6 w-6" />}
                  label="Calon Peserta Bukan Anggota"
                  value={response?.total_jumlah_calon_peserta_non_anggota ?? 0}
                  color="red-500"
                />
              )}
            </div>
          </div>
        </CardBody>
      </Card>

      <Card>
        <CardHeader>
          <SectionTitle
            text="Rangkuman Data Panitia"
            className="text-xl font-medium"
          />
        </CardHeader>
        <Divider />

        <CardBody className="grid grid-cols-1 gap-2">
          <div>
            <SubsectionTitle
              text="Total Panitia"
              className="mb-2 font-semibold"
            />
            <div className="grid grid-cols-1 gap-2 md:grid-cols-6 md:gap-6 lg:grid-cols-9">
              {loading ? (
                <div className="flex w-full justify-center py-6 md:col-span-2 lg:col-span-3">
                  <Spinner />
                </div>
              ) : (
                <StatisticCard
                  icon={<FaPeopleGroup className="h-6 w-6" />}
                  label="Panitia"
                  value={response?.total_jumlah_panitia ?? 0}
                  color="secondary"
                />
              )}
              {loading ? (
                <div className="flex w-full justify-center py-6 md:col-span-2 lg:col-span-3">
                  <Spinner />
                </div>
              ) : (
                <StatisticCard
                  icon={<FaPeopleGroup className="h-6 w-6" />}
                  label="Check-in Panitia"
                  value={response?.total_jumlah_panitia_checked ?? 0}
                  color="secondary"
                />
              )}
              {loading ? (
                <div className="flex w-full justify-center py-6 md:col-span-2 lg:col-span-3">
                  <Spinner />
                </div>
              ) : (
                <StatisticCard
                  icon={<FaPeopleGroup className="h-6 w-6" />}
                  label="Calon Panitia"
                  value={response?.total_jumlah_calon_panitia ?? 0}
                  color="secondary"
                />
              )}
            </div>
          </div>
        </CardBody>
      </Card>

      <Card>
        <CardHeader>
          <SectionTitle
            text="Rangkuman Data Acara"
            className="text-xl font-medium"
          />
        </CardHeader>
        <Divider />
        <CardBody className="flex-col-reverse gap-4">
          {response?.event_summary?.map((event, index) => (
            <div key={index}>
              <SubsectionTitle
                text={
                  event.name === "ETC" ? "Lainnya" : event.name.toLowerCase()
                }
                className="mb-2 font-semibold !capitalize"
              />
              <div className="grid grid-cols-1 gap-2 md:grid-cols-6 md:gap-6 lg:grid-cols-9 xl:grid-cols-12">
                {event.values.map((value) => (
                  <>
                    {loading ? (
                      <div className="flex w-full justify-center py-6 md:col-span-2 lg:col-span-3">
                        <Spinner />
                      </div>
                    ) : (
                      <StatisticCard
                        key={value.name}
                        icon={<FaPeopleGroup className="h-6 w-6" />}
                        label={value.name}
                        value={value.value}
                      />
                    )}
                  </>
                ))}
              </div>
            </div>
          ))}
        </CardBody>
      </Card>

      <Card>
        <CardHeader>
          <SectionTitle
            text="Rangkuman Data Baju Tiap PC"
            className="text-xl font-medium"
          />
        </CardHeader>
        <Divider />
        <CardBody className="flex flex-row flex-wrap justify-center gap-2">
          {modifiedClothesSizeSummary?.map((daerah, index) => (
            <Card key={index} className="w-[100px] md:w-[175px]">
              <CardHeader>
                <SubsectionTitle
                  text={`PC ${daerah.pcName}`}
                  className="text-xs font-semibold !capitalize md:text-sm"
                />
              </CardHeader>
              <Divider />
              <CardBody>
                {daerah?.values.map((value, index) => (
                  <Fragment key={index}>
                    {loading ? (
                      <div className="flex w-full justify-center py-6 md:col-span-3">
                        <Spinner />
                      </div>
                    ) : (
                      <div className="flex space-x-2 italic">
                        <small>{value.clothesSize} : </small>
                        <small>{value.value.toString()}</small>
                      </div>
                    )}
                  </Fragment>
                ))}
              </CardBody>
            </Card>
          ))}
        </CardBody>
      </Card>
    </div>
  );
};
export default StatisticList;
