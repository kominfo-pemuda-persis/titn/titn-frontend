import { ReactNode } from "react";

import { Card, CardBody } from "@/components/atoms/Card/Card";
import { Paragraph } from "@/components/atoms/Text";

export const StatisticCard = ({
  label,
  value,
  icon,
  color = "primary",
}: {
  label: string;
  value: number | string;
  icon: ReactNode;
  color?: string;
}) => {
  return (
    <Card className="col-span-3 dark:bg-default-100">
      <CardBody className="items-center justify-center">
        <div className="flex w-full items-center justify-between space-x-4">
          <div className="mx-auto">
            <div
              className={`flex items-center justify-center text-white bg-${color || "primary"} h-12 w-12 rounded-full`}
            >
              {icon}
            </div>
          </div>
          <div className="flex flex-1 flex-col">
            <Paragraph
              text={label}
              className="!text-sm leading-3 dark:text-foreground"
            />
            <Paragraph text={value} className="!text-2xl font-bold" />
          </div>
        </div>
      </CardBody>
    </Card>
  );
};
