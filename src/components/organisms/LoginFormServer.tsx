"use client";

import { MouseEvent, useState } from "react";
import { useRouter, useSearchParams } from "next/navigation";
import { useMutation } from "@tanstack/react-query";
import { useSetAtom } from "jotai";

import { Button } from "@/components/atoms/Button";
import { Input, PasswordInput } from "@/components/atoms/Input";
import { SubsectionTitle, Subtitle, Title } from "@/components/atoms/Text";
import { ROUTE_DASHBOARD } from "@/constant/routes";
import { authenticate } from "@/services/actions";
import { loggedUser } from "@/states/user";

const LoginFormServer = () => {
  const router = useRouter();
  const searchParams = useSearchParams();
  const setCurrentUser = useSetAtom(loggedUser);

  const [npa, setNpa] = useState<{
    value: string;
    isChanged: boolean;
  }>({
    value: "",
    isChanged: false,
  });

  const [password, setPassword] = useState<{
    value: string;
    isChanged: boolean;
  }>({
    value: "",
    isChanged: false,
  });

  const { mutate, error, isPending } = useMutation({
    mutationFn: async () => {
      const user = await authenticate({
        npa: npa.value,
        password: password.value,
      });

      if (!user.error) {
        setCurrentUser(user);

        const redirectUrl = searchParams.get("redirect");

        if (redirectUrl) {
          router.push(redirectUrl);
        } else {
          router.push(ROUTE_DASHBOARD);
        }
      } else {
        throw new Error("Login gagal, pastikan NPA dan password benar.");
      }
    },
  });

  const handleSubmit = async (event: MouseEvent<HTMLFormElement>) => {
    event.preventDefault();

    if (npa.value !== "" || password.value !== "") mutate();
  };

  return (
    <div className="mx-auto flex w-full min-w-80 max-w-md justify-center space-y-4 rounded-none border bg-white shadow-md dark:bg-slate-800 sm:rounded-xl">
      <form
        id="login-form"
        onSubmit={handleSubmit}
        className="flex w-full flex-col gap-4 rounded-md px-8 py-10 shadow-sm"
      >
        <div className="mb-8 space-y-2 text-center">
          <Title text="Masuk Admin" className="font-medium text-primary" />
          <Subtitle
            text="Masukan NPA dan password anaonline anda"
            className="text-center !text-medium text-secondary"
          />
        </div>

        <Input
          name="npa"
          type="text"
          label="Npa"
          autoComplete="username"
          placeholder="Masukan NPA anda"
          isRequired
          isClearable
          isDisabled={isPending}
          isInvalid={npa.value === "" && npa.isChanged}
          errorMessage="Tolong isi NPA anda"
          color="primary"
          value={npa.value}
          onValueChange={(value) =>
            setNpa({
              value,
              isChanged: true,
            })
          }
        />

        <PasswordInput
          name="password"
          type="password"
          label="Password"
          autoComplete="current-password"
          placeholder="Masukan password anda"
          isRequired
          isDisabled={isPending}
          isInvalid={password.value === "" && password.isChanged}
          errorMessage="Tolong isi password anda"
          color="primary"
          value={password.value}
          onValueChange={(value) =>
            setPassword({
              value,
              isChanged: true,
            })
          }
        />

        {error?.message && (
          <SubsectionTitle
            text={error?.message}
            className="text-sm text-red-500"
          />
        )}

        <Button
          type="submit"
          variant="solid"
          color="primary"
          isDisabled={npa.value === "" || password.value === "" || isPending}
          isLoading={isPending}
        >
          Masuk
        </Button>
      </form>
    </div>
  );
};

export default LoginFormServer;
