import Image from "next/image";

import { Paragraph } from "@/components/atoms/Text";

const Footer = ({
  withoutLogo,
  className = "",
}: {
  withoutLogo?: boolean;
  className?: string;
}) => (
  <footer
    className={`absolute bottom-0 left-0 right-0 space-y-4 py-2 text-center ${className}`}
  >
    {withoutLogo || (
      <Image
        src="/assets/Berdakwah_Berdampak.png"
        width={200}
        height={100}
        quality={100}
        alt="Slogan PP"
        className="mx-auto h-full w-full max-w-32"
      />
    )}
    <Paragraph
      text="© 2024 | KOMINFO PP Pemuda PERSIS"
      className="text-sm text-primary"
    />
  </footer>
);

export { Footer };
