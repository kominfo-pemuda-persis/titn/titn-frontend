"use client";

import { useCallback, useEffect, useMemo, useState } from "react";
import { useCookies } from "next-client-cookies";
import dynamic from "next/dynamic";
import { useSearchParams } from "next/navigation";
import { getKeyValue, useDisclosure } from "@nextui-org/react";
import { useQuery } from "@tanstack/react-query";
import { useAtom } from "jotai";
import { BsCardList, BsCheck, BsEye, BsSend, BsX } from "react-icons/bs";

import { Button } from "@/components/atoms/Button";
import { Chip } from "@/components/atoms/Chip";
import Toast from "@/components/atoms/Toast";
import TableTemplate from "@/components/molecules/Table/Template";
import { COLUMNS, statusOptions } from "@/constant/data/recap";
import { updateRecap } from "@/services/mutation/recaps";
import { generateAndSendSyahadahByRekapId } from "@/services/mutation/syahadah";
import { listRecaps } from "@/services/query/recaps";
import { tableStates } from "@/states/table";
import { Recap, RecapStatus, RowRekap } from "@/types";
import { Sort } from "@/types/utils";
import { getColorBasedOnStatus } from "@/utils";
import { sleep } from "@/utils/debounce";

const ModalConfirmation = dynamic(
  () => import("@/components/organisms/Modal/ModalConfirmation"),
  {
    ssr: false,
  }
);

const ModalShowImage = dynamic(
  () => import("@/components/organisms/Modal/ModalShowImage"),
  {
    ssr: false,
  }
);

const ModalDetailRekap = dynamic(
  () => import("@/components/organisms/Modal/ModalDetailRekap"),
  {
    ssr: false,
  }
);

const TableRekap = () => {
  const { onOpen, isOpen, onClose } = useDisclosure();

  const [
    { searchValue, page, rowsPerPage, sortDescriptor, statusFilter },
    setTableConfig,
  ] = useAtom(tableStates);

  const cookie = useCookies();
  const session = cookie?.get("session");
  const searchParams = useSearchParams();

  const {
    onOpen: onOpenModalImage,
    isOpen: isOpenModalImage,
    onClose: onCloseModalImage,
  } = useDisclosure();

  const {
    onOpen: onOpenRekap,
    isOpen: isOpenRekap,
    onClose: onCloseRekap,
  } = useDisclosure();

  const [selectedIdRecap, setSelectedIdRecap] = useState<number | null>(null);

  const handleCloseRekap = () => {
    setSelectedIdRecap(null);
    onCloseRekap();
  };

  const [modalValue, setModalValue] = useState<{
    title: string;
    message: string;
    onConfirm: () => void;
  }>({
    title: "",
    message: "",
    onConfirm: () => {},
  });

  const [modalShowImage, setModalShowImage] = useState<{
    url: string;
  }>({
    url: "",
  });

  const [isLoading, setIsLoading] = useState<boolean>(false);

  const { data, isFetching, refetch, error } = useQuery({
    queryKey: [
      searchValue,
      rowsPerPage,
      page,
      Array.from(statusFilter),
      sortDescriptor.column,
      sortDescriptor.direction,
    ],
    enabled: !isOpenRekap || Array.from(statusFilter).length !== 0,
    queryFn: async ({ signal }) => {
      await sleep(750);

      return await listRecaps({
        keyword: searchValue,
        pageSize: rowsPerPage,
        pageNo: page > 0 ? page - 1 : 0,
        signal,
        session,
        status:
          Array.from(statusFilter).length > 0
            ? (Array.from(statusFilter)[0] as RecapStatus)
            : RecapStatus.SUBMITTED,
        sortBy: sortDescriptor.column as string,
        sortOrder:
          sortDescriptor.direction === "ascending" ? Sort.ASC : Sort.DESC,
      });
    },
  });

  const response = useMemo(
    () => (isFetching ? [] : data?.data),
    [data?.data, isFetching]
  );

  const filteredItems: RowRekap[] = useMemo(() => {
    let mappedData = response?.content?.map((item: Recap, index: number) => ({
      key: item.recap_id,
      npa: item.npa || "-",
      fullName: item.full_name || "-",
      pcName: item.pc_name || "-",
      pdName: item.pd_name || "-",
      pwName: item.pw_name || "-",
      nums: item.nums || "-",
      createdAt: new Date(item.created_at)
        .toLocaleDateString("id-ID", {
          year: "numeric",
          month: "long",
          day: "numeric",
          hour: "numeric",
          minute: "numeric",
          timeZoneName: "short",
        })
        .replace("pukul", ""),
      status: item.status || "-",
      buktiTransfer: item.bukti_transfer || "-",
      no: page === 1 ? index + 1 : (page - 1) * rowsPerPage + index + 1,
    }));

    return mappedData;
  }, [page, response?.content, rowsPerPage]);

  const handleApprove = async (id: string) => {
    setModalValue({
      title: "Approve Rekap",
      message: "Apakah Anda yakin ingin menyetujui rekap ini?",
      onConfirm: async () => {
        setIsLoading(true);
        const response = await updateRecap(id, {
          status: RecapStatus.APPROVED,
          reject_reason: "",
        });
        if (response.success) {
          Toast({
            message: "Rekap berhasil disetujui!",
            type: "success",
          });
          refetch();
        } else {
          Toast({
            message: response.message as string,
            type: "error",
          });
          console.log(response.message);
        }
        setIsLoading(false);
      },
    });
    onOpen();
  };

  const handleOpenESyahadahConfirmation = async (id: string) => {
    setModalValue({
      title: "Kirim E-Syahadah",
      message:
        "Apakah Anda yakin ingin mengirim E-Syahadah kepada seluruh peserta didalam rekap ini?",
      onConfirm: async () => {
        setIsLoading(true);

        const response = await generateAndSendSyahadahByRekapId(id);

        if (response.success) {
          Toast({
            message: "E-Syahadah berhasil dikirim!",
            type: "success",
          });
        } else {
          Toast({
            message: response.message as string,
            type: "error",
          });
        }
        setIsLoading(false);
      },
    });

    onOpen();
  };

  const showModalImage = (rekap: RowRekap, columnKey: string): void => {
    setModalShowImage({
      url: getKeyValue(rekap, columnKey),
    });
    onOpenModalImage();
  };

  const showModalDetailRekap = (rekap: RowRekap, columnKey: string): void => {
    setSelectedIdRecap(getKeyValue(rekap, "key"));
    onOpenRekap();
  };

  const handleReject = async (id: string) => {
    setModalValue({
      title: "Reject Rekap",
      message: "Apakah Anda yakin ingin menolak rekap ini?",
      onConfirm: async () => {
        setIsLoading(true);
        const response = await updateRecap(id, {
          status: RecapStatus.REJECTED,
          reject_reason: "Ditolak",
        });
        if (response.success) {
          refetch();
        }
        setIsLoading(false);
      },
    });
    onOpen();
  };

  const renderCell = useCallback((rekap: RowRekap, columnKey: string) => {
    switch (columnKey) {
      case "status":
        return (
          <Chip
            color={`${getColorBasedOnStatus(getKeyValue(rekap, columnKey))}`}
            size="sm"
            variant="flat"
          >
            {getKeyValue(rekap, columnKey)}
          </Chip>
        );

      case "nums":
        return (
          <div className="flex items-center justify-center">
            {getKeyValue(rekap, columnKey)}
            <Button
              isIconOnly
              onClick={(e) => {
                e.stopPropagation();
                showModalDetailRekap(rekap, columnKey);
              }}
              variant="light"
              size="sm"
              color="primary"
              title="Lihat"
              className="ml-1"
            >
              <BsCardList size={20} />
            </Button>
          </div>
        );

      case "buktiTransfer":
        return (
          <Button
            isIconOnly
            onClick={(e) => {
              e.stopPropagation();
              showModalImage(rekap, columnKey);
            }}
            variant="light"
            size="sm"
            color="warning"
            title="Lihat"
          >
            <BsEye size={20} />
          </Button>
        );

      case "actions":
        return (
          <div className="relative flex items-center justify-center gap-2">
            {rekap.status === RecapStatus.APPROVED && (
              <Button
                size="sm"
                color="success"
                variant="flat"
                title="Send E-syahadah"
                onClick={() => handleOpenESyahadahConfirmation(rekap.key)}
              >
                <BsSend size={15} />
                <span className="block">E-Syahadah</span>
              </Button>
            )}
            {rekap.status === RecapStatus.SUBMITTED && (
              <>
                <Button
                  isIconOnly
                  size="sm"
                  color="success"
                  variant="light"
                  title="Approve"
                  onClick={() => handleApprove(rekap.key)}
                >
                  <BsCheck size={20} />
                </Button>

                <Button
                  isIconOnly
                  size="sm"
                  variant="light"
                  color="danger"
                  title="Reject"
                  onClick={() => handleReject(rekap.key)}
                >
                  <BsX size={20} />
                </Button>
              </>
            )}
          </div>
        );

      default:
        return getKeyValue(rekap, columnKey);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    const hasFilter = searchParams.has("filter");

    setTableConfig((prev) => ({
      ...prev,
      statusFilter: hasFilter
        ? new Set([searchParams.get("filter") as string])
        : new Set([RecapStatus.SUBMITTED]),
    }));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [searchParams]);

  useEffect(() => {
    if (error) {
      Toast({
        message: "Gagal mendapatkan data",
        type: "error",
      });
    }
  }, [error]);

  return (
    <>
      <TableTemplate
        response={response}
        isFetching={isFetching}
        emptyContent="Rekap tidak ditemukan"
        renderCell={renderCell}
        columns={COLUMNS}
        items={filteredItems}
        statusOptions={statusOptions}
      />

      <ModalDetailRekap
        isOpen={isOpenRekap}
        onClose={handleCloseRekap}
        recapId={selectedIdRecap}
        size="5xl"
      />

      <ModalShowImage
        onClose={onCloseModalImage}
        url={modalShowImage.url}
        isOpen={isOpenModalImage}
        handleOpen={onOpenModalImage}
      />

      <ModalConfirmation
        isOpen={isOpen}
        onClose={onClose}
        title={modalValue.title}
        message={modalValue.message}
        handleOpen={modalValue.onConfirm}
        isDisabled={isLoading}
        isLoading={isLoading}
      />
    </>
  );
};

export default TableRekap;
