"use client";

import { FormEvent, useRef, useState } from "react";
import { useMutation } from "@tanstack/react-query";
import { z } from "zod";

import { Button } from "@/components/atoms/Button";
import { Image } from "@/components/atoms/Image";
import { Input } from "@/components/atoms/Input";
import { Paragraph } from "@/components/atoms/Text";
import {
  Modal,
  ModalBody,
  ModalContent,
  ModalFooter,
  ModalHeader,
} from "@/components/molecules/Modal";
import { postRegisterParticipant } from "@/services/mutation/participant";
import { BaseResponse, DataPengunjungScheme } from "@/types";

export const PendaftaranPengunjung = () => {
  const [success, setSuccess] = useState<boolean>(false);
  const [error, setError] = useState<string>("");

  const { mutate, isPending } = useMutation({
    mutationFn: postRegisterParticipant,
    onSuccess: (resp: BaseResponse) => {
      if (resp.success) {
        setSuccess(true);
        setError("");
      } else {
        setSuccess(true);
        setError(resp.message ?? "Gagal Mendaftarkan Pengunjung");
      }
    },
  });

  const formRef = useRef<HTMLFormElement>(null);

  const onSubmit = async (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault();

    try {
      const formData = new FormData(event.currentTarget);
      const full_name = formData.get("nama") as string;
      const email = formData.get("email") as string;
      const phone = formData.get("phone") as string;
      const randomIdentifierNIK = new Date().getTime().toString();

      const payload = {
        full_name,
        email,
        phone,
        event_ids: [23],
        // Ignored fields
        clothes_size: "XXXL",
        nik: randomIdentifierNIK,
        pc_code: "99999999",
        pc_name: "tidak ada pc",
        pd_code: "99999999",
        pd_name: "tidak ada pd",
        pw_code: "99999999",
        pw_name: "tidak ada pw",
      };

      const parsed = DataPengunjungScheme.parse(payload);
      mutate(parsed);
    } catch (e) {
      if (e instanceof z.ZodError) {
        setSuccess(true);
        const messages = e.issues.map((err) => {
          return err.message + "\n";
        });
        setError(messages.join(""));
      }
    }
  };

  return (
    <div className="container mx-auto w-full px-4">
      <form ref={formRef} id="register-pengunjung" onSubmit={onSubmit}>
        <div className="flex flex-col gap-2 space-y-10">
          <Input
            labelPlacement="outside"
            id="nama"
            label="Nama"
            name="nama"
            placeholder="Fulan"
            isRequired
            disabled={isPending}
          />
          <Input
            labelPlacement="outside"
            id="email"
            placeholder="username@email.com"
            name="email"
            type="email"
            label="Email"
            isRequired
            disabled={isPending}
          />
          <Input
            labelPlacement="outside"
            id="phone"
            placeholder="08987654321"
            type="phone"
            name="phone"
            label="Phone"
            isRequired
            disabled={isPending}
            maxLength={15}
          />
        </div>
        <div className="my-4 flex flex-col items-center gap-1">
          <Paragraph
            text="(Silahkan langsung melakukan pembayaran sebesar 35rb/peserta)"
            className="!text-sm text-slate-500"
          />
          <div className="flex gap-2">
            <Image
              src="/assets/BRI_2020.png"
              alt="Logo BRI"
              radius="none"
              width={100}
              className="mx-auto"
            />
            <div className="!text-left">
              <Paragraph text="Atas nama Hendi Santika" className="!text-sm" />
              <Paragraph
                text={
                  <span className="flex w-full items-center space-x-1">
                    <span>3772</span>
                    <span>0102</span>
                    <span>1521</span>
                    <span>502</span>
                  </span>
                }
                className="!text-sm font-bold italic"
              />
            </div>
          </div>
        </div>
        <Button
          isDisabled={isPending}
          type="submit"
          className="mt-2 w-full bg-[#89d09d]"
        >
          Daftar
        </Button>
      </form>
      <Modal
        isOpen={success}
        onClose={() => {
          setSuccess(false);
          if (!error) {
            formRef.current?.reset();
          }
        }}
      >
        <ModalContent>
          <ModalHeader />
          <ModalBody className="w-full items-center font-semibold">
            {error.length > 0 ? (
              <div className="text-red-500">
                {error.split("\n").map((e) => (
                  <p key={e}>{e}</p>
                ))}
              </div>
            ) : (
              <div className="text-green-500">Pendaftaran Berhasil</div>
            )}
          </ModalBody>
          <ModalFooter className="w-full justify-center">
            <Button
              color={error.length > 0 ? "danger" : "success"}
              onPress={() => {
                setSuccess(false);
                if (!error) {
                  formRef.current?.reset();
                }
              }}
            >
              {error ? "Ok" : "Ok"}
            </Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </div>
  );
};
