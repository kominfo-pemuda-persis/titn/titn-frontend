"use client";

import { FormEvent, useRef, useState } from "react";
import { Dropzone, ExtFile, FileMosaic } from "@files-ui/react";
import { useMutation } from "@tanstack/react-query";

import { Button } from "@/components/atoms/Button";
import { Paragraph, SubsectionTitle } from "@/components/atoms/Text";
import ModalConfirmation from "@/components/organisms/Modal/ModalConfirmation";
import { postSyahadah } from "@/services/mutation/syahadah";
import { BaseResponse } from "@/types";

const FormSyahadahWrapper = () => {
  const [isOpenModal, setOpenModal] = useState<boolean>(false);
  const [error, setError] = useState<string>("");
  const [files, setFiles] = useState<ExtFile[]>([]);

  const formRef = useRef<HTMLFormElement>(null);

  const { mutate, isPending } = useMutation({
    mutationFn: postSyahadah,
    onSuccess: (resp: BaseResponse) => {
      if (resp.success) {
        setOpenModal(true);
        setError("");
      } else {
        setOpenModal(true);
        setError(resp.message ?? "Gagal Mengirimkan Bukti Transfer");
      }
    },
  });

  const handleChangeFiles = (newFiles: ExtFile[]) => {
    setFiles(newFiles);
  };

  const onSubmit = async (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault();

    try {
      if (event.currentTarget.checkValidity()) {
        try {
          if (files.length === 0 || !files[0].file || !files[0].name) {
            setError("File tidak boleh kosong");
            setOpenModal(true);
            return;
          }
          const data = new FormData();
          data.append("file", files[0].file);
          mutate(data);
        } catch {
          setError("Gagal Mengsubmit Data Penerima" as string);
          setOpenModal(true);
        }
      }
    } catch (error) {
      console.log(error);
    }
  };

  const handleCloseModal = () => {
    setError("");
    setOpenModal(false);
    if (!error) {
      formRef.current?.reset();
      setFiles([]);
    }
  };

  return (
    <>
      <form
        ref={formRef}
        id="rekap-form"
        onSubmit={onSubmit}
        className="mx-auto my-4 flex w-full max-w-3xl flex-col gap-4 px-5 lg:flex-row"
      >
        <div className="flex-1">
          <SubsectionTitle
            text="Daftar Penerima E-Syahadah"
            className="font-medium text-secondary"
          />
          <Paragraph
            text="Silahkan upload daftar penerima E-Syahadah"
            className="!text-sm text-gray-500"
          />
        </div>

        <div className="flex-1">
          <Dropzone
            accept=".xlsx"
            label="Upload daftar penerima E-Syahadah"
            onChange={handleChangeFiles}
            maxFiles={1}
            maxFileSize={2 * 1024 * 1024}
            value={files}
          >
            {files.map((file) => (
              <FileMosaic key={file.id} {...file} alwaysActive preview />
            ))}
          </Dropzone>

          <Button
            type="submit"
            className="mt-2 w-full text-white"
            isLoading={isPending}
            color="primary"
          >
            Kirim
          </Button>
        </div>
      </form>

      <ModalConfirmation
        size="lg"
        isOpen={isOpenModal}
        onClose={handleCloseModal}
        isDisabled={isPending}
        isLoading={isPending}
        message={
          error ? (
            <>
              <SubsectionTitle text="Error" className="text-xl text-red-500" />
              <ul className="list-inside list-disc">
                {error
                  .split("\n")
                  .filter(Boolean)
                  .map((e) => (
                    <li key={e}>{e}</li>
                  ))}
              </ul>
            </>
          ) : (
            <div className="text-green-500">
              Pengiriman Daftar Penerima E-Syahadah Berhasil
            </div>
          )
        }
        footer={
          <Button
            color={error ? "danger" : "success"}
            size="sm"
            onPress={handleCloseModal}
          >
            Ok
          </Button>
        }
      />
    </>
  );
};

export default FormSyahadahWrapper;
