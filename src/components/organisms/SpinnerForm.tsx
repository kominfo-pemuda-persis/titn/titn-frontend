"use client";

import { Key, useRef, useState } from "react";
import { useCookies } from "next-client-cookies";
import dynamic from "next/dynamic";
import { useMutation, useQuery } from "@tanstack/react-query";
import { SwipeableButton } from "react-swipeable-button";

import { Button } from "@/components/atoms/Button";
import { Card, CardBody } from "@/components/atoms/Card";
import { Chip } from "@/components/atoms/Chip";
import { Input } from "@/components/atoms/Input";
import { Scanner, ScannerProps } from "@/components/atoms/Scanner";
import {
  Modal,
  ModalBody,
  ModalContent,
  ModalFooter,
  ModalHeader,
} from "@/components/molecules/Modal";
import { Tab, Tabs } from "@/components/molecules/Tabs/Tabs";
import { postSpinner } from "@/services/mutation/participant";
import { listParticipant } from "@/services/query/participant";
import { BaseResponse, Participant, ParticipantStatus } from "@/types";

const TablePesertaSpinned = dynamic(
  () =>
    import("@/components/organisms/TablePesertaSpinned/TablePesertaSpinned"),
  {
    ssr: false,
  }
);

const SpinnerForm = () => {
  const [selectedMenu, setSelectedMenu] = useState<Key>("scanner");
  const [namaPeserta, setNamaPeserta] = useState<string>("");
  const [kodePeserta, setKodePeserta] = useState<string>("");
  const [participant, setParticipant] = useState<Participant[]>(
    [] as Participant[]
  );
  const [success, setSuccess] = useState<boolean>(false);
  const [error, setError] = useState<string>("");
  const formRef = useRef<HTMLFormElement>(null);
  const cookie = useCookies();
  const session = cookie?.get("session");

  const { isPending, mutate } = useMutation({
    mutationFn: postSpinner,
    onSuccess: (resp: BaseResponse) => {
      if (resp.success) {
        setSuccess(true);
        setError("");
        setNamaPeserta("");
      } else {
        setSuccess(true);
        setError(resp.message ?? "Gagal Merubah Data Peserta");
      }
    },
    onError: (resp: BaseResponse) => {
      setSuccess(true);
      setError(resp.message ?? "Gagal Merubah Data Peserta");
    },
  });

  const getScannerResults: ScannerProps["onScan"] = (results) => {
    mutate({ code: results[0].rawValue });
  };

  useQuery({
    queryKey: ["participant", namaPeserta],
    queryFn: async () => {
      setParticipant([]);

      // get anggota participant
      const { data } = await listParticipant({
        keyword: namaPeserta,
        session,
        status: ParticipantStatus.CHECKED_IN,
      });
      const results = data?.content ?? [];
      setParticipant(results);
      return results;
    },
    retry: 0,
    enabled: namaPeserta.length > 2,
  });

  const handleClick = () => {
    mutate({ code: kodePeserta });
  };

  return (
    <>
      <form ref={formRef} id="checkin-form" className="mx-auto w-full py-4">
        <Tabs
          fullWidth
          aria-label="Metode Spinner"
          onSelectionChange={setSelectedMenu}
          className="w-full"
          variant="solid"
          color="primary"
        >
          <Tab key="scanner" title="Scan" className="mx-auto w-full max-w-xl">
            <div className="flex w-full items-center justify-center">
              <div className="relative h-[300px] w-[300px] overflow-hidden rounded-xl md:h-[500px] md:w-[500px]">
                <Scanner
                  allowMultiple
                  onScan={getScannerResults}
                  formats={[
                    "aztec",
                    "code_128",
                    "code_39",
                    "code_93",
                    "codabar",
                    "databar",
                    "databar_expanded",
                    "data_matrix",
                    "dx_film_edge",
                    "ean_13",
                    "ean_8",
                    "itf",
                    "maxi_code",
                    "micro_qr_code",
                    "pdf417",
                    "qr_code",
                    "rm_qr_code",
                    "upc_a",
                    "upc_e",
                    "linear_codes",
                    "matrix_codes",
                    "unknown",
                  ]}
                  paused={success || isPending}
                />
              </div>
            </div>
          </Tab>
          <Tab
            key="manual_input"
            title="Cari"
            className="mx-auto w-full max-w-xl"
          >
            <div className="flex w-full flex-col justify-center gap-3">
              <Input
                name="nama_peserta"
                type="text"
                label="Masukan Nama Peserta"
                value={namaPeserta}
                placeholder="Masukan kode peserta anda"
                className="w-full"
                isRequired={selectedMenu === "manual_input"}
                onChange={(event) => {
                  if (event.target.value === "") {
                    setParticipant([]);
                  }
                  setNamaPeserta(event.target.value);
                }}
              />
              {participant?.map((data) => (
                <div
                  onClick={() => {
                    setKodePeserta(data.participant_id);
                  }}
                  key={data.participant_id}
                >
                  <Card className="border duration-300 hover:border-primary hover:shadow-primary hover:ease-in">
                    <CardBody>
                      <h1 className="text-xl font-semibold text-primary">
                        {data.full_name}
                      </h1>
                      <span className="text-sm text-gray-500">
                        {data.participant_id}
                      </span>
                      <div className="mt-2 flex flex-wrap gap-2">
                        {data.event && (
                          <Chip
                            className="capitalize"
                            color="primary"
                            size="sm"
                            variant="flat"
                          >
                            {data.event.name}
                          </Chip>
                        )}
                        {data.clothes_size && (
                          <Chip
                            className="capitalize"
                            color="secondary"
                            size="sm"
                            variant="flat"
                          >
                            {data.clothes_size}
                          </Chip>
                        )}
                      </div>
                    </CardBody>
                  </Card>
                </div>
              ))}
            </div>
          </Tab>
          <Tab key="list_scanned" title="List">
            <TablePesertaSpinned />
          </Tab>
        </Tabs>

        {participant
          .filter((data) => data.participant_id === kodePeserta)
          .map((data) => (
            <Modal
              key={data.participant_id}
              isOpen={kodePeserta.length > 0}
              onClose={() => {
                setKodePeserta("");
              }}
            >
              <ModalContent>
                <ModalHeader>
                  <h1 className="text-xl">
                    Spinner Acara Titn Dengan Akun{" "}
                    {<b className="text-primary">{data.full_name}</b>}{" "}
                  </h1>
                </ModalHeader>
                <ModalBody className="w-full items-center font-semibold">
                  <Card className="w-full border">
                    <CardBody>
                      <h1 className="text-xl font-semibold text-primary">
                        {data.full_name}
                      </h1>
                      <span className="text-sm text-gray-500">
                        {data.participant_id}
                      </span>
                      <div className="mt-2 flex flex-wrap gap-2">
                        {data.event && (
                          <Chip
                            className="capitalize"
                            color="primary"
                            size="sm"
                            variant="flat"
                          >
                            {data.event.name}
                          </Chip>
                        )}
                        {data.clothes_size && (
                          <Chip
                            className="capitalize"
                            color="secondary"
                            size="sm"
                            variant="flat"
                          >
                            {data.clothes_size}
                          </Chip>
                        )}
                      </div>
                    </CardBody>
                  </Card>
                </ModalBody>
                <ModalFooter className="w-full justify-center">
                  <SwipeableButton
                    onSuccess={handleClick}
                    text="Spinner Sekarang"
                    text_unlocked="Spinner Diproses"
                    color="#37b05b"
                  />
                </ModalFooter>
              </ModalContent>
            </Modal>
          ))}
      </form>

      <Modal
        isOpen={success}
        onClose={() => {
          setSuccess(false);
          setKodePeserta("");
          if (!error) {
            formRef.current?.reset();
          }
        }}
      >
        <ModalContent>
          <ModalHeader />
          <ModalBody className="w-full items-center font-semibold">
            {error.length > 0 ? (
              <p className="capitalize text-red-500">{error}</p>
            ) : (
              <div className="text-green-500">Check-in Spinner Berhasil!</div>
            )}
          </ModalBody>
          <ModalFooter className="w-full justify-center">
            <Button
              variant="solid"
              size="md"
              autoFocus
              className="w-full text-white"
              color={error.length > 0 ? "danger" : "success"}
              onPress={() => {
                setKodePeserta("");
                setSuccess(false);
                if (!error || selectedMenu === "manual_input") {
                  setParticipant([]);
                  formRef.current?.reset();
                }
              }}
            >
              OK
            </Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </>
  );
};

export default SpinnerForm;
