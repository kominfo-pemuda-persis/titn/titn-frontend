import { ParticipantStatus } from "@/types/participant";

export const COLUMNS: {
  name: string;
  uid: string;
  sortable: boolean;
}[] = [
  {
    name: "NO",
    uid: "no",
    sortable: false,
  },
  { name: "NPA", uid: "npa", sortable: true },
  { name: "NAMA LENGKAP", uid: "fullName", sortable: true },
  {
    name: "STATUS",
    uid: "status",
    sortable: true,
  },
  {
    name: "EVENTS",
    uid: "events",
    sortable: false,
  },
  {
    name: "EMAIL",
    uid: "email",
    sortable: false,
  },
  {
    name: "PHONE",
    uid: "phone",
    sortable: false,
  },
  {
    name: "TGL. DAFTAR",
    uid: "createdAt",
    sortable: true,
  },
  {
    name: "TERAKHIR UPDATE",
    uid: "updatedAt",
    sortable: true,
  },
  { name: "PC", uid: "pcName", sortable: true },
  { name: "PD", uid: "pdName", sortable: true },
  { name: "PW", uid: "pwName", sortable: true },
  {
    name: "UK. BAJU",
    uid: "clothesSize",
    sortable: true,
  },
  {
    name: "ACTIONS",
    uid: "actions",
    sortable: false,
  },
];

export const statusOptions = [
  // { uid: ParticipantStatus.SUBMITTED, name: "Submitted" },
  { uid: ParticipantStatus.APPROVED, name: "Approved" },
  { uid: ParticipantStatus.REJECTED, name: "Rejected" },
  { uid: ParticipantStatus.CHECKED_IN, name: "Checked" },
];

export const COLUMNS_NON_ANGGOTA: {
  name: string;
  uid: string;
  sortable: boolean;
}[] = [
  {
    name: "NO",
    uid: "no",
    sortable: false,
  },
  { name: "NAMA LENGKAP", uid: "fullName", sortable: true },
  {
    name: "STATUS",
    uid: "status",
    sortable: true,
  },
  {
    name: "EMAIL",
    uid: "email",
    sortable: false,
  },
  {
    name: "PHONE",
    uid: "phone",
    sortable: false,
  },
  {
    name: "TGL. DAFTAR",
    uid: "createdAt",
    sortable: true,
  },
  {
    name: "TERAKHIR UPDATE",
    uid: "updatedAt",
    sortable: true,
  },
  { name: "PC", uid: "pcName", sortable: true },
  { name: "PD", uid: "pdName", sortable: true },
  { name: "PW", uid: "pwName", sortable: true },
  {
    name: "UK. BAJU",
    uid: "clothesSize",
    sortable: true,
  },
  {
    name: "ACTIONS",
    uid: "actions",
    sortable: false,
  },
];

export const COLUMNS_SPINNER: {
  name: string;
  uid: string;
  sortable: boolean;
}[] = [
  {
    name: "NO",
    uid: "no",
    sortable: false,
  },
  {
    name: "NPA",
    uid: "npa",
    sortable: false,
  },
  { name: "NAMA LENGKAP", uid: "fullName", sortable: true },
  {
    name: "TERAKHIR UPDATE",
    uid: "updatedAt",
    sortable: true,
  },
  {
    name: "STATUS",
    uid: "status",
    sortable: true,
  },
];

export interface RowPeserta {
  [key: string]: string | number | undefined | any;
  npa?: string;
  fullName: string;
  pcName: string;
  pdName: string;
  pwName: string;
  key: string;
  no: string | number;
  status: string;
  events: {
    name: string;
    type: string;
  }[];
}
export interface RowPesertaNonAnggota {
  [key: string]: string | number;
  fullName: string;
  pcName: string;
  pdName: string;
  pwName: string;
  no: string | number;
}

// Define type interface for the LIST, make sure to check if fields are optional
export interface Peserta {
  npa: string;
  nama: string;
  tempat?: string;
  tanggalLahir?: string;
  statusMerital?: string;
  pekerjaan?: string;
  pw: {
    kdPw: string;
    namaWilayah: string;
    diresmikan: string;
  };
  pd: {
    kdPd: string;
    namaPd: string;
    diresmikan: string;
  };
  pc: {
    kdPc: string;
    namaPc: string;
    diresmikan: string;
  };
  pj: string;
  namaPj: string;
  desa: {
    id: string;
    nama: string;
  };
  provinsi: {
    id: string;
    namaProvinsi: string;
  };
  kabupaten: {
    id: string;
    nama: string;
  };
  kecamatan: {
    id: string;
    nama: string;
  };
  golDarah: string;
  email: string;
  noTelpon: string;
  alamat: string;
  jenisKeanggotaan: string;
  statusAktif: string;
  foto: string;
  idOtonom: number;
  jenjangPendidikan: {
    id: string;
    pendidikan: string;
  };
  masaAktifKta: string;
  regDate: string;
  levelTafiq: string;
  id: string;
  lastUpdated: string;
  createdBy: string;
  createdDate: string;
  detailPendidikan: [];
  detailKeluarga: [];
}
