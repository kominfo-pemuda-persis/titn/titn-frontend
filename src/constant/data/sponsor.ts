export const sponsorLarge = [
  {
    name: "Bank BSI",
    logo: "/assets/sponsor/bsi.png",
  },
  {
    name: "Mabes Polri",
    logo: "/assets/sponsor/polri.png",
  },
  {
    name: "OJK",
    logo: "/assets/sponsor/ojk.png",
  },
];

export const sponsorMedium = [
  {
    name: "TGM",
    logo: "/assets/sponsor/tgm.png",
  },
  {
    name: "South Legend",
    logo: "/assets/sponsor/south-legend.png",
  },
  {
    name: "Baznas Jawa Barat",
    logo: "/assets/sponsor/baznas-jabar.png",
  },
  {
    name: "Jejakin",
    logo: "/assets/sponsor/jejakin.png",
  },
];

export const sponsorSmall = [
  {
    name: "R And R Film",
    logo: "/assets/sponsor/r-n-r.png",
  },
  {
    name: "Barade Sublimation",
    logo: "/assets/sponsor/barade.png",
  },
  {
    name: "Hibro",
    logo: "/assets/sponsor/hibro.png",
  },
  {
    name: "Sumber Buncis",
    logo: "/assets/sponsor/sumber-buncis.png",
  },
  {
    name: "ZHR Textile",
    logo: "/assets/sponsor/zhr.png",
  },
];
