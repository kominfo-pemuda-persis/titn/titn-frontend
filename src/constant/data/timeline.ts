export const TIMELINE_ITEMS = [
  {
    title: "9 Juni",
    cardTitle: "Sosialisasi",
    cardDetailedText:
      "Sosialisasi kepada PD, PW Pemuda Persis, sekaligus menyampaikan rancang design TITN 2024",
  },
  {
    title: "10 - 30 Juni",
    cardTitle: "Roadshow TITN",
    cardDetailedText:
      "Roadshow TITN ke PD dan PC Pemuda Persis seindonesia Offline dan Online",
  },
  {
    title: "6 Juli",
    cardTitle: "Grand Launching dan Charity Summit",
    cardDetailedText:
      "Grand Launching Logo, Maskot serta Launching Charity Berdakwah Berdampak",
  },
  {
    title: "1 Juli - 30 Agustus",
    cardTitle: "Pendafataran Kompetisi",
    cardDetailedText:
      "Pendaftaran Kompetisi TITN, baik Temu ilmiah maupun Taaruf",
  },
  {
    title: "1 - 30 Agustus",
    cardTitle: "Kompetisi Online",
    cardDetailedText: "Kompetisi TITN bersifat Online",
  },
  {
    title: "26 - 27 September",
    cardTitle: "Kompetisi Offline",
    cardDetailedText: "Perlombaan TITN bersifat Offline",
  },
  {
    title: "28 - 29 September",
    cardTitle: "TITN",
    cardDetailedText: "Acara Puncak TITN",
  },
];
