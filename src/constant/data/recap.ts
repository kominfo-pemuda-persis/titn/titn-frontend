import { RecapStatus } from "@/types";

export const COLUMNS: {
  name: string;
  uid: string;
  sortable: boolean;
}[] = [
  {
    name: "NO",
    uid: "no",
    sortable: false,
  },
  { name: "NPA", uid: "npa", sortable: true },
  { name: "NAMA", uid: "fullName", sortable: true },
  {
    name: "STATUS",
    uid: "status",
    sortable: true,
  },
  { name: "PC", uid: "pcName", sortable: true },
  { name: "PD", uid: "pdName", sortable: true },
  { name: "PW", uid: "pwName", sortable: true },
  { name: "DIKIRIMKAN", uid: "createdAt", sortable: true },
  { name: "DIAJUKAN", uid: "nums", sortable: false },
  {
    name: "BUKTI",
    uid: "buktiTransfer",
    sortable: true,
  },
  {
    name: "ACTIONS",
    uid: "actions",
    sortable: false,
  },
];

export const COLUMNS_APPROVAL: {
  name: string;
  uid: string;
  sortable: boolean;
}[] = [
  {
    name: "NO",
    uid: "no",
    sortable: false,
  },
  { name: "NPA", uid: "npa", sortable: true },
  { name: "NAMA", uid: "fullName", sortable: true },
  { name: "PC", uid: "pcName", sortable: true },
  { name: "PD", uid: "pdName", sortable: true },
  { name: "PW", uid: "pwName", sortable: true },
  { name: "DIBUAT", uid: "createdAt", sortable: true },
  {
    name: "DISETUJUI",
    uid: "updatedAt",
    sortable: true,
  },
  {
    name: "PENYETUJU",
    uid: "updatedBy",
    sortable: true,
  },
  {
    name: "BUKTI",
    uid: "buktiTransfer",
    sortable: true,
  },
];

export const statusOptions = [
  { uid: RecapStatus.SUBMITTED, name: "Submitted" },
  { uid: RecapStatus.APPROVED, name: "Approved" },
  { uid: RecapStatus.REJECTED, name: "Rejected" },
];
