export const DATA_KOMPETISI = [
  {
    title: "Kompetisi Rekreatif",
    description:
      "Berikut adalah daftar kompetisi rekreatif jasadiyah dalam bentuk perlombaan cabang olahraga.",
    listKompetisi: [
      {
        title: "Futsal",
        path: "futsal",
      },
      {
        title: "Bola Voli",
        path: "volley",
      },
      {
        title: "Tenis Meja",
        path: "tenis",
      },
      {
        title: "Bulu Tangkis",
        path: "badminton",
      },
      {
        title: "Panahan",
        path: "archery",
      },
      {
        title: "Eksibisi Surulkan",
        path: "surulkan",
      },
      {
        title: "Balap Egrang",
        path: "egrang",
      },
      {
        title: "Tarik Tambang",
        path: "tambang",
      },
      {
        title: "Marathon",
        path: "marathon",
      },
    ],
  },
  {
    title: "Kompetisi Ilmiah",
    description: "Berikut adalah daftar kompetisi ilmiah dalam bidang ilmiah.",
    listKompetisi: [
      {
        title: "Musabaqoh Manhaj",
        path: "discussion",
      },
      {
        title: "Fahmul Quran",
        path: "research",
      },
      {
        title: "Qiroatul Kutub",
        path: "reading",
      },
      {
        title: "Menulis Ilmiah/Kreatif",
        path: "writing",
      },
      {
        title: "Debat",
        path: "debate",
      },
      {
        title: "Business Plan",
        path: "business-plan",
      },
      {
        title: "Pemetaan Dakwah",
        path: "pemetaan-dakwah",
      },
      {
        title: "Coding",
        path: "coding",
      },
      {
        title: "Konten Kreatif",
        path: "content",
      },
    ],
  },
];
