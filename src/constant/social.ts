import {
  BsFacebook,
  BsGlobe,
  BsInstagram,
  BsTelegram,
  BsTiktok,
  BsYoutube,
} from "react-icons/bs";

export const SocialLinks = [
  {
    href: "https://www.instagram.com/titnpemudapersis/",
    handler: "@titnpemudapersis",
    icon: BsInstagram,
    className:
      "text-orange-500 hover:brightness-75 flex items-center gap-4 col-span-1 md:col-span-2 md:justify-center place-center",
  },
  {
    href: "https://www.facebook.com/kominfopemudapersis",
    handler: "@kominfopemudapersis",
    icon: BsFacebook,
    className:
      "text-blue-500 hover:brightness-75 flex items-center gap-4 col-span-1",
  },
  {
    href: "https://anaonline.id",
    handler: "@anaonline.id",
    icon: BsGlobe,
    className:
      "text-green-500 hover:brightness-75 flex items-center gap-4 col-span-1",
  },
  {
    href: "https://www.tiktok.com/@pp_pemudapersis",
    handler: "@pp_pemudapersis",
    icon: BsTiktok,
    className:
      "text-purple-500 hover:text-gray-400 flex items-center gap-4 col-span-1",
  },
  {
    href: "https://www.instagram.com/pp_pemudapersis",
    handler: "@pp_pemudapersis",
    icon: BsInstagram,
    className:
      "text-yellow-500 hover:brightness-75 flex items-center gap-4 col-span-1",
  },
  {
    href: "https://www.youtube.com/c/pppemudapersis",
    handler: "@pppemudapersis",
    icon: BsYoutube,
    className:
      "text-red-500 hover:brightness-75 flex items-center gap-4 col-span-1",
  },
  {
    href: "https://t.me/pemudapersis",
    handler: "@pemudapersis",
    icon: BsTelegram,
    className:
      "text-blue-500 hover:brightness-75 flex items-center gap-4 col-span-1",
  },
];

export const GoogleMapLink =
  "https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d5392.264680716187!2d107.388184515236!3d-7.1385081133456225!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e688db141aaddb1%3A0x6c2898e118b83daf!2sRanca%20Upas%20Ciwidey!5e0!3m2!1sen!2sid!4v1717779206828!5m2!1sen!2sid";
