export const AUTH = "/api/auth";
export const GET_PW = "/api/wilayah";
export const GET_PD_BY_PW = "/api/daerah";
export const GET_PC_BY_PD = "/api/cabang";
export const GET_USER_BY_NPA = "/api/anggota/npa";
export const USER_MANAGEMENT = "/api/management/user";
export const ANGGOTA_PROFILE = "/api/anggota/profile";

// TITN-service api
export const PARTICIPANT = "/api/participants";
export const PARTICIPANT_PUBLIC = "/api/public/participants";
export const SPINNER_PUBLIC = "/api/public/participants/spinner";
export const DASHBOARD = "/api/dashboard";
export const DASHBOARD_PANITIA = "/api/dashboard/panitia";
export const EVENTS = "api/events";
export const RECAPS = "/api/recaps";
export const SYAHADAH = "/api/e-syahadah";

export const UPLOAD_IMAGE = "/api/upload";
