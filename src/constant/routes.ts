import {
  BsBook,
  BsCamera,
  BsCheck2All,
  BsHouseFill,
  BsNewspaper,
  BsPaperclip,
  BsPersonFill,
  BsPersonLinesFill,
} from "react-icons/bs";

export const ROUTE_HOME = "/";
export const ROUTE_LOGIN = "/login";
export const ROUTE_DAFTAR = "/daftar";
export const ROUTE_CHECK_IN = "/check-in";
export const ROUTE_SPINNER = "/spinner";
export const ROUTE_DASHBOARD = "/dashboard";
export const ROUTE_LIST_PESERTA = "/peserta";
export const ROUTE_APPROVAL_HISTORY = "/approval-history";
export const ROUTE_REKAP = "/rekap";
export const ROUTE_SETTINGS = "/settings";
export const ROUTE_SETTINGS_PROFILE = "/settings/profile";
export const ROUTE_CALON_PESERTA = "/calon-peserta";
export const ROUTE_MY_PROFILE = "/my-profile";
export const ROUTER_DOKUMEN = "/dokumen";

export const NAV_HOME_ITEMS = [
  {
    href: "#tentang",
    text: "Tentang",
  },
  {
    href: "#timeline",
    text: "Timeline",
  },
  {
    href: "#kompetisi",
    text: "Kompetisi",
  },
  {
    href: "#dokumentasi",
    text: "Dokumentasi",
  },
  {
    href: "#gallery",
    text: "Gallery",
  },
  {
    href: "#faq",
    text: "FAQ",
  },
  {
    href: "#social",
    text: "Social",
  },
];

export const NAV_DASHBOARD_ITEMS = [
  {
    title: "Dashboard",
    icon: BsHouseFill,
    href: ROUTE_DASHBOARD,
    isActive: false,
    isHidden: false,
  },
  {
    title: "Calon Peserta",
    icon: BsPersonLinesFill,
    href: ROUTE_CALON_PESERTA,
    isActive: false,
    isHidden: false,
  },
  {
    title: "Peserta",
    icon: BsPersonFill,
    href: ROUTE_LIST_PESERTA,
    isActive: false,
    isHidden: false,
  },
  {
    title: "List Rekap",
    icon: BsPaperclip,
    href: ROUTE_REKAP,
    isActive: false,
    isHidden: false,
  },
  {
    title: "Approval History",
    icon: BsBook,
    href: ROUTE_APPROVAL_HISTORY,
    isActive: false,
    isHidden: false,
  },
  {
    title: "Check-in",
    icon: BsCheck2All,
    href: ROUTE_CHECK_IN,
    isActive: false,
    isHidden: false,
  },
  {
    title: "Spinner",
    icon: BsCamera,
    href: ROUTE_SPINNER,
    isActive: false,
    isHidden: false,
  },
  {
    title: "Dokumen",
    icon: BsNewspaper,
    href: ROUTER_DOKUMEN,
    isActive: false,
    isHidden: false,
  },
  // {
  //   title: "Profile Saya",
  //   icon: BsPerson,
  //   href: ROUTE_MY_PROFILE,
  //   isActive: false,
  //   isHidden: false,
  // },
];

export const RouteConfig = {
  allowedAnggotaUrl: [
    ROUTE_DASHBOARD,
    ROUTE_MY_PROFILE,
    ROUTE_SETTINGS_PROFILE,
    ROUTER_DOKUMEN,
  ],
  allowedAdminPCUrl: [
    ROUTE_DASHBOARD,
    ROUTE_CALON_PESERTA,
    ROUTE_LIST_PESERTA,
    ROUTE_SETTINGS_PROFILE,
    ROUTER_DOKUMEN,
  ],
  allowedAdminPDUrl: [
    ROUTE_DASHBOARD,
    ROUTE_CALON_PESERTA,
    ROUTE_LIST_PESERTA,
    ROUTE_SETTINGS_PROFILE,
    ROUTER_DOKUMEN,
  ],
  allowedAdminPWUrl: [
    ROUTE_DASHBOARD,
    ROUTE_CALON_PESERTA,
    ROUTE_LIST_PESERTA,
    ROUTE_SETTINGS_PROFILE,
    ROUTER_DOKUMEN,
  ],
  allowedAdminPPUrl: [
    ROUTE_DASHBOARD,
    ROUTE_CALON_PESERTA,
    ROUTE_LIST_PESERTA,
    ROUTE_REKAP,
    ROUTE_APPROVAL_HISTORY,
    ROUTE_CHECK_IN,
    ROUTE_SPINNER,
    ROUTE_SETTINGS_PROFILE,
    ROUTER_DOKUMEN,
  ],
  allowedTasykilPPUrl: [
    ROUTE_DASHBOARD,
    ROUTE_MY_PROFILE,
    ROUTE_SETTINGS_PROFILE,
    ROUTE_CALON_PESERTA,
    ROUTER_DOKUMEN,
  ],
  allowedSuperAdminUrl: [
    ROUTE_DASHBOARD,
    ROUTE_CALON_PESERTA,
    ROUTE_LIST_PESERTA,
    ROUTE_REKAP,
    ROUTE_APPROVAL_HISTORY,
    ROUTE_CHECK_IN,
    ROUTE_SPINNER,
    ROUTE_SETTINGS_PROFILE,
    ROUTER_DOKUMEN,
  ],
};
