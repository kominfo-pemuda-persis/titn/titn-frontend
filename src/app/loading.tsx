import Loader from "@/components/atoms/Loader/Loader";
import { FullScreenContainer } from "@/components/template/Container";
import styles from "@/styles/styles.module.css";

const Loading = () => (
  <FullScreenContainer className={styles.background}>
    <div className="flex flex-col items-center justify-center text-center">
      <Loader />
    </div>
  </FullScreenContainer>
);

export default Loading;
