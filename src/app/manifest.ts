import { MetadataRoute } from "next";

import { ROUTE_HOME } from "@/constant/routes";

export default function manifest(): MetadataRoute.Manifest {
  return {
    name: "TITN Pemuda Persis",
    short_name: "TITN Pemuda Persatuan Islam 2024",
    description:
      "Ini adalah landing page untuk event TITN ke-14 Pemuda Persatuan Islam 2024.",
    start_url: ROUTE_HOME,
    display: "standalone",
    background_color: "#fff",
    theme_color: "#fff",
    icons: [
      {
        src: "/android-chrome-192x192.png?v=1.0",
        sizes: "192x192",
        type: "image/png",
      },
      {
        src: "/android-chrome-512x512.png?v=1.0",
        sizes: "512x512",
        type: "image/png",
      },
    ],
  };
}
