import { Metadata } from "next";

import DokumenContainer from "@/components/pages/DokumenContainer/DokumenContainer";

export const metadata: Metadata = {
  title: "Dokumen",
};

const DokumentPage = () => <DokumenContainer />;

export default DokumentPage;
