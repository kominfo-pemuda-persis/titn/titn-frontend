import { Metadata } from "next";

import ListRekapContainer from "@/components/pages/ListRekapContainer/ListRekapContainer";
import { TableContainer } from "@/components/template/Container";

export const metadata: Metadata = {
  title: "List Rekap Peserta",
};

const RekapPage = () => (
  <TableContainer>
    <ListRekapContainer />
  </TableContainer>
);
export default RekapPage;
