import { Metadata } from "next";

import { ConfirmSyahadahContainer } from "@/components/pages/ConfirmSyahadahContainer";

export const metadata: Metadata = {
  title: "Upload Peserta E-Syahadah",
};

const ConfirmSyahadahPage = () => {
  return <ConfirmSyahadahContainer />;
};

export default ConfirmSyahadahPage;
