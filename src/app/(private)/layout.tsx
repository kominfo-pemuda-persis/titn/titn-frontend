"use client";

import { ReactNode, Suspense } from "react";

import Loader from "@/components/atoms/Loader/Loader";
import MainAdminLayout from "@/components/template/MainAdminLayout";
import useFetchUser from "@/hooks/useFetchUser";

const DashboardLayout = ({ children }: { children: ReactNode }) => {
  const { isLoading } = useFetchUser();

  return (
    <MainAdminLayout>
      {isLoading ? (
        <Loader />
      ) : (
        <Suspense fallback={<Loader />}>{children}</Suspense>
      )}
    </MainAdminLayout>
  );
};

export default DashboardLayout;
