"use client";

import SettingsProfileContainer from "@/components/pages/SettingsProfileContainer/SettingsProfileContainer";

const SettingsProfilePage = () => <SettingsProfileContainer />;

export default SettingsProfilePage;
