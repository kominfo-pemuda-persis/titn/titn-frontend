import { Metadata } from "next";

import { PesertaDetailContainer } from "@/components/pages/PesertaDetailContainer";

export const metadata: Metadata = {
  title: "Detail Peserta",
};

const PesertaDetailPage = ({
  params: { participant_id },
}: {
  params: { participant_id: string };
}) => {
  return <PesertaDetailContainer id={participant_id} />;
};

export default PesertaDetailPage;
