import { Suspense } from "react";
import { Metadata } from "next";

import { PesertaContainer } from "@/components/pages/PesertaContainer";
import { TableContainer } from "@/components/template/Container";

export const metadata: Metadata = {
  title: "List Peserta",
};

const PesertaPage = () => (
  <TableContainer>
    <Suspense fallback={null}>
      <PesertaContainer />
    </Suspense>
  </TableContainer>
);

export default PesertaPage;
