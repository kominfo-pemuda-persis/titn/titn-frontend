import { Metadata } from "next";

import { DashboardContainer } from "@/components/pages/DashboardContainer";

export const metadata: Metadata = {
  title: "Dashboard",
};

const DashboardPage = () => <DashboardContainer />;

export default DashboardPage;
