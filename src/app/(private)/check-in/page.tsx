import { Metadata } from "next";

import { CheckinContainer } from "@/components/pages/CheckinContainer";

export const metadata: Metadata = {
  title: "Check In Peserta",
};

const CheckinPage = () => {
  return <CheckinContainer />;
};

export default CheckinPage;
