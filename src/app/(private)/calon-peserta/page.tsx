import { Suspense } from "react";
import { Metadata } from "next";

import { CalonPesertaContainer } from "@/components/pages/CalonPesertaContainer";
import { TableContainer } from "@/components/template/Container";

export const metadata: Metadata = {
  title: "List Calon Peserta",
};

const PesertaPage = () => (
  <TableContainer>
    <Suspense fallback={null}>
      <CalonPesertaContainer />
    </Suspense>
  </TableContainer>
);

export default PesertaPage;
