import { Metadata } from "next";

import { ConfirmRekapContainer } from "@/components/pages/ConfirmRekapContainer";

export const metadata: Metadata = {
  title: "Ajuan Rekap Peserta",
};

const ConfirmRekapPage = () => {
  return <ConfirmRekapContainer />;
};

export default ConfirmRekapPage;
