import { Metadata } from "next";

import AddPesertaContainer from "@/components/pages/AddPesertaContainer/AddPesertaContainer";

export const metadata: Metadata = {
  title: "Pendaftaran Calon Peserta",
};

const TambahPesertaPage = () => <AddPesertaContainer />;

export default TambahPesertaPage;
