import { Metadata } from "next";

import { ApprovalHistoryContainer } from "@/components/pages/ApprovalHistoryContainer";
import { TableContainer } from "@/components/template/Container";

export const metadata: Metadata = {
  title: "Approval History",
};

const ApprovalHistoryPage = () => (
  <TableContainer>
    <ApprovalHistoryContainer />
  </TableContainer>
);

export default ApprovalHistoryPage;
