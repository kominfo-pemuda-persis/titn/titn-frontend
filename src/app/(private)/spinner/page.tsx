import { Metadata } from "next";

import { SpinnerContainer } from "@/components/pages/SpinnerContainer";

export const metadata: Metadata = {
  title: "Spinner",
};

const SpinnerPage = () => {
  return <SpinnerContainer />;
};

export default SpinnerPage;
