import { NextRequest, NextResponse } from "next/server";

import { upload } from "@/services/actions";

export async function POST(req: NextRequest) {
  const data = await req.formData();
  const file = data.get("file");
  const category = data.get("category");
  if (!file) {
    return NextResponse.json(
      { message: "Please provide a file" },
      { status: 400 }
    );
  }
  try {
    const imageUrl = await upload(file as File, category as string);
    return NextResponse.json({ imageUrl });
  } catch (error) {
    if (error instanceof Error) {
      return NextResponse.json({ message: error.message }, { status: 400 });
    }
    return NextResponse.json(
      { message: "Failed to upload image" },
      { status: 400 }
    );
  }
}
