import type { Metadata } from "next";

import { Inter } from "next/font/google";

import "./globals.css";

import { GoogleAnalytics } from "@next/third-parties/google";

import Providers from "@/providers";
import { Theme } from "@/types/utils";

const inter = Inter({ subsets: ["latin"] });

export const metadata: Metadata = {
  title: "TITN Pemuda Persatuan Islam 2024",
  description:
    "Ini adalah landing page untuk event TITN ke-14 Pemuda Persatuan Islam 2024.",
};

const RootLayout = ({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) => {
  return (
    <html lang="en" className={Theme.Light}>
      <head>
        <link rel="icon" href="/favicon.ico?v=1.0" sizes="any" />
        <link
          rel="apple-touch-icon"
          sizes="180x180"
          href="/apple-touch-icon.png?v=1.0"
        />
        <link
          rel="icon"
          type="image/png"
          sizes="32x32"
          href="/favicon-32x32.png?v=1.0"
        />
        <link
          rel="icon"
          type="image/png"
          sizes="16x16"
          href="/favicon-16x16.png?v=1.0"
        />
        <link
          rel="mask-icon"
          href="/safari-pinned-tab.svg?v=1.0"
          color="#5bbad5"
        />
        <link rel="shortcut icon" href="/favicon.ico?v=1.0" />
        <meta name="apple-mobile-web-app-title" content="TITN Pemuda Persis" />
        <meta name="application-name" content="TITN Pemuda Persis" />
        <meta name="msapplication-TileColor" content="#da532c" />
        <meta name="theme-color" content="#ffffff" />
      </head>
      <body className={inter.className}>
        <Providers>{children}</Providers>
      </body>

      <GoogleAnalytics
        gaId={process.env.NEXT_PUBLIC_GOOGLE_ANALYTICS_ID || ""}
      />
    </html>
  );
};

export default RootLayout;
