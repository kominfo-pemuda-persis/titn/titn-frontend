import { Metadata } from "next";

import { HomepageContainer } from "@/components/pages/HomepageContainer";

export const metadata: Metadata = {
  title: "Beranda | TITN-14 Pemuda Persis 2024",
};

const HomePage = async () => <HomepageContainer />;

export default HomePage;
