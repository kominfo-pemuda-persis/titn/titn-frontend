"use client";

import { BsDownload } from "react-icons/bs";

import { Button } from "@/components/atoms/Button";
import PDFReader from "@/components/atoms/Pdf";
import { Paragraph, Subtitle } from "@/components/atoms/Text";
import { FullScreenContainer } from "@/components/template/Container";

const KetentuanPage = () => {
  return (
    <FullScreenContainer>
      <div className="mt-16 space-y-4 text-center">
        <div>
          <Subtitle
            text="Ketentuan Lomba"
            className="font-bold uppercase text-primary"
          />
          <Paragraph text="Berikut adalah ketentuan lomba yang harus dipatuhi oleh peserta." />
        </div>
        <Button startContent={<BsDownload />} size="sm" color="primary">
          <a href="/assets/pdf/ketentuan-lomba.pdf" download>
            Download PDF
          </a>
        </Button>
      </div>

      <div className="mt-4">
        <PDFReader src="/assets/pdf/ketentuan-lomba.pdf" />
      </div>
    </FullScreenContainer>
  );
};

export default KetentuanPage;
