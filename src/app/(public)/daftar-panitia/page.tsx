import { Metadata } from "next";
import { CookiesProvider } from "next-client-cookies/server";

import DaftarPanitiaContainer from "@/components/pages/DaftarPanitiaContainer";

export const metadata: Metadata = {
  title: "Pendaftaran Panitia",
};

const DaftarPage = () => (
  <CookiesProvider>
    <DaftarPanitiaContainer />
  </CookiesProvider>
);

export default DaftarPage;
