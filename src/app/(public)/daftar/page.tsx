import { Metadata } from "next";
import { CookiesProvider } from "next-client-cookies/server";

import { DaftarContainer } from "@/components/pages/DaftarContainer";

export const metadata: Metadata = {
  title: "Pendaftaran Calon Peserta",
};

const DaftarPage = () => (
  <CookiesProvider>
    <DaftarContainer />
  </CookiesProvider>
);

export default DaftarPage;
