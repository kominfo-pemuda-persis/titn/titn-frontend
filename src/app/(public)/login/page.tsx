import { Metadata } from "next";

import { LoginContainer } from "@/components/pages/LoginContainer";

export const metadata: Metadata = {
  title: "Masuk Admin",
};

const LoginPage = () => <LoginContainer />;

export default LoginPage;
