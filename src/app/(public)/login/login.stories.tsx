import React from "react";
import { Meta } from "@storybook/react";

import Providers from "@/providers";
import LoginPage from "./page";

export default {
  title: "Pages/Login",
  component: LoginPage,
  argTypes: {},
} as Meta<typeof LoginPage>;

export const DefaultPage = () => (
  <Providers>
    <LoginPage />
  </Providers>
);
