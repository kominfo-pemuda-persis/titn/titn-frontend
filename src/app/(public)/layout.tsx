import { ReactNode } from "react";

import { NavigationBar } from "@/components/organisms/NavigationBar";
import styles from "@/styles/styles.module.css";

const PublicTemplate = ({ children }: { children: ReactNode }) => {
  return (
    <>
      <NavigationBar />
      <main className={`relative flex h-screen flex-col ${styles.background}`}>
        {children}
      </main>
    </>
  );
};

export default PublicTemplate;
