import { Metadata } from "next";
import { CookiesProvider } from "next-client-cookies/server";

import { DaftarPengunjung } from "@/components/pages/DaftarPengunjung";

export const metadata: Metadata = {
  title: "Pendaftaran Pengunjung",
};

const DaftarPage = () => (
  <CookiesProvider>
    <DaftarPengunjung />
  </CookiesProvider>
);

export default DaftarPage;
