export type BaseResponse = {
  message?: string;
  success: boolean;
  data?: any;
};
