import { z } from "zod";

export type DataRecap = {
  npa: string;
  full_name: string;
  email: string;
  phone: string;
  pc_code: string;
  pd_code: string;
  pw_code: string;
  pc_name: string;
  pd_name: string;
  pw_name: string;
  bukti_transfer: string;
  participants: [string];
  nums: string;
};

export interface UpdateInputRecap {
  status: RecapStatus;
  reject_reason?: string;
}

export interface Recap {
  recap_id: string;
  npa: string;
  full_name: string;
  email: string;
  phone: string;
  pc: string;
  pc_name: string;
  pd: string;
  pd_name: string;
  pw: string;
  pw_name: string;
  created_at: string;
  bukti_transfer: string;
  nums: string;
  status: string;
  updated_by: string;
  updated_at: string;
}

export const DataRecapSchema = z.object({
  npa: z
    .string({ message: "Nomor NPA wajib diisi" })
    .nonempty({ message: "Nomor NPA wajib diisi" }),
  full_name: z
    .string({ message: "Nama wajib diisi" })
    .nonempty({ message: "Nama wajib diisi" }),
  email: z
    .string({ message: "Email wajib diisi" })
    .email({ message: "Email tidak aktif" })
    .nonempty({ message: "Email wajib diisi" }),
  phone: z
    .string({ message: "Nomor telepon wajib diisi" })
    .nonempty({ message: "Nomor telepon wajib diisi" }),
  pc_code: z
    .string({ message: "Kode PC wajib diisi" })
    .nonempty({ message: "Kode PC wajib diisi" }),
  pd_code: z
    .string({ message: "Kode PD wajib diisi" })
    .nonempty({ message: "Kode PD wajib diisi" }),
  pw_code: z
    .string({ message: "Kode PW wajib diisi" })
    .nonempty({ message: "Kode PW wajib diisi" }),
  pc_name: z.optional(z.string()),
  pd_name: z.optional(z.string()),
  pw_name: z.optional(z.string()),
  bukti_transfer: z
    .string({ message: "Bukti transfer wajib diisi" })
    .nonempty({ message: "Bukti transfer wajib diisi" }),
  participants: z
    .array(z.string({ message: "Anggota wajib diisi" }))
    .nonempty({ message: "Anggota wajib diisi" }),
  nums: z
    .string({ message: "Jumlah yang dikonfirmasi wajib diisi" })
    .nonempty({ message: "Jumlah yang dikonfirmasi wajib diisi" }),
});

export enum RecapStatus {
  SUBMITTED = "SUBMITTED",
  APPROVED = "APPROVED",
  REJECTED = "REJECTED",
}

export interface RowRekap {
  [key: string]: string | number;
  npa: string;
  fullName: string;
  pcName: string;
  pdName: string;
  pwName: string;
  key: string;
  buktiTransfer: string;
  status: string;
  createdAt: string;
  nums: string;
}

export interface RowApproval {
  [key: string]: string | number;
  npa: string;
  fullName: string;
  pcName: string;
  pdName: string;
  pwName: string;
  key: string;
  buktiTransfer: string;
  updatedAt: string;
  updatedBy: string;
  no: number;
}
