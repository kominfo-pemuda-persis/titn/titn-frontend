export interface Participant {
  participant_id: string;
  full_name: string;
  npa: string;
  email: string;
  phone: string;
  nik: string;
  foto: string;
  photo?: string;
  event: {
    id: string;
    name: string;
    description: string;
    type: string;
  };
  event_details?: {
    id: string;
    name: string;
    description: string;
    type: string;
  }[];
  clothes_size: string;
  pc_code: string;
  pc_name: string;
  pd_code: string;
  pd_name: string;
  pw_code: string;
  pw_name: string;
  status: ParticipantStatus;
  updated_by: string;
  updated_at: string;
  created_by: string;
  created_at: string;
}

export enum ParticipantStatus {
  SUBMITTED = "SUBMITTED",
  APPROVED = "APPROVED",
  REJECTED = "REJECTED",
  CHECKED_IN = "CHECKED_IN",
}

export enum EventType {
  etc = "ETC",
  olahraga = "OLAHRAGA",
  ilmiah = "ILMIAH",
}
