export enum Theme {
  Light = "light",
  Dark = "dark",
}

export interface TokenPayload {
  header: { alg: string; typ: string };
  payload: {
    sub: string;
    exp: number;
    iss: string;
    permissions: any[];
    role: string;
    npa: string;
  };
  signature: string;
}

export enum Sort {
  ASC = "ASC",
  DESC = "DESC",
}

export enum NotificationType {
  EMAIL = "EMAIL",
  WA = "WA",
  ALL = "ALL",
}
