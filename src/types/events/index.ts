export type eventItem = {
  id: string;
  name: string;
  description: string;
  type: string;
  updated_by: string;
  created_by: string;
  created_at: string;
  updated_at: string;
};

export type EventType = "ETC" | "OLAHRAGA" | "ILMIAH";
