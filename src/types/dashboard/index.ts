export type StatisticEvent = {
  name: string;
  values: {
    name: string;
    value: number;
  }[];
};

export type StatisticResponse = {
  total_jumlah_calon_peserta: number;
  total_jumlah_calon_peserta_anggota: number;
  total_jumlah_calon_peserta_non_anggota: number;
  total_jumlah_peserta: number;
  total_jumlah_peserta_anggota: number;
  total_jumlah_peserta_non_anggota: number;
  total_jumlah_peserta_anggota_checked: number;
  total_jumlah_peserta_checked: number;
  total_jumlah_panitia_checked: number;
  total_jumlah_panitia: number;
  total_jumlah_calon_panitia: number;
  total_jumlah_peserta_non_anggota_checked: number;
  event_summary: StatisticEvent[];
  clothes_size_summary: {
    pcCode: string;
    pcName: string;
    clothesSize: string;
    value: number;
  }[];
};
