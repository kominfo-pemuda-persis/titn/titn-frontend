import { z } from "zod";

export type DataAnggota = {
  participant_id?: string;
  npa?: string;
  full_name: string;
  email: string;
  phone: string;
  nik?: string;
  event_ids: number[];
  clothes_size?: string | undefined;
  pc_code?: string;
  pc_name?: string;
  pd_code?: string;
  pd_name?: string;
  pw_code?: string;
  pw_name?: string;
  photo?: string;
};

export type DataPengunjung = {
  full_name: string;
  email: string;
  phone: string;
  event_ids: number[];
};

export const DataAnggotaSchema = z.object({
  participant_id: z.string().optional(),
  npa: z.string().min(5, { message: "NPA tidak valid" }),
  full_name: z.string({ message: "Field nama wajib dipenuhi" }),
  email: z
    .string({ message: "Field email wajib dipenuhi" })
    .email({ message: "Email tidak valid" }),
  phone: z.string({ message: "Field nomor telepon wajib dipenuhi" }),
  event_ids: z.number({ message: "Pilih Event Terlebih dahulu" }).array(),
  clothes_size: z.string({
    message: "Pilih Ukuran Baju Terlebih Dahulu",
  }),
  pc_code: z.string({ message: "Pilih Cabang Terlebih Dahulu" }).optional(),
  pc_name: z.string({ message: "Pilih Cabang Terlebih Dahulu" }),
  pd_code: z.string({ message: "Pilih Daerah Terlebih Dahulu" }).optional(),
  pd_name: z.string({ message: "Pilih Daerah Terlebih Dahulu" }),
  pw_code: z.string({ message: "Pilih Wilayah Terlebih Dahulu" }).optional(),
  pw_name: z.string({ message: "Pilih Wilayah Terlebih Dahulu" }),
  photo: z.string({ message: "Upload Gambar Terlebih Dahulu" }),
});

export const DataSimpatisanSchema = z.object({
  participant_id: z.string(),
  npa: z.literal(""),
  full_name: z.string({ message: "Field Nama Wajib Dipenuhi" }),
  email: z
    .string({ message: "Field Email Wajib Dipenuhi" })
    .email({ message: "Email tidak valid" }),
  nik: z.string().min(5, { message: "Field NIK Wajib Dipenuhi" }),
  clothes_size: z.string({ message: "Pilih UKuran Baju Terlebih Dahulu" }),
  pc_code: z.string({ message: "Pilih Cabang Terlebih Dahulu" }),
  pc_name: z.string({ message: "Pilih Cabang Terlebih Dahulu" }),
  pd_code: z.string({ message: "Pilih Daerah Terlebih Dahulu" }),
  pd_name: z.string({ message: "Pilih Daerah Terlebih Dahulu" }),
  pw_code: z.string({ message: "Pilih Wilayah Terlebih Dahulu" }),
  pw_name: z.string({ message: "Pilih Wilayah Terlebih Dahulu" }),
  photo: z.string({ message: "Upload Gambar Terlebih Dahulu" }),
  event_ids: z.number({ message: "Pilih Event Terlebih dahulu" }).array(),
  phone: z.string().min(5, { message: "Field nomor telepon wajib dipenuhi" }),
});

export const DataPengunjungScheme = z.object({
  nik: z.string().optional(),
  full_name: z.string({ message: "Field Nama Wajib Dipenuhi" }),
  email: z
    .string({ message: "Field Email Wajib Dipenuhi" })
    .email({ message: "Email tidak valid" }),
  event_ids: z.number({ message: "Pilih Event Terlebih dahulu" }).array(),
  clothes_size: z.string({ message: "Pilih UKuran Baju Terlebih Dahulu" }),
  phone: z.string().min(5, { message: "Field nomor telepon wajib dipenuhi" }),
  pc_code: z.string({ message: "Pilih Cabang Terlebih Dahulu" }),
  pc_name: z.string({ message: "Pilih Cabang Terlebih Dahulu" }),
  pd_code: z.string({ message: "Pilih Daerah Terlebih Dahulu" }),
  pd_name: z.string({ message: "Pilih Daerah Terlebih Dahulu" }),
  pw_code: z.string({ message: "Pilih Wilayah Terlebih Dahulu" }),
  pw_name: z.string({ message: "Pilih Wilayah Terlebih Dahulu" }),
});

export interface RegisterParticipantPayload extends DataAnggota {
  session?: string;
}

export interface AnggotaProps {
  npa: string;
  nama: string;
  tempat: string;
  tanggalLahir: string;
  statusMerital: string;
  pekerjaan: string;
  pw: {
    kdPw: string;
    namaWilayah: string;
    diresmikan: string;
  };
  pd: {
    kdPd: string;
    namaPd: string;
    diresmikan: string;
  };
  pc: {
    kdPc: string;
    kdPd: string;
    namaPc: string;
    diresmikan: string;
  };
  pj: string;
  namaPj: string;
  desa: {
    id: string;
    nama: string;
  };
  provinsi: {
    id: string;
    namaProvinsi: string;
  };
  kabupaten: {
    id: string;
    nama: string;
  };
  kecamatan: {
    id: string;
    nama: string;
  };
  golDarah: string;
  email: string;
  noTelpon: string;
  alamat: string;
  jenisKeanggotaan: string;
  statusAktif: string;
  photo: string;
  foto?: string;
  idOtonom: number;
  jenjangPendidikan: {
    id: string;
    pendidikan: string;
  };
  masaAktifKta: string;
  regDate: string;
  levelTafiq: string;
  id: string;
  lastUpdated: string;
  createdBy: string;
  createdDate: string;
  role: [
    {
      id: number;
      name: string;
      displayName: string;
      description: string;
      status: string;
      permissions: [
        {
          module: string;
          permission: string;
        },
      ];
    },
  ];
  detailPendidikan: [
    {
      idPendidikan: number;
      jenjangPendidikan: {
        id: string;
        pendidikan: string;
      };
      instansi: string;
      jurusan: string;
      tahunMasuk: number;
      tahunKeluar: number;
      jenisPendidikan: string;
    },
  ];
  detailKeluarga: [
    {
      idKeluarga: number;
      namaKeluarga: string;
      hubungan: string;
      jumlahAnak: number;
      alamat: string;
      otonom: {
        idOtonom: number;
        namaOtonom: string;
      };
      keterangan: string;
      statusAnggota: string;
    },
  ];
}
