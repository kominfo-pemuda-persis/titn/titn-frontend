"use server";

import { cookies } from "next/headers";
import axios from "axios";

import { getUserToken } from "@/services/mutation/user";
import { TokenPayload } from "@/types/utils";
import { decodeJWT } from "@/utils/text";

export const login = async (formData: { npa: string; password: string }) => {
  clearCookies();

  const response = await getUserToken({
    npa: formData.npa,
    password: formData.password,
  });

  if (!response.accessToken) {
    throw new Error("Failed to authenticate");
  }

  // save token into axios headers
  axios.defaults.headers.common["Authorization"] =
    `Bearer ${response.accessToken}`;

  // decode token using base64
  const decodedToken: TokenPayload = decodeJWT(response.accessToken);

  if (decodedToken) {
    // Save the session in a cookie
    cookies().set("session", response.accessToken);
  } else {
    throw new Error("Invalid token");
  }
};

export const clearCookies = async () => {
  cookies().set("session", "", { expires: new Date(0) });
};

export const getSession = async () => {
  const accessToken = cookies().get("session")?.value;

  if (!accessToken) return null;

  const session = {
    accessToken,
  };

  return session;
};
