"use server";

import axios from "axios";

import { RECAPS } from "@/constant/endpoint";
import { getSession } from "@/lib/auth";
import { BaseResponse, DataRecap, UpdateInputRecap } from "@/types";

export const postRecaps = async ({
  npa,
  full_name,
  email,
  phone,
  pc_code,
  pd_code,
  pw_code,
  pc_name,
  pd_name,
  pw_name,
  bukti_transfer,
  participants,
  nums,
}: DataRecap) => {
  try {
    const token = await getSession();

    const response = await axios.post(
      `${RECAPS}`,
      {
        npa,
        full_name,
        email,
        phone,
        pc_code,
        pd_code,
        pw_code,
        pc_name,
        pd_name,
        pw_name,
        bukti_transfer,
        participants,
        nums,
      },
      {
        baseURL: process.env.NEXT_PUBLIC_API_URL2,
        headers: {
          "Content-Type": "application/json",
          Authorization: token?.accessToken
            ? `Bearer ${token.accessToken}`
            : "",
        },
      }
    );

    return {
      success: true,
      message: response.data.message as string,
    } as BaseResponse;
  } catch (error) {
    if (error instanceof axios.AxiosError) {
      const errors: string[] = error.response?.data.errors;
      return {
        success: false,
        message: errors
          .map((err: string) => err.replaceAll("_", " "))
          .join(",")
          .toLowerCase(),
      } as BaseResponse;
    }

    return {
      success: false,
      message: "Something went wrong",
    } as BaseResponse;
  }
};

export const deleteRecaps = async (id: string) => {
  try {
    const token = await getSession();

    const response = await axios.delete(`${RECAPS}/${id}`, {
      baseURL: process.env.NEXT_PUBLIC_API_URL2,
      headers: {
        Authorization: token?.accessToken ? `Bearer ${token.accessToken}` : "",
      },
    });

    console.log(response);

    return {
      success: true,
      message: response.data.message as string,
    } as BaseResponse;
  } catch (error) {
    if (error instanceof axios.AxiosError) {
      const errors: string[] = error.response?.data.errors;
      return {
        success: false,
        message: errors
          .map((err: string) => err.replaceAll("_", " "))
          .join(",")
          .toLowerCase(),
      } as BaseResponse;
    }

    return {
      success: false,
      message: "Something went wrong",
    } as BaseResponse;
  }
};

export const updateRecap = async (id: string, data: UpdateInputRecap) => {
  try {
    const token = await getSession();

    const response = await axios.put(`${RECAPS}/${id}`, data, {
      baseURL: process.env.NEXT_PUBLIC_API_URL2,
      headers: {
        Authorization: token?.accessToken ? `Bearer ${token.accessToken}` : "",
      },
    });

    console.log(response);

    return {
      success: true,
      message: response.data.message as string,
    } as BaseResponse;
  } catch (error) {
    if (error instanceof axios.AxiosError) {
      const errors: string[] = error.response?.data.errors;
      return {
        success: false,
        message: errors
          .map((err: string) => err.replaceAll("_", " "))
          .join(",")
          .toLowerCase(),
      } as BaseResponse;
    }

    return {
      success: false,
      message: "Something went wrong",
    } as BaseResponse;
  }
};
