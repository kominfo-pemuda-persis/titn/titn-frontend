"use server";

import axios from "axios";

import {
  PARTICIPANT,
  PARTICIPANT_PUBLIC,
  SPINNER_PUBLIC,
} from "@/constant/endpoint";
import { getSession } from "@/lib/auth";
import { BaseResponse, RegisterParticipantPayload } from "@/types";
import { NotificationType } from "@/types/utils";

export const postRegisterParticipant = async ({
  participant_id,
  npa,
  nik = "",
  full_name,
  email,
  pc_name,
  pc_code,
  pd_name,
  pd_code,
  pw_name,
  pw_code,
  photo,
  event_ids,
  clothes_size,
  phone,
}: RegisterParticipantPayload) => {
  try {
    const response = await axios.post(
      `${PARTICIPANT_PUBLIC}`,
      {
        participant_id,
        npa,
        nik,
        full_name,
        email,
        pc_name,
        pd_name,
        pw_name,
        pc_code,
        pd_code,
        pw_code,
        photo,
        event_ids,
        clothes_size,
        phone,
      },
      {
        baseURL: process.env.NEXT_PUBLIC_API_URL2,
        headers: {
          "Content-Type": "application/json",
        },
      }
    );

    return {
      success: true,
      message: response.data.message as string,
    } as BaseResponse;
  } catch (error) {
    if (error instanceof axios.AxiosError) {
      const errors: string[] = error.response?.data.errors;
      return {
        success: false,
        message: errors
          ? errors
              .map((err: string) => err.replaceAll("_", " "))
              .join(",")
              .toLowerCase()
          : "Gagal Mendaftarkan Anggota",
      } as BaseResponse;
    }

    return {
      success: false,
      message: "Something went wrong",
    } as BaseResponse;
  }
};

export const postUploadBuktiPembayaran =
  async ({}: RegisterParticipantPayload): Promise<BaseResponse> => {
    let response = {} as BaseResponse;

    return response;
  };

export const postCheckin = async ({ code }: { code: string }) => {
  try {
    const response = await axios.patch(
      `${PARTICIPANT_PUBLIC}/${code}`,
      {
        status: "checked_in",
      },
      {
        baseURL: process.env.NEXT_PUBLIC_API_URL2,
        headers: {
          "Content-Type": "application/json",
        },
      }
    );

    return {
      success: true,
      message: response.data.message as string,
      data: response?.data,
    };
  } catch (error) {
    if (error instanceof axios.AxiosError) {
      const errors: string[] = error.response?.data.errors;
      if (errors) {
        return {
          success: false,
          message: errors
            ? errors
                .map((err: string) => err.replaceAll("_", " "))
                .join(",")
                .toLowerCase()
            : "Gagal Melakukan Check In",
        } as BaseResponse;
      }
      return {
        success: false,
        message: error.message ?? "Something went wrong",
        data: null,
      } as BaseResponse;
    }

    return {
      success: false,
      message: "Something went wrong",
      data: null,
    } as BaseResponse;
  }
};

export const deleteParticipant = async (id: string) => {
  const token = await getSession();
  try {
    const response = await axios.delete(`${PARTICIPANT}/${id}`, {
      baseURL: process.env.NEXT_PUBLIC_API_URL2,
      headers: {
        "Content-Type": "application/json",
        Authorization: token?.accessToken ? `Bearer ${token.accessToken}` : "",
      },
    });

    return {
      success: true,
      message: response.data.message as string,
    } as BaseResponse;
  } catch (error) {
    if (error instanceof axios.AxiosError) {
      const errors: string[] = error.response?.data.errors;
      return {
        success: false,
        message: errors
          ? errors
              .map((err: string) => err.replaceAll("_", " "))
              .join(",")
              .toLowerCase()
          : "Gagal Menghapus Anggota",
      } as BaseResponse;
    }

    return {
      success: false,
      message: "Something went wrong",
    } as BaseResponse;
  }
};

export const updateParticipantData = async ({
  participant_id,
  npa,
  nik = "",
  full_name,
  email,
  pc_name,
  pc_code,
  pd_name,
  pd_code,
  pw_name,
  pw_code,
  photo,
  event_ids,
  clothes_size,
  phone,
}: RegisterParticipantPayload) => {
  try {
    const payload = {
      participant_id,
      npa,
      nik,
      full_name,
      email,
      pc_name,
      pd_name,
      pw_name,
      pc_code,
      pd_code,
      pw_code,
      photo,
      event_ids,
      clothes_size,
      phone,
    };
    const token = await getSession();
    const response = await axios.put(
      `${PARTICIPANT}/${participant_id}`,
      payload,
      {
        baseURL: process.env.NEXT_PUBLIC_API_URL2,
        headers: {
          "Content-Type": "application/json",
          Authorization: token?.accessToken
            ? `Bearer ${token.accessToken}`
            : "",
        },
      }
    );

    return {
      success: true,
      message: response.data.message as string,
    } as BaseResponse;
  } catch (error) {
    console.log(error);
    if (error instanceof axios.AxiosError) {
      const errors: string[] = error.response?.data.errors;
      return {
        success: false,
        message: errors
          ? errors
              .map((err: string) => err.replaceAll("_", " "))
              .join(",")
              .toLowerCase()
          : "Gagal Mengupdate Anggota",
      } as BaseResponse;
    }

    return {
      success: false,
      message: "Something went wrong",
    } as BaseResponse;
  }
};

export const postSpinner = async ({ code }: { code: string }) => {
  try {
    const response = await axios.patch(
      `${SPINNER_PUBLIC}/${code}`,
      {
        isSpinner: true,
      },
      {
        baseURL: process.env.NEXT_PUBLIC_API_URL2,
        headers: {
          "Content-Type": "application/json",
        },
      }
    );

    return {
      success: true,
      message: response.data.message as string,
    };
  } catch (error) {
    if (error instanceof axios.AxiosError) {
      const errors: string[] = error.response?.data.errors;
      if (errors) {
        return {
          success: false,
          message: errors
            ? errors
                .map((err: string) => err.replaceAll("_", " "))
                .join(",")
                .toLowerCase()
            : "Gagal Melakukan Check In Spinner",
        } as BaseResponse;
      }
      return {
        success: false,
        message: error.message ?? "Something went wrong",
      } as BaseResponse;
    }

    return {
      success: false,
      message: "Something went wrong",
    } as BaseResponse;
  }
};

export const updateSendNotification = async ({
  type = NotificationType.ALL,
  participantId,
  isForce = false,
}: {
  type: NotificationType;
  participantId: string;
  isForce?: boolean;
}) => {
  try {
    const token = await getSession();
    const response = await axios.put(
      `${PARTICIPANT}/notification/${participantId}?type=${type}&isForce=${isForce}`,
      {},
      {
        baseURL: process.env.NEXT_PUBLIC_API_URL2,
        headers: {
          "Content-Type": "application/json",
          Authorization: token?.accessToken
            ? `Bearer ${token.accessToken}`
            : "",
        },
      }
    );

    return {
      success: true,
      message: response.data.message as string,
    } as BaseResponse;
  } catch (error) {
    if (error instanceof axios.AxiosError) {
      const errors: string[] = error.response?.data.errors;
      return {
        success: false,
        message: errors
          ? errors
              .map((err: string) => err.replaceAll("_", " "))
              .join(",")
              .toLowerCase()
          : "Gagal Mengirim Notifikasi",
      } as BaseResponse;
    }

    return {
      success: false,
      message: "Something went wrong",
    } as BaseResponse;
  }
};
