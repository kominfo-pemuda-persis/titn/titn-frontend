"use server";

import axios from "axios";

import { SYAHADAH } from "@/constant/endpoint";
import { getSession } from "@/lib/auth";
import { BaseResponse } from "@/types";

export const postSyahadah = async (formData: FormData) => {
  try {
    const token = await getSession();

    const response = await axios.post(`${SYAHADAH}/upload`, formData, {
      baseURL: process.env.NEXT_PUBLIC_API_URL2,
      headers: {
        Authorization: token?.accessToken ? `Bearer ${token.accessToken}` : "",
      },
    });

    return {
      success: true,
      message: response.data.message as string,
    } as BaseResponse;
  } catch (error) {
    if (error instanceof axios.AxiosError) {
      const errors: string[] = error.response?.data.errors;
      return {
        success: false,
        message: errors
          .map((err: string) => err.replaceAll("_", " "))
          .join(",")
          .toLowerCase(),
      } as BaseResponse;
    }

    return {
      success: false,
      message: "Something went wrong",
    } as BaseResponse;
  }
};

export const generateAndSendSyahadahByRekapId = async (recapId: string) => {
  try {
    const token = await getSession();

    const response = await axios.post(
      `${SYAHADAH}/recap/${recapId}`,
      {
        recapId,
      },
      {
        baseURL: process.env.NEXT_PUBLIC_API_URL2,
        headers: {
          "Content-Type": "application/json",
          Authorization: token?.accessToken
            ? `Bearer ${token.accessToken}`
            : "",
        },
      }
    );

    return {
      success: true,
      message: response.data.message as string,
    } as BaseResponse;
  } catch (error) {
    if (error instanceof axios.AxiosError) {
      const errors: string[] = error.response?.data.errors;
      return {
        success: false,
        message: errors
          .map((err: string) => err.replaceAll("_", " "))
          .join(",")
          .toLowerCase(),
      } as BaseResponse;
    }

    return {
      success: false,
      message: "Something went wrong",
    } as BaseResponse;
  }
};

export const generateAndSendSyahadahByParticipantId = async (
  participantId: string
) => {
  try {
    const token = await getSession();

    const response = await axios.post(
      `${SYAHADAH}/${participantId}`,
      {
        participantId,
      },
      {
        baseURL: process.env.NEXT_PUBLIC_API_URL2,
        headers: {
          "Content-Type": "application/json",
          Authorization: token?.accessToken
            ? `Bearer ${token.accessToken}`
            : "",
        },
      }
    );

    return {
      success: true,
      message: response.data.message as string,
    } as BaseResponse;
  } catch (error) {
    if (error instanceof axios.AxiosError) {
      const errors: string[] = error.response?.data.errors;
      return {
        success: false,
        message: errors
          .map((err: string) => err.replaceAll("_", " "))
          .join(",")
          .toLowerCase(),
      } as BaseResponse;
    }

    return {
      success: false,
      message: "Something went wrong",
    } as BaseResponse;
  }
};
