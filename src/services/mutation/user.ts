import axios from "axios";

import { AUTH } from "@/constant/endpoint";

export const getUserToken = async ({
  npa,
  password,
}: {
  npa: string;
  password: string;
}) => {
  const response = await axios.post(
    AUTH,
    {
      npa,
      password,
    },
    {
      baseURL: process.env.NEXT_PUBLIC_API_URL,
      headers: {
        "Content-Type": "application/json",
      },
    }
  );

  return response.data;
};
