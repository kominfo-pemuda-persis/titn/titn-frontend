import axios from "axios";

import { GET_PD_BY_PW } from "@/constant/endpoint";

export const getPDByPW = async (PW: string) => {
  const { data } = await axios.get(
    `${process.env.NEXT_PUBLIC_API_URL}${GET_PD_BY_PW}?kdPw=${PW}&pageSize=1000&pageNo=0`
  );
  return data.data.content;
};
