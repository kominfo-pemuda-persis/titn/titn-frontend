import axios from "axios";

import { RECAPS } from "@/constant/endpoint";
import { RecapStatus } from "@/types";
import { Sort } from "@/types/utils";

interface ListRecapsInputFilter {
  keyword?: string;
  sortBy?: string;
  pageNo?: number;
  pageSize?: number;
  signal?: AbortSignal;
  status?: RecapStatus;
  session?: string;
  sortOrder?: Sort;
}

export const listRecaps = async ({
  keyword,
  sortBy = "createdAt",
  pageNo = 0,
  pageSize = 10,
  signal,
  status,
  session,
  sortOrder = Sort.DESC,
}: ListRecapsInputFilter) => {
  try {
    const response = await axios.get(
      `${RECAPS}?${status ? `status=${status}` : ""}&pageSize=${pageSize}&pageNo=${pageNo}&sortBy=${sortBy}${keyword ? `&keyword=${keyword}` : ""}&sortOrder=${sortOrder}`,
      {
        baseURL: process.env.NEXT_PUBLIC_API_URL2,
        headers: {
          "Content-Type": "application/json",
          Authorization: session ? `Bearer ${session}` : "",
        },
        signal,
      }
    );

    if (!response) {
      return null;
    }

    return response.data;
  } catch (error: any) {
    if (error.code !== "ERR_CANCELED") console.log(error);
  }
};
