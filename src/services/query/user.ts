import axios from "axios";

import { GET_USER_BY_NPA, USER_MANAGEMENT } from "@/constant/endpoint";

export const getUserByNpa = async (
  npa: string,
  {
    signal,
  }: {
    signal: AbortSignal;
  }
) => {
  try {
    const response = await axios.get(`${GET_USER_BY_NPA}/${npa}`, {
      baseURL: process.env.NEXT_PUBLIC_API_URL,
      headers: {
        "Content-Type": "application/json",
      },
      signal,
    });

    if (!response) {
      return null;
    }

    return response.data;
  } catch (error) {
    console.log({ AxiosError: error });
    return error;
  }
};

export const getUserManagementByNpa = async (npa: string) => {
  try {
    const response = await axios.get(`${USER_MANAGEMENT}/${npa}`, {
      baseURL: process.env.NEXT_PUBLIC_API_URL,
      headers: {
        "Content-Type": "application/json",
      },
    });

    if (!response) {
      return null;
    }

    return response.data;
  } catch (error) {
    console.log(error);
  }
};
