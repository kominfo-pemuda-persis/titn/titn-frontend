import axios from "axios";

import { PARTICIPANT } from "@/constant/endpoint";
import { ParticipantStatus } from "@/types/participant";
import { Sort } from "@/types/utils";

interface ListParticipantInputFilter {
  isAnggota?: boolean;
  keyword?: string;
  session?: string;
  sortBy?: string;
  recapId?: number | null;
  pageNo?: number;
  pageSize?: number;
  signal?: AbortSignal;
  status?: ParticipantStatus;
  sortOrder?: Sort;
  isRecap?: boolean;
  isSpinner?: boolean;
}

export const listParticipant = async ({
  isAnggota,
  keyword,
  sortBy = "createdAt",
  pageNo = 0,
  pageSize = 10,
  recapId,
  signal,
  status,
  session,
  isSpinner,
  isRecap,
  sortOrder = Sort.DESC,
}: ListParticipantInputFilter) => {
  try {
    const response = await axios.get(
      `${PARTICIPANT}?${status ? `status=${status}` : ""}${typeof isRecap == "boolean" ? `&isRecap=${isRecap}` : ""}&pageSize=${pageSize}&pageNo=${pageNo}&sortBy=${sortBy}${typeof isAnggota == "boolean" ? `&isAnggota=${isAnggota}` : ""}${keyword ? `&keyword=${keyword}` : ""}&sortOrder=${sortOrder}${recapId ? `&recapId=${recapId}` : ""}${isSpinner !== undefined ? `&isSpinner=${isSpinner}` : ""}`,
      {
        baseURL: process.env.NEXT_PUBLIC_API_URL2,
        headers: {
          "Content-Type": "application/json",
          Authorization: session ? `Bearer ${session}` : "",
        },
        signal,
      }
    );

    if (!response) {
      return null;
    }

    return response.data;
  } catch (error: any) {
    if (error.code !== "ERR_CANCELED") console.log(error);
  }
};

export const getParticipantById = async (id: string, session?: string) => {
  try {
    const response = await axios.get(`${PARTICIPANT}/${id}`, {
      baseURL: process.env.NEXT_PUBLIC_API_URL2,
      headers: {
        "Content-Type": "application/json",
        Authorization: session ? `Bearer ${session}` : "",
      },
    });

    if (!response) {
      return null;
    }

    return response.data;
  } catch (error) {
    console.log(error);
  }
};

export const getParticipantByNpa = async (npa: string, session?: string) => {
  try {
    const response = await axios.get(`${PARTICIPANT}/npa/${npa}`, {
      baseURL: process.env.NEXT_PUBLIC_API_URL2,
      headers: {
        "Content-Type": "application/json",
        Authorization: session ? `Bearer ${session}` : "",
      },
    });

    if (!response) {
      return null;
    }

    return response.data;
  } catch (error) {
    console.log(error);
  }
};
