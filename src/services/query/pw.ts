import axios from "axios";

import { GET_PW } from "@/constant/endpoint";

export const getPW = async () => {
  try {
    const { data } = await axios.get(
      `${process.env.NEXT_PUBLIC_API_URL}${GET_PW}?pageSize=1000&pageNo=0`
    );
    return data.data.content;
  } catch (error) {
    console.log(error);
  }
};
