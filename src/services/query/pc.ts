import axios from "axios";

import { GET_PC_BY_PD } from "@/constant/endpoint";

export const getPCByPD = async (PD: string) => {
  const { data } = await axios.get(
    `${process.env.NEXT_PUBLIC_API_URL}${GET_PC_BY_PD}?kdPd=${PD}&pageSize=1000&pageNo=0`
  );

  return data.data.content;
};
