import axios from "axios";

import { EVENTS } from "@/constant/endpoint";

export const getEvents = async () => {
  try {
    const { data } = await axios.get(
      `${process.env.NEXT_PUBLIC_API_URL2}${EVENTS}?pageSize=1000&pageNo=0`
    );
    return data.data ?? {};
  } catch (error) {
    throw new Error("Gagal Mendapatkan Data Anggota: " + error);
  }
};
