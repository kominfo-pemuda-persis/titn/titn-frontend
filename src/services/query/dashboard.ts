"use server";

import axios from "axios";

import { DASHBOARD_PANITIA } from "@/constant/endpoint";
import { getSession } from "@/lib/auth";

export const getStatistic = async ({
  PC,
  PD,
  PW,
}: {
  PC?: string;
  PD?: string;
  PW?: string;
}) => {
  const token = await getSession();

  const response = await axios.get(
    `${DASHBOARD_PANITIA}?${PC ? `pc=${PC}` : ""}${PD ? `&pd=${PD}` : ""}${PW ? `&pw=${PW}` : ""}`,
    {
      baseURL: process.env.NEXT_PUBLIC_API_URL2,
      headers: {
        "Content-Type": "application/json",
        Authorization: token?.accessToken ? `Bearer ${token.accessToken}` : "",
      },
    }
  );

  return response.data;
};
