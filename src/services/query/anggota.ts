import axios from "axios";

import { ANGGOTA_PROFILE } from "@/constant/endpoint";

export const getCurrentAnggota = async () => {
  try {
    const response = await axios.get(`${ANGGOTA_PROFILE}`, {
      baseURL: process.env.NEXT_PUBLIC_API_URL,
      headers: {
        "Content-Type": "application/json",
      },
    });

    if (!response) {
      return null;
    }

    return response.data;
  } catch (error) {
    console.log(error);
  }
};
