"use server";

import { PutObjectCommand, S3Client } from "@aws-sdk/client-s3";
import axios from "axios";

import { clearCookies, getSession, login } from "@/lib/auth";
import { getCurrentAnggota } from "@/services/query/anggota";

export const authenticate = async (formData: {
  npa: string;
  password: string;
}) => {
  try {
    await login(formData);

    const response = await getCurrentAnggota();

    if (response) {
      return response.data;
    } else {
      clearCookies();
      throw new Error("Failed to get user data");
    }
  } catch (error: any) {
    clearCookies();
    console.log(error);
    return { error: error.message };
  }
};

export const fetchCurrentUser = async () => {
  try {
    const session = await getSession();

    // save token into axios headers
    axios.defaults.headers.common["Authorization"] =
      `Bearer ${session?.accessToken as string}`;

    const response = await getCurrentAnggota();

    if (response) {
      return response.data;
    } else {
      clearCookies();
      throw new Error("Failed to get user data");
    }
  } catch (error: any) {
    clearCookies();
  }
};

export const clearUserData = async () => {
  try {
    clearCookies();
  } catch (error: any) {
    console.log(error);
    return "Something wrong.";
  }
};

// S3
export const upload = async (file: File, category?: string) => {
  const s3Client = new S3Client({
    region: "ap-southeast-1",
    credentials: {
      accessKeyId: process.env.AWS_ACCESS_KEY_ID as string,
      secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY as string,
    },
  });
  const timestamp = new Date(
    new Date().toLocaleString("en-US", { timeZone: "Asia/Jakarta" })
  );
  const pad = (n: number) => `${Math.floor(Math.abs(n))}`.padStart(2, "0");
  const name =
    timestamp.getFullYear().toString() +
    pad(timestamp.getMonth() + 1) +
    pad(timestamp.getDate()) +
    "_" +
    pad(timestamp.getHours()) +
    pad(timestamp.getMinutes()) +
    pad(timestamp.getSeconds()) +
    pad(timestamp.getMilliseconds());
  const Bucket = process.env.S3_BUCKET_NAME;
  const Body = (await file.arrayBuffer()) as Buffer;
  let Key = name + ".jpeg";

  if (category === "rekap") {
    Key = "recap/" + Key;
  } else if (category === "calon_peserta") {
    Key = "calon-peserta/" + Key;
  }

  Key = "images/" + Key;
  try {
    await s3Client.send(new PutObjectCommand({ Bucket, Key, Body }));

    return process.env.S3_BASE_URL + Key;
  } catch (error) {
    throw new Error("Gagal Upload File: " + error);
  }
};

export const getServerDate = async () => {
  try {
    const response = await axios.get(
      "https://worldtimeapi.org/api/timezone/Asia/Jakarta"
    );

    return response.data.datetime;
  } catch (error: any) {
    console.log(error);
    return "Failed to get server date";
  }
};
