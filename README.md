# TITN 2024 Web Application

Temu Ilmiah dan Ta'aruf Nasional (TITN) adalah kegiatan yang diselenggarakan Pimpinan Pusat Pemuda Persis yang berisi
seminar, workshop, perlombaan, serta silaturahmi dan tukar gagasan antar anggota Pemuda se-Indonesia.

- [Figma Flow](https://www.figma.com/board/u2OsoUHLtfPY7fxHnSp8C5/TITN-Flow-V2?node-id=0-1&t=NWLyO91bx2bhNWf5-1)
- [Component Design](https://bradfrost.com/blog/post/atomic-web-design/)
- DEV FE: https://titn2024.anaonline.id/
- DEV BE: https://titn-dev.persis.or.id/swagger-ui/index.html
- PROD FE: https://titn2024.anaonline.id
- PROD BE: https://titn.persis.or.id/swagger-ui/index.html

This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Getting Started

First, run the development server:

```bash
bun dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

## Learn More

To learn more about Next.js, take a look at the following resources:

- [Next.js Documentation](https://nextjs.org/docs) - learn about Next.js features and API.
- [Learn Next.js](https://nextjs.org/learn) - an interactive Next.js tutorial.

You can check out [the Next.js GitHub repository](https://github.com/vercel/next.js/) - your feedback and contributions are welcome!

## Assets

- [Unsplash](https://unsplash.com/)
