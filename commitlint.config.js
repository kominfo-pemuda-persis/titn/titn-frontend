module.exports = {
  plugins: [
    {
      rules: {
        kominfo: ({ raw }) => {
          const format =
            "<spasi>#<nomer-issue><spasi>|<spasi><nama><spasi>|<spasi><commit-message>";
          const title = "FORMAT COMMIT KOMINFO PEMUDA PERSIS";
          const description = "Format message yang benar";
          const rightFormat = "<spasi>#400 | Jhon Doe | contoh commit";
          if (!raw.match(/\s#\d+\s\|\s\w+\s\|\s.+/))
            return [
              false,
              `\n${title}\n\n${description} ${format}\nContoh: ${rightFormat}`,
            ];
          return [true, ""];
        },
      },
    },
  ],
  rules: { kominfo: [2, "always"] },
};
