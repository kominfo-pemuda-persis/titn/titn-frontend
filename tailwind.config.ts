import type { Config } from "tailwindcss";

import { nextui } from "@nextui-org/react";

const config: Config = {
  content: [
    "./src/pages/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/components/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/app/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/constant/**/*.{js,ts,jsx,tsx,mdx}",
    // Path to NextUi module
    "./node_modules/@nextui-org/theme/dist/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      colors: {
        primary: "#37b05b",
        secondary: "#f59e0b",
        dark: "#827576",
      },
    },
  },
  darkMode: "class",
  plugins: [nextui()],
};

export default config;

// Theme color picked from logo

// primary : #37b05b
// 'salem': {
//   '50': '#f2fbf4',
//   '100': '#e0f8e6',
//   '200': '#c3efcf',
//   '300': '#94e1aa',
//   '400': '#5dcb7d',
//   '500': '#37b05b',
//   '600': '#289147',
//   '700': '#267c40',
//   '800': '#205b32',
//   '900': '#1c4b2c',
//   '950': '#0a2915',
// },
// secondary : #f59e0b
// 'lightning-yellow': {
//   '50': '#fffbeb',
//   '100': '#fef3c7',
//   '200': '#fde68a',
//   '300': '#fcd34d',
//   '400': '#fbc332',
//   '500': '#f59e0b',
//   '600': '#d97706',
//   '700': '#b45309',
//   '800': '#92400e',
//   '900': '#78350f',
//   '950': '#451a03',
// },
// dark : #827576
// 'shark': {
//   '50': '#f8f7f7',
//   '100': '#f0eeee',
//   '200': '#ded9d9',
//   '300': '#c0b9ba',
//   '400': '#9e9293',
//   '500': '#827576',
//   '600': '#6b5e5e',
//   '700': '#574d4d',
//   '800': '#4a4242',
//   '900': '#403a3a',
//   '950': '#201d1d',
// },

// light : #ffffff
